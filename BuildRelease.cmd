@echo off

cd /d "%~dp0"

set PRODUCT_NAME=
set PRODUCT_VERSION=
call "%~dp0ProductVars.cmd"

set PLATFORM=%1
set PROJECT_PATH=%SOLUTION_EXTRA_PATH%%PRODUCT_NAME%.sln
set CONFIGURATION=Build
set MAXCPUCOUNT=2

if "_%PLATFORM%_" == "__" goto PlatformNotSet
if "_%TARGET%_" == "__" set TARGET=Rebuild

rem Checking if build is running in Gitlab CI...
if not "%CI_PROJECT_DIR%" == "" set VSBUILDDIR=%CI_PROJECT_DIR%\VSBuild

set MSBUILD_ARGS=
set MSBUILD_ARGS=%MSBUILD_ARGS% /t:%TARGET%
set MSBUILD_ARGS=%MSBUILD_ARGS% /p:Platform=%PLATFORM%
set MSBUILD_ARGS=%MSBUILD_ARGS% /p:Configuration=%CONFIGURATION%
set MSBUILD_ARGS=%MSBUILD_ARGS% /maxcpucount:%MAXCPUCOUNT%
rem Checking if build is running in Gitlab CI...
if not "%CI_PROJECT_DIR%" == "" set MSBUILD_ARGS=%MSBUILD_ARGS% /p:OutDir=%CI_PROJECT_DIR%\%PLATFORM%\

if "_%VS_COMMON_TOOLS_DIR%_" == "__" set VS_COMMON_TOOLS_DIR=%VS150COMNTOOLS%
echo Loading VS tools from "%VS_COMMON_TOOLS_DIR%"...
call "%VS_COMMON_TOOLS_DIR%\VsDevCmd.bat"

echo Building solution %PROJECT_NAME%...
echo  - VSBUILDDIR   : %VSBUILDDIR% 
echo  - Platform     : %PLATFORM%
echo  - Configuration: %CONFIGURATION%
echo  - Target       : %TARGET%
echo  - MSBUILD_ARGS : %MSBUILD_ARGS%
msbuild.exe %PROJECT_PATH% %MSBUILD_ARGS%

goto final

:PlatformNotSet
echo Error : Platform not set
exit 1

:final
