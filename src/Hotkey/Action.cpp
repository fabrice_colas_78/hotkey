// Action.cpp: implementation of the CAction class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "global.h"
#include "Action.h"

#include <log.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAction::CAction()
{
	m_uActionId    = ACTION_OPEN_FILE;
	m_uSubActionId = s_misc_actions[0].uId;
	m_dwTimerValue = 30;
	m_fDesactivated = false;
}

CAction::~CAction()
{

}

void CAction::operator=(const CAction& action)
{
	//LOG1F("CAction::operator =\n");
	m_fDesactivated = action.m_fDesactivated;
	m_sProgram     = (const char *)action.m_sProgram;
	m_sURL         = (const char *)action.m_sURL;
	m_sURLFavorite = (const char *)action.m_sURLFavorite;
	m_sRASEntry    = (const char *)action.m_sRASEntry;
	m_sICAConnection= (const char *)action.m_sICAConnection;
	m_sScreenSaver = (const char *)action.m_sScreenSaver;
	m_sPasteText   = (const char *)action.m_sPasteText;
	m_sSearchEngine = (const char *)action.m_sSearchEngine;
	m_uActionId    = action.m_uActionId;
	m_uSubActionId = action.m_uSubActionId;
	m_dwTimerValue = action.m_dwTimerValue;
	m_sDisplayMessage = action.m_sDisplayMessage;
}

CString CAction::GetActionString() const
{
	CString sAction;
	switch(m_uActionId)
	{
	case ACTION_OPEN_FILE:
		sAction = "Ouvrir: ";
		sAction += m_sProgram;
		break;
	case ACTION_GOTO_URL:
		sAction = "URL: ";
		sAction += m_sURL;
		break;
	case ACTION_GOTO_URL_FAVORITE:
		sAction = "Favoris internet: ";
		sAction += m_sURLFavorite;
		break;
	case ACTION_SOUND:
		{
			sAction = "Son: ";
			for(int i=0;; i++)
			{
				if (s_volume[i].szLabel == NULL)
				{
					ASSERT(FALSE);
					break;
				}
				if (s_volume[i].uId == m_uSubActionId)
				{
					sAction += s_volume[i].szLabel;
					break;
				}
			}
		}
		break;
	case ACTION_RAS_DIALUP:
		sAction.Format("Con. dist.: %s", m_sRASEntry);
		break;
	case ACTION_ICA_CONNECTION:
		sAction.Format("Con. ICA.: %s", m_sICAConnection);
		break;
	case ACTION_SCREEN_SAVER:
		sAction.Format("Economiseur d'�cran: %s", m_sScreenSaver);
		break;
	case ACTION_MISC:
		{
			for(int i=0;; i++)
			{
				if (s_misc_actions[i].szLabel == NULL)
				{
					ASSERT(FALSE);
					break;
				}
				if (s_misc_actions[i].uId == m_uSubActionId)
				{
					sAction = s_misc_actions[i].szLabel;
					break;
				}
			}
		}
		break;
	case ACTION_TIMER:
		sAction.Format("Pause: %u seconde(s)", m_dwTimerValue);
		break;
	case ACTION_PASTE_TEXT:
		sAction.Format("Coller du texte: %s", m_sPasteText);
		break;
	case ACTION_WINAMP:
		sAction = "WinAmp: ";
		{
			for(int i=0;; i++)
			{
				if (s_winamp_actions[i].szLabel == NULL)
				{
					ASSERT(FALSE);
					break;
				}
				if (s_winamp_actions[i].uId == m_uSubActionId)
				{
					sAction += s_winamp_actions[i].szLabel;
					break;
				}
			}
		}
		break;
	case ACTION_SEARCH_ENGINE:
		sAction = "Moteur de recherche : ";
		sAction += m_sSearchEngine;
		break;
	case ACTION_DISPLAY_MESSAGE:
		sAction = "Afficher un message : ";
		{
			CString s;
			s = m_sDisplayMessage.Left(40);
			if (m_sDisplayMessage.GetLength() > 40)
				s += "[...]";
			s.Replace("\r\n", "<LF>");
			sAction += s;
		}
		break;
	default:
		ASSERT(FALSE);
		break;
	}
	ASSERT(sAction != "");
	return sAction;
}
