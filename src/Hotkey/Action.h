// Action.h: interface for the CAction class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ACTION_H__655F6703_85B9_4522_8F36_3B6EC439770D__INCLUDED_)
#define AFX_ACTION_H__655F6703_85B9_4522_8F36_3B6EC439770D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//	Main actions
#define ACTION_OPEN_FILE			0
#define ACTION_GOTO_URL				1
#define ACTION_GOTO_URL_FAVORITE	2

#define ACTION_SOUND			5
#define ACTION_RAS_DIALUP		6
#define ACTION_WINAMP			7
#define ACTION_ICA_CONNECTION	8
#define ACTION_SCREEN_SAVER		9
#define ACTION_MISC				10

#define ACTION_TIMER			15

#define ACTION_PASTE_TEXT		20

#define ACTION_SEARCH_ENGINE	30

#define ACTION_DISPLAY_MESSAGE	40


//	ACTION_MISC
#define A_MISC_EXPLORER				10

#define A_MISC_BROWSER_CLIENT		20
#define A_MISC_NEWSGROUP_CLIENT		21
#define A_MISC_GOTOURL				22

#define A_MISC_MAIL_CLIENT			30
#define A_MISC_MAIL_NEW_MESSAGE		31
#define A_MISC_MAIL_CONTACT			32
#define A_MISC_MAIL_CALENDAR		33
#define A_MISC_RECYCLE_BIN			34

#define A_MISC_MEDIA_CLIENT			50

#define A_MISC_TASK_MANAGER			60
#define A_MISC_MOUSERIGHTCLICK		67
#define A_MISC_MOUSELEFTCLICK		68
#define A_MISC_MOUSEMOVE			69

#define A_MISC_LOGOFF				70
#define A_MISC_POWEROFF				71
#define A_MISC_REBOOT				72
#define A_MISC_LOCK_WORKSTATION		73
#define A_MISC_SUSPEND				74
#define A_MISC_HIBERNATE			75

#define A_MISC_SF_MYDOCUMENTS		80
#define A_MISC_SF_DOCTEMPLATES		81
#define A_MISC_SF_MRD_LIST			82
#define A_MISC_SF_NETHOOD			83

//	ACTION_SOUND
#define SOUND_VOLUME_UP				90
#define SOUND_VOLUME_DOWN			91
#define SOUND_MUTE_ON				92
#define SOUND_MUTE_OFF				93
#define SOUND_MUTE_TOGGLE			94

//	ACTION_WINAMP
#define A_WINAMP_PLAY				100
#define A_WINAMP_STOP				101
#define A_WINAMP_PAUSE				102
#define A_WINAMP_NEXT_TRACK			103
#define A_WINAMP_PREVIOUS_TRACK		104
#define A_WINAMP_VOLUME_UP			105
#define A_WINAMP_VOLUME_DOWN		106

/*
Procedure d'ajout d'une action:
-------------------------------
- Ajout code commande: ex: ACTION_<type>
- Ajout texte de l'action dans GetActionString()
- Ajout code de recopie du(des) parametre(s) de l'action dans CAction::operator=()
- Ajout bouton radio dans Dialog/IDD_DIALOG_ACTION
- Ajout handler dans CActionPropertiesDlg sur le bouton radio
- Ajout code dans CActionDlg::EnableControls()
- Ajout code de retour dans CActionPropertiesDlg::GetActionType()
- Ajout code dans CActionPropertiesDlg::SetActionType()
- Ajout init variable dlg dans CActionPropertiesDlg::OnInitDialog (Dlg.var <- m_action)
- Ajout recup variable dlg dans CActionPropertiesDlg::OnOK() dans m_action (m_action <- Dlg.var)
- Ajout enregistrement et lecture depuis la registry (CRegistryConfig::SaveAction et CRegistryConfig::RestoreAction)
- Ajout code action dans CHotkeyDlg::ExecuteAction

*/

class CAction  
{
public:
	//static CString GetActionString(UINT uActionId, UINT uSubActionId, const char * szProgram, const char * szURL, const char * szRasEntry);
	CString GetActionString() const;
	void operator=(const CAction& action);
	CAction();
	virtual ~CAction();

	CString m_sProgram;
	CString m_sURL;
	CString m_sURLFavorite;
	CString m_sRASEntry;
	CString m_sICAConnection;
	CString m_sScreenSaver;
	CString m_sPasteText;
	CString m_sSearchEngine;
	UINT m_uActionId;
	UINT m_uSubActionId;
	DWORD m_dwTimerValue;
	CString m_sDisplayMessage;
	BOOL    m_fDesactivated;
};

#endif // !defined(AFX_ACTION_H__655F6703_85B9_4522_8F36_3B6EC439770D__INCLUDED_)
