#if !defined(AFX_ACTIONDLG_H__FB657524_2B19_4C77_8F4B_BC6B42EED3D0__INCLUDED_)
#define AFX_ACTIONDLG_H__FB657524_2B19_4C77_8F4B_BC6B42EED3D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ActionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CActionDlg dialog

#include "Global.h"
#include "Action.h"	// Added by ClassView

class CActionPropertiesDlg : public CDialog
{
// Construction
public:
	CAction m_action;
	CActionPropertiesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CActionPropertiesDlg)
	enum { IDD = IDD_DIALOG_ACTION };
	CComboBox	m_cbICAConnection;
	CComboBox	m_cbWinAmp;
	CComboBox	m_cbURLFavorites;
	CComboBox	m_cbRASDialUp;
	CComboBox	m_cbSound;
	CComboBox	m_cbMiscAction;
	CString	m_sProgram;
	CString	m_sURL;
	CString	m_sTimerValue;
	CString	m_sPasteText;
	CString	m_sURLFavorite;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CActionPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CActionPropertiesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioExecuteProgram();
	afx_msg void OnRadioMiscAction();
	afx_msg void OnRadioGotoUrl();
	afx_msg void OnRadioGotoUrlFavorite();
	afx_msg void OnButtonBrowseProgram();
	afx_msg void OnChangeEditProgram();
	afx_msg void OnChangeEditUrl();
	afx_msg void OnRadioSound();
	afx_msg void OnRadioRasDialup();
	virtual void OnOK();
	afx_msg void OnRadioTimerAction();
	afx_msg void OnChangeEditTimerValue();
	afx_msg void OnRadioPasteText();
	afx_msg void OnChangeEditPasteText();
	afx_msg void OnEditchangeComboUrlFavorite();
	afx_msg void OnSelchangeComboUrlFavorite();
	afx_msg void OnRadioWinamp();
	afx_msg void OnRadioIcaConnection();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void FillFavoritesURL(const char * szDir);
	void EnableControls();
	void FillCombos();
	UINT GetActionType();
	void SetActionType(UINT uActionId);
	UINT GetSubActionId();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTIONDLG_H__FB657524_2B19_4C77_8F4B_BC6B42EED3D0__INCLUDED_)
