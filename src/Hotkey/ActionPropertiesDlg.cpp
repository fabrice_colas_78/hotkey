// ActionPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Hotkey.h"
#include "ActionPropertiesDlg.h"

#include "Global.h"
#include <RasDial.h>
#include <SpecialFolder.h>
#include <MacrosDef.h>
#include <CommonLib.h>
#include <log.h>
#include <inifile.h>

#include <MyFileDialog.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CActionPropertiesDlg dialog

CActionPropertiesDlg::CActionPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CActionPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CActionPropertiesDlg)
	m_sProgram = _T("");
	m_sURL = _T("");
	m_sTimerValue = _T("");
	m_sPasteText = _T("");
	m_sURLFavorite = _T("");
	m_sSearchEngine = _T("");
	m_sDisplayMessage = _T("");
	m_fDesactivated = FALSE;
	//}}AFX_DATA_INIT
}


void CActionPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CActionPropertiesDlg)
	DDX_Control(pDX, IDC_COMBO_SEARCH_INTERNET_ACTION, m_cbSearchEngine);
	DDX_Control(pDX, IDC_COMBO_ICA_CONNECTION, m_cbICAConnection);
	DDX_Control(pDX, IDC_COMBO_SCREEN_SAVER, m_cbScreenSaver);
	DDX_Control(pDX, IDC_COMBO_WINAMP, m_cbWinAmp);
	DDX_Control(pDX, IDC_COMBO_URL_FAVORITE, m_cbURLFavorites);
	DDX_Control(pDX, IDC_COMBO_RAS_DIALUP, m_cbRASDialUp);
	DDX_Control(pDX, IDC_COMBO_SOUND, m_cbSound);
	DDX_Control(pDX, IDC_COMBO_MISC_ACTION, m_cbMiscAction);
	DDX_Text(pDX, IDC_EDIT_PROGRAM, m_sProgram);
	DDX_Text(pDX, IDC_EDIT_URL, m_sURL);
	DDX_Text(pDX, IDC_EDIT_TIMER_VALUE, m_sTimerValue);
	DDX_Text(pDX, IDC_EDIT_PASTE_TEXT, m_sPasteText);
	DDX_CBString(pDX, IDC_COMBO_URL_FAVORITE, m_sURLFavorite);
	DDX_CBString(pDX, IDC_COMBO_SEARCH_INTERNET_ACTION, m_sSearchEngine);
	DDX_Text(pDX, IDC_EDIT_DISPLAY_MESSAGE, m_sDisplayMessage);
	DDX_Check(pDX, IDC_CHECK_DESACTIVATED, m_fDesactivated);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CActionPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CActionPropertiesDlg)
	ON_BN_CLICKED(IDC_RADIO_OPEN_FILE, OnRadioExecuteProgram)
	ON_BN_CLICKED(IDC_RADIO_MISC_ACTION, OnRadioMiscAction)
	ON_BN_CLICKED(IDC_RADIO_GOTO_URL, OnRadioGotoUrl)
	ON_BN_CLICKED(IDC_RADIO_GOTO_URL_FAVORITE, OnRadioGotoUrlFavorite)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_PROGRAM, OnButtonBrowseProgram)
	ON_EN_CHANGE(IDC_EDIT_PROGRAM, OnChangeEditProgram)
	ON_EN_CHANGE(IDC_EDIT_URL, OnChangeEditUrl)
	ON_BN_CLICKED(IDC_RADIO_SOUND, OnRadioSound)
	ON_BN_CLICKED(IDC_RADIO_RAS_DIALUP, OnRadioRasDialup)
	ON_BN_CLICKED(IDC_RADIO_TIMER_ACTION, OnRadioTimerAction)
	ON_EN_CHANGE(IDC_EDIT_TIMER_VALUE, OnChangeEditTimerValue)
	ON_BN_CLICKED(IDC_RADIO_PASTE_TEXT, OnRadioPasteText)
	ON_EN_CHANGE(IDC_EDIT_PASTE_TEXT, OnChangeEditPasteText)
	ON_CBN_EDITCHANGE(IDC_COMBO_URL_FAVORITE, OnEditchangeComboUrlFavorite)
	ON_CBN_SELCHANGE(IDC_COMBO_URL_FAVORITE, OnSelchangeComboUrlFavorite)
	ON_BN_CLICKED(IDC_RADIO_WINAMP, OnRadioWinamp)
	ON_BN_CLICKED(IDC_RADIO_ICA_CONNECTION, OnRadioIcaConnection)
	ON_BN_CLICKED(IDC_RADIO_SCREEN_SAVER, OnRadioScreenSaver)
	ON_BN_CLICKED(IDC_RADIO_SEARCH_INTERNET_ACTION, OnRadioSearchInternetAction)
	ON_EN_CHANGE(IDC_EDIT_DISPLAY_MESSAGE, OnChangeEditDisplayMessage)
	ON_BN_CLICKED(IDC_RADIO_DISPLAY_MESSAGE, OnRadioDisplayMessage)
	ON_BN_CLICKED(IDC_CHECK_DESACTIVATED, OnCheckDesactivated)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CActionPropertiesDlg message handlers

BOOL CActionPropertiesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	FillCombos();
	FillComboSearchInternet();

	SetActionType(m_action.m_uActionId);
	m_sURL         = m_action.m_sURL;
	m_sURLFavorite = m_action.m_sURLFavorite;
	m_sProgram     = m_action.m_sProgram;
	m_sPasteText   = m_action.m_sPasteText;
	m_sSearchEngine = m_action.m_sSearchEngine;
	m_sDisplayMessage = m_action.m_sDisplayMessage;

	m_sTimerValue.Format("%u", m_action.m_dwTimerValue);

	m_fDesactivated = m_action.m_fDesactivated;

#ifndef _DEBUG
	GetDlgItem(IDC_RADIO_PASTE_TEXT)->EnableWindow(false);
#endif
	UpdateData(FALSE);
	EnableControls();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CActionPropertiesDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	m_action.m_uActionId    = GetActionType();
	m_action.m_uSubActionId = GetSubActionId();
	m_action.m_sProgram     = m_sProgram;
	m_action.m_sURL         = m_sURL;
	m_action.m_sURLFavorite = m_sURLFavorite;
	m_action.m_dwTimerValue = atoi(m_sTimerValue);
	m_action.m_sPasteText   = m_sPasteText;
	m_action.m_sSearchEngine = m_sSearchEngine;
	m_action.m_sDisplayMessage = m_sDisplayMessage;

	if (m_cbRASDialUp.GetCurSel() != CB_ERR)
		m_cbRASDialUp.GetLBText(m_cbRASDialUp.GetCurSel(), m_action.m_sRASEntry);
	if (m_cbICAConnection.GetCurSel() != CB_ERR)
		m_cbICAConnection.GetLBText(m_cbICAConnection.GetCurSel(), m_action.m_sICAConnection);
	if (m_cbScreenSaver.GetCurSel() != CB_ERR)
		m_cbScreenSaver.GetLBText(m_cbScreenSaver.GetCurSel(), m_action.m_sScreenSaver);

	m_action.m_fDesactivated = m_fDesactivated;

	//SaveComboSearchInternet();
	CDialog::OnOK();
}

void CActionPropertiesDlg::FillComboSearchInternet()
{
	LPCTSTR motors[] = 
	{
		"http://www.google.com",
		"http://www.yahoo.com",
		"http://www.voila.fr",
	};

	for(int i=0; i<sizeof(motors)/sizeof(motors[0]); i++)
	{
		m_cbSearchEngine.AddString(motors[i]);
	}
}

void CActionPropertiesDlg::FillFavoritesURL(const char * szSubDir)
{
	CString sDir;
	CSpecialFolder sf;
	if (strlen(szSubDir) > 0)
	{
		sDir = sf.Get(CSpecialFolder::Favorites) + "\\" + szSubDir + "\\*.*";
	}
	else
		sDir = sf.Get(CSpecialFolder::Favorites) + "\\*.*";
	LOG3F("sDir='%s'\n", sDir);

	WIN32_FIND_DATA Win32FD;
	HANDLE hFFF = FindFirstFile(sDir, &Win32FD);
	if (hFFF == INVALID_HANDLE_VALUE) return;
	for(;;)
	{
		if (! IS_WIN32FD_CURRENT_OR_PARENT_FOLDER(Win32FD))
		{
			if (IS_WIN32FD_DIRECTORY(Win32FD))
			{
				CString sSubDir = szSubDir;
				if (strlen(szSubDir) > 0) sSubDir += "\\";
				sSubDir += Win32FD.cFileName;
				LOG3F("SubDir = '%s'\n", sSubDir);
				FillFavoritesURL(sSubDir);
			}
			else
			{
				if (! _tcsicmp(GetFileNameComposant(Win32FD.cFileName, GFNC_EXT), "url"))
				{
					CString sURLName = szSubDir;
					if (strlen(szSubDir) > 0) sURLName += "\\";
					sURLName += GFNC(Win32FD.cFileName, GFNC_FILENAME);
					m_cbURLFavorites.AddString(sURLName);
				}
			}
		}
		if (! FindNextFile(hFFF, &Win32FD)) break;
	}
	FindClose(hFFF);
}

void GetScreenSaversList(CStringArray& saScreenSavers)
{
	HANDLE hFFF = INVALID_HANDLE_VALUE;
	CString sSys32;
	sSys32.Format("%s\\system32\\*.scr", GetEnv("SYSTEMROOT"));
	WIN32_FIND_DATA Win32FD;
	hFFF = ::FindFirstFile(sSys32, &Win32FD);
	if (hFFF == INVALID_HANDLE_VALUE) return;
	for(;;)
	{
		saScreenSavers.Add(Win32FD.cFileName);
		if (! FindNextFile(hFFF, &Win32FD)) break;
	}
	FindClose(hFFF);
}

void CActionPropertiesDlg::FillCombos()
{
	int i;

	//	Volume
	for(i=0;; i++)
	{
		if (s_volume[i].szLabel == NULL) break;
		m_cbSound.AddString(s_volume[i].szLabel);
		if (s_volume[i].uId == m_action.m_uSubActionId)
		{
			m_cbSound.SetCurSel(i);
		}
	}
	if (m_cbSound.GetCurSel() == CB_ERR) m_cbSound.SetCurSel(0);

	//	RAS Entry
	RASENTRYNAME * pRasEntryName;
	DWORD dwNumberOfEntries;
	if (CRasDial::EnumEntries(&pRasEntryName, dwNumberOfEntries))
	{
		for (DWORD dwCount = 0; dwCount < dwNumberOfEntries; dwCount++)
		{
			BOOL fPassword;
			RASDIALPARAMS RasDialParams;
			if (CRasDial::GetEntryDialParams(pRasEntryName[dwCount].szEntryName, &RasDialParams, &fPassword))
			{
				CString sEntryName = pRasEntryName[dwCount].szEntryName;
				int nIdAdd = m_cbRASDialUp.AddString(sEntryName);
				if (m_action.m_sRASEntry == sEntryName)
				{
					m_cbRASDialUp.SetCurSel(nIdAdd);
				}
			}
		}
		CRasDial::MemoryFree (pRasEntryName);
	}
	if (m_cbRASDialUp.GetCurSel() == CB_ERR) m_cbRASDialUp.SetCurSel(0);

	//	ICA Connections
	CSpecialFolder sf;
	CString sAppSrvIni = ConcatFileNameComposant(sf.Get(CSIDL_APPDATA), "ICAClient");
	sAppSrvIni = ConcatFileNameComposant(sAppSrvIni, "APPSRV.INI");
	CIniFile IniFile(sAppSrvIni);
	CIniSection isAppServers(IniFile, "ApplicationServers");
	for(WORD w=0; w<isAppServers.CountKeys(); w++)
	{
		CString sICAConnection;
		isAppServers.EnumKey(w, sICAConnection.GetBuffer(100), 100);
		int nIdAdd = m_cbICAConnection.AddString(sICAConnection);
		if (m_action.m_sICAConnection == sICAConnection)
		{
			m_cbICAConnection.SetCurSel(nIdAdd);
		}
	}
	if (m_cbICAConnection.GetCurSel() == CB_ERR) m_cbICAConnection.SetCurSel(0);

	//	Screen saver
	CStringArray saScreenSavers;
	GetScreenSaversList(saScreenSavers);
	for(int i=0; i<saScreenSavers.GetSize(); i++)
	{
		int nIdAdd = m_cbScreenSaver.AddString(saScreenSavers[i]);
		if (m_action.m_sScreenSaver == saScreenSavers[i])
		{
			m_cbScreenSaver.SetCurSel(nIdAdd);
		}
	}
	if (m_cbScreenSaver.GetCurSel() == CB_ERR) m_cbScreenSaver.SetCurSel(0);

	//	WinAmp
	for(int i=0;; i++)
	{
		if (s_winamp_actions[i].szLabel == NULL) break;
		m_cbWinAmp.AddString(s_winamp_actions[i].szLabel);
		if (s_winamp_actions[i].uId == m_action.m_uSubActionId)
		{
			m_cbWinAmp.SetCurSel(i);
		}
	}
	if (m_cbWinAmp.GetCurSel() == CB_ERR) m_cbWinAmp.SetCurSel(0);

	//	Miscellaneous actions
	for(int i=0;; i++)
	{
		if (s_misc_actions[i].szLabel == NULL) break;
		m_cbMiscAction.AddString(s_misc_actions[i].szLabel);
		if (s_misc_actions[i].uId == m_action.m_uSubActionId)
		{
			m_cbMiscAction.SetCurSel(i);
		}
	}
	if (m_cbMiscAction.GetCurSel() == CB_ERR) m_cbMiscAction.SetCurSel(0);

	//	URL Favorites
	FillFavoritesURL("");
	if (m_cbURLFavorites.GetCurSel() == CB_ERR) m_cbURLFavorites.SetCurSel(0);
}

void CActionPropertiesDlg::SetActionType(UINT uActionId)
{
	//((CButton *)GetDlgItem(IDC_RADIO_OPEN_FILE))->SetCheck(FALSE);
	//((CButton *)GetDlgItem(IDC_RADIO_GOTO_URL))->SetCheck(FALSE);
	//((CButton *)GetDlgItem(IDC_RADIO_MISC_ACTION))->SetCheck(FALSE);
	switch(uActionId)
	{
	case ACTION_OPEN_FILE:
		((CButton *)GetDlgItem(IDC_RADIO_OPEN_FILE))->SetCheck(TRUE);
		break;
	case ACTION_GOTO_URL:
		((CButton *)GetDlgItem(IDC_RADIO_GOTO_URL))->SetCheck(TRUE);
		break;
	case ACTION_GOTO_URL_FAVORITE:
		((CButton *)GetDlgItem(IDC_RADIO_GOTO_URL_FAVORITE))->SetCheck(TRUE);
		break;
	case ACTION_SOUND:
		((CButton *)GetDlgItem(IDC_RADIO_SOUND))->SetCheck(TRUE);
		break;
	case ACTION_RAS_DIALUP:
		((CButton *)GetDlgItem(IDC_RADIO_RAS_DIALUP))->SetCheck(TRUE);
		break;
	case ACTION_ICA_CONNECTION:
		((CButton *)GetDlgItem(IDC_RADIO_ICA_CONNECTION))->SetCheck(TRUE);
		break;
	case ACTION_SCREEN_SAVER:
		((CButton *)GetDlgItem(IDC_RADIO_SCREEN_SAVER))->SetCheck(TRUE);
		break;
	case ACTION_MISC:
		((CButton *)GetDlgItem(IDC_RADIO_MISC_ACTION))->SetCheck(TRUE);
		break;
	case ACTION_TIMER:
		((CButton *)GetDlgItem(IDC_RADIO_TIMER_ACTION))->SetCheck(TRUE);
		break;
	case ACTION_PASTE_TEXT:
		((CButton *)GetDlgItem(IDC_RADIO_PASTE_TEXT))->SetCheck(TRUE);
		break;
	case ACTION_WINAMP:
		((CButton *)GetDlgItem(IDC_RADIO_WINAMP))->SetCheck(TRUE);
		break;
	case ACTION_SEARCH_ENGINE:
		((CButton *)GetDlgItem(IDC_RADIO_SEARCH_INTERNET_ACTION))->SetCheck(TRUE);
		break;
	case ACTION_DISPLAY_MESSAGE:
		((CButton *)GetDlgItem(IDC_RADIO_DISPLAY_MESSAGE))->SetCheck(TRUE);
		break;
	default:
		ASSERT(FALSE);
		((CButton *)GetDlgItem(IDC_RADIO_OPEN_FILE))->SetCheck(TRUE);
	}
	EnableControls();
}

UINT CActionPropertiesDlg::GetActionType()
{
	if (((CButton *)GetDlgItem(IDC_RADIO_OPEN_FILE))->GetState() & 0x0003) return ACTION_OPEN_FILE;
	if (((CButton *)GetDlgItem(IDC_RADIO_GOTO_URL))->GetState() & 0x0003) return ACTION_GOTO_URL;
	if (((CButton *)GetDlgItem(IDC_RADIO_GOTO_URL_FAVORITE))->GetState() & 0x0003) return ACTION_GOTO_URL_FAVORITE;
	if (((CButton *)GetDlgItem(IDC_RADIO_SOUND))->GetState() & 0x0003) return ACTION_SOUND;
	if (((CButton *)GetDlgItem(IDC_RADIO_RAS_DIALUP))->GetState() & 0x0003) return ACTION_RAS_DIALUP;
	if (((CButton *)GetDlgItem(IDC_RADIO_ICA_CONNECTION))->GetState() & 0x0003) return ACTION_ICA_CONNECTION;
	if (((CButton *)GetDlgItem(IDC_RADIO_SCREEN_SAVER))->GetState() & 0x0003) return ACTION_SCREEN_SAVER;
	if (((CButton *)GetDlgItem(IDC_RADIO_MISC_ACTION))->GetState() & 0x0003) return ACTION_MISC;
	if (((CButton *)GetDlgItem(IDC_RADIO_TIMER_ACTION))->GetState() & 0x0003) return ACTION_TIMER;
	if (((CButton *)GetDlgItem(IDC_RADIO_PASTE_TEXT))->GetState() & 0x0003) return ACTION_PASTE_TEXT;
	if (((CButton *)GetDlgItem(IDC_RADIO_WINAMP))->GetState() & 0x0003) return ACTION_WINAMP;
	if (((CButton *)GetDlgItem(IDC_RADIO_SEARCH_INTERNET_ACTION))->GetState() & 0x0003) return ACTION_SEARCH_ENGINE;
	if (((CButton *)GetDlgItem(IDC_RADIO_DISPLAY_MESSAGE))->GetState() & 0x0003) return ACTION_DISPLAY_MESSAGE;
	return ACTION_OPEN_FILE;
}

UINT CActionPropertiesDlg::GetSubActionId()
{
	switch(GetActionType())
	{
	case ACTION_SOUND:
		ASSERT(m_cbSound.GetCurSel() != -1);
		return s_volume[m_cbSound.GetCurSel()].uId;
	case ACTION_MISC:
		ASSERT(m_cbMiscAction.GetCurSel() != -1);
		return s_misc_actions[m_cbMiscAction.GetCurSel()].uId;
	case ACTION_WINAMP:
		ASSERT(m_cbWinAmp.GetCurSel() != -1);
		return s_winamp_actions[m_cbWinAmp.GetCurSel()].uId;
		break;
	default:
		return 0;
	}
}

void CActionPropertiesDlg::OnButtonBrowseProgram() 
{
	// TODO: Add your control notification handler code here
	CMyFileDialog Dlg(this, TRUE, "Tous les fichiers|*.*||", "");
	
	if (Dlg.DoModal() == IDOK)
	{
		UpdateData(TRUE);
		m_sProgram = Dlg.m_ofn.lpstrFile;
		UpdateData(FALSE);
		EnableControls();
	}
}

void CActionPropertiesDlg::EnableControls()
{
	UpdateData(TRUE);

	int nTabCtrlId[] = 
	{
		IDC_RADIO_OPEN_FILE,
		IDC_RADIO_GOTO_URL,
		IDC_RADIO_GOTO_URL_FAVORITE,
		IDC_RADIO_SOUND,
		IDC_RADIO_WINAMP,
		IDC_RADIO_RAS_DIALUP,
		IDC_RADIO_ICA_CONNECTION,
		IDC_RADIO_TIMER_ACTION,
		IDC_RADIO_PASTE_TEXT,
		IDC_RADIO_DISPLAY_MESSAGE,
		IDC_RADIO_SCREEN_SAVER,
		IDC_RADIO_SEARCH_INTERNET_ACTION,
		IDC_RADIO_MISC_ACTION,
	};
	for(int i=0; i<sizeof(nTabCtrlId)/sizeof(nTabCtrlId[0]); i++)
	{
		GetDlgItem(nTabCtrlId[i])->EnableWindow(!m_fDesactivated);
	}

	int nActionType = GetActionType();

	//	Action type
	GetDlgItem(IDC_EDIT_PROGRAM)->EnableWindow(nActionType == ACTION_OPEN_FILE && !m_fDesactivated);
	GetDlgItem(IDC_BUTTON_BROWSE_PROGRAM)->EnableWindow(nActionType == ACTION_OPEN_FILE && !m_fDesactivated);
	GetDlgItem(IDC_EDIT_URL)->EnableWindow(nActionType == ACTION_GOTO_URL && !m_fDesactivated);
	GetDlgItem(IDC_COMBO_SOUND)->EnableWindow(nActionType == ACTION_SOUND && !m_fDesactivated);
	GetDlgItem(IDC_COMBO_RAS_DIALUP)->EnableWindow(nActionType == ACTION_RAS_DIALUP && !m_fDesactivated);
	GetDlgItem(IDC_COMBO_ICA_CONNECTION)->EnableWindow(nActionType == ACTION_ICA_CONNECTION && !m_fDesactivated);
	GetDlgItem(IDC_COMBO_SCREEN_SAVER)->EnableWindow(nActionType == ACTION_SCREEN_SAVER && !m_fDesactivated);
	GetDlgItem(IDC_COMBO_MISC_ACTION)->EnableWindow(nActionType == ACTION_MISC && !m_fDesactivated);
	GetDlgItem(IDC_COMBO_URL_FAVORITE)->EnableWindow(nActionType == ACTION_GOTO_URL_FAVORITE && !m_fDesactivated);
	GetDlgItem(IDC_COMBO_WINAMP)->EnableWindow(nActionType == ACTION_WINAMP && !m_fDesactivated);
	GetDlgItem(IDC_EDIT_TIMER_VALUE)->EnableWindow(nActionType == ACTION_TIMER && !m_fDesactivated);
	GetDlgItem(IDC_EDIT_PASTE_TEXT)->EnableWindow(nActionType == ACTION_PASTE_TEXT && !m_fDesactivated);
	GetDlgItem(IDC_COMBO_SEARCH_INTERNET_ACTION)->EnableWindow(nActionType == ACTION_SEARCH_ENGINE && !m_fDesactivated);
	GetDlgItem(IDC_EDIT_DISPLAY_MESSAGE)->EnableWindow(nActionType == ACTION_DISPLAY_MESSAGE && !m_fDesactivated);

	BOOL fEnIDOK = TRUE;
	switch(nActionType)
	{
	case ACTION_OPEN_FILE:
		fEnIDOK = fEnIDOK && m_sProgram != "";
		break;
	case ACTION_GOTO_URL:
		fEnIDOK = fEnIDOK && m_sURL != "";
		break;
	case ACTION_RAS_DIALUP:
		fEnIDOK = fEnIDOK && m_cbRASDialUp.GetCurSel() != CB_ERR;
		break;
	case ACTION_ICA_CONNECTION:
		fEnIDOK = fEnIDOK && m_cbICAConnection.GetCurSel() != CB_ERR;
		break;
	case ACTION_SCREEN_SAVER:
		fEnIDOK = fEnIDOK && m_cbScreenSaver.GetCurSel() != CB_ERR;
		break;
	case ACTION_TIMER:
		fEnIDOK = fEnIDOK && m_sTimerValue != "";
		break;
	case ACTION_PASTE_TEXT:
		fEnIDOK = fEnIDOK && m_sPasteText != "";
		break;
	case ACTION_GOTO_URL_FAVORITE:
		fEnIDOK = fEnIDOK && m_sURLFavorite != "";
		break;
	case ACTION_DISPLAY_MESSAGE:
		fEnIDOK = fEnIDOK && m_sDisplayMessage != "";
		break;
	}
	GetDlgItem(IDOK)->EnableWindow(fEnIDOK);
}

void CActionPropertiesDlg::OnRadioExecuteProgram() 
{
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CActionPropertiesDlg::OnRadioMiscAction() 
{
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CActionPropertiesDlg::OnRadioGotoUrl() 
{
	// TODO: Add your control notification handler code here
	EnableControls();
}


void CActionPropertiesDlg::OnRadioSound() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnRadioRasDialup() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnRadioTimerAction() 
{
	// TODO: Add your control notification handler code here
	EnableControls();
}
void CActionPropertiesDlg::OnChangeEditProgram() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CActionPropertiesDlg::OnChangeEditUrl() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CActionPropertiesDlg::OnChangeEditTimerValue() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnRadioPasteText() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnChangeEditPasteText() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CActionPropertiesDlg::OnRadioGotoUrlFavorite() 
{
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CActionPropertiesDlg::OnEditchangeComboUrlFavorite() 
{
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CActionPropertiesDlg::OnSelchangeComboUrlFavorite() 
{
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CActionPropertiesDlg::OnRadioWinamp() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnRadioIcaConnection() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnRadioScreenSaver() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnRadioSearchInternetAction() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnChangeEditDisplayMessage() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnRadioDisplayMessage() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CActionPropertiesDlg::OnCheckDesactivated() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}
