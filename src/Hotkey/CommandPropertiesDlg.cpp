// CommandPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "CommandPropertiesDlg.h"

#include "ActionPropertiesDlg.h"
#include "HotkeyDlg.h"
#include "HotkeyPropertiesDlg.h"
#include "TaskSchedulingDlg.h"

#include "Global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define LV_ACTION_INSERT_COLUMN()		m_lvActions.InsertColumn(0, "Actions � �x�cuter", LVCFMT_LEFT, 378);
#define LV_ACTION_INSERT_ITEM(pos,act)	{ int nItem = m_lvActions.InsertItem(pos, act->GetActionString(), !act->m_fDesactivated?m_nImageOn:m_nImageOff); \
												m_lvActions.SetItemData(nItem, (DWORD_PTR)act); \
											}
#define LV_ACTION_SET_ITEM(index,act)	{ m_lvActions.SetItemText(index, 0, act->GetActionString()); \
												LVITEM lvItem; lvItem.mask = LVIF_IMAGE; lvItem.iItem = index; \
												lvItem.iSubItem = 0; lvItem.iImage = !act->m_fDesactivated?m_nImageOn:m_nImageOff; \
												m_lvActions.SetItem(&lvItem); \
												m_lvActions.SetItemData(index, (DWORD_PTR)act); \
											}
#define LV_ACTION_GET_ACTION_PTR(index)	(CAction *)m_lvActions.GetItemData(index)
/*
#define LV_ACTION_GET_ITEM(lv,index,desc,phk)	{ desc = lv.GetItemText(index, 0); \
											phk = (CHotkey *)lv.GetItemData(index); \
											}
*/

#define LV_HOTKEY_INSERT_COLUMN()		m_lvHotkeys.InsertColumn(0, "S�quence de touches", LVCFMT_LEFT, 355);
#define LV_HOTKEY_INSERT_ITEM(pos,hk)	{ int nItem = m_lvHotkeys.InsertItem(pos, hk->GetHotkeyString()); \
												m_lvHotkeys.SetItemData(nItem, (DWORD_PTR)hk); \
											}
#define LV_HOTKEY_SET_ITEM(index,hk)	{ m_lvHotkeys.SetItemText(index, 0, hk->GetHotkeyString()); \
												m_lvHotkeys.SetItemData(index, (DWORD_PTR)hk); \
											}
#define LV_HOTKEY_GET_HOTKEY_PTR(index)	(CHotkey *)m_lvHotkeys.GetItemData(index)

#define LV_SCHEDUL_INSERT_COLUMN()		m_lvSchedulers.InsertColumn(0, "Planifications", LVCFMT_LEFT, 355);
#define LV_SCHEDUL_INSERT_ITEM(pos,st)	{ int nItem = m_lvSchedulers.InsertItem(pos, st->GetScheduledTaskString()); \
												m_lvSchedulers.SetItemData(nItem, (DWORD_PTR)st); \
											}
#define LV_SCHEDUL_SET_ITEM(index,st)	{ m_lvSchedulers.SetItemText(index, 0, st->GetScheduledTaskString()); \
												m_lvSchedulers.SetItemData(index, (DWORD_PTR)st); \
											}
#define LV_SCHEDUL_GET_SCHEDUL_PTR(index)	(CScheduledTask *)m_lvSchedulers.GetItemData(index)


/////////////////////////////////////////////////////////////////////////////
// CCommandPropertiesDlg dialog


CCommandPropertiesDlg::CCommandPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCommandPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCommandPropertiesDlg)
	m_fActive = FALSE;
	m_sDescription = _T("");
	m_fExecutionMinimized = FALSE;
	//}}AFX_DATA_INIT
	m_pLVActionsImageListSmall = new CImageList();
}

CCommandPropertiesDlg::~CCommandPropertiesDlg()
{
	if (m_pLVActionsImageListSmall) delete m_pLVActionsImageListSmall;
}


void CCommandPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCommandPropertiesDlg)
	DDX_Control(pDX, IDC_LIST_SCHEDULER, m_lvSchedulers);
	DDX_Control(pDX, IDC_LIST_HOTKEYS, m_lvHotkeys);
	DDX_Control(pDX, IDC_LIST_ACTIONS, m_lvActions);
	DDX_Check(pDX, IDC_CHECK_ACTIVATE, m_fActive);
	DDX_Text(pDX, IDC_EDIT_DESCRIPTION, m_sDescription);
	DDX_Check(pDX, IDC_CHECK_EXECUTION_MINIMIZED, m_fExecutionMinimized);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCommandPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CCommandPropertiesDlg)
	ON_EN_CHANGE(IDC_EDIT_DESCRIPTION, OnChangeEditDescription)
	ON_BN_CLICKED(IDC_BUTTON_ADD_ACTION, OnButtonAddAction)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_ACTIONS, OnItemchangedListActions)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_ACTION, OnButtonEditAction)
	ON_BN_CLICKED(IDC_BUTTON_DEL_ACTION, OnButtonDelAction)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_ACTIONS, OnDblclkListActions)
	ON_BN_CLICKED(IDC_BUTTON_ACTION_DOWN, OnButtonActionDown)
	ON_BN_CLICKED(IDC_BUTTON_ACTION_UP, OnButtonActionUp)
	ON_BN_CLICKED(IDC_BUTTON_ADD_HOTKEY, OnButtonAddHotkey)
	ON_BN_CLICKED(IDC_BUTTON_DEL_HOTKEY, OnButtonDelHotkey)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOTKEYS, OnDblclkListHotkeys)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_HOTKEYS, OnItemchangedListHotkeys)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_HOTKEY, OnButtonEditHotkey)
	ON_BN_CLICKED(IDC_BUTTON_ADD_SCHEDULER, OnButtonAddScheduler)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_SCHEDULER, OnButtonEditScheduler)
	ON_BN_CLICKED(IDC_BUTTON_DEL_SCHEDULER, OnButtonDelScheduler)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SCHEDULER, OnDblclkListScheduler)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_SCHEDULER, OnItemchangedListScheduler)
	ON_BN_CLICKED(IDC_BUTTON_EXECUTE_ACTION, OnButtonExecuteAction)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommandPropertiesDlg message handlers

BOOL CCommandPropertiesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	LV_ACTION_INSERT_COLUMN();
	m_lvActions.SetHighlightType(HIGHLIGHT_ALLCOLUMNS);
	
	if (m_pLVActionsImageListSmall)
	{
		m_pLVActionsImageListSmall->Create(16, 16, 1|ILC_COLOR16, 0, 10);
		m_nImageOn      = m_pLVActionsImageListSmall->Add(AfxGetApp()->LoadIcon(IDI_ICON_ON));
		m_nImageOff     = m_pLVActionsImageListSmall->Add(AfxGetApp()->LoadIcon(IDI_ICON_OFF));
		m_lvActions.SetImageList(m_pLVActionsImageListSmall, LVSIL_SMALL);
	}

	
	LV_HOTKEY_INSERT_COLUMN();
	m_lvHotkeys.SetHighlightType(HIGHLIGHT_ALLCOLUMNS);

	LV_SCHEDUL_INSERT_COLUMN();
	m_lvSchedulers.SetHighlightType(HIGHLIGHT_ALLCOLUMNS);

	FillActions();
	FillHotkeys();
	FillScheduledTasks();
	
	//	A placer apres FillActions sinon la chaine est remise a zero.
	m_sDescription = (const char *)m_hkcommand.m_sDescription;
	m_fActive      = m_hkcommand.m_fActive;
	m_fExecutionMinimized = m_hkcommand.m_fExecutionMinimized;

	UpdateData(FALSE);
	EnableControls();

#ifdef _DEBUG0
	CAction * pAction;
	pAction = new CAction;
	pAction->m_uActionId = ACTION_VOLUME;
	pAction->m_uSubActionId = VOLUME_MUTE_ON;
	LV_ACTION_INSERT_ITEM(100000, pAction);

	pAction = new CAction;
	pAction->m_uActionId = ACTION_OTHER;
	pAction->m_uSubActionId = EXECUTE_MEDIA_CLIENT;
	LV_ACTION_INSERT_ITEM(100000, pAction);

	pAction = new CAction;
	pAction->m_uActionId = ACTION_OTHER;
	pAction->m_uSubActionId = EXECUTE_MAIL_CLIENT;
	LV_ACTION_INSERT_ITEM(100000, pAction);
#endif
#ifdef _DEBUG
#endif
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCommandPropertiesDlg::EnableControls()
{
	UpdateData(TRUE);

	int nActionSelCount = m_lvActions.GetSelectedCount();
	int nActionSelected = m_lvActions.GetFirstSelectedItem();
	int nActionCount    = m_lvActions.GetItemCount();
	GetDlgItem(IDC_BUTTON_DEL_ACTION)->EnableWindow(nActionSelCount > 0);
	GetDlgItem(IDC_BUTTON_EDIT_ACTION)->EnableWindow(nActionSelCount == 1);
	GetDlgItem(IDC_BUTTON_EXECUTE_ACTION)->EnableWindow(nActionSelCount == 1);
	GetDlgItem(IDC_BUTTON_ACTION_UP)->EnableWindow(nActionSelCount == 1 && nActionSelected > 0);
	GetDlgItem(IDC_BUTTON_ACTION_DOWN)->EnableWindow(nActionSelCount == 1 && nActionSelected < (nActionCount-1));
	
	int nHotkeySelCount = m_lvHotkeys.GetSelectedCount();
	GetDlgItem(IDC_BUTTON_DEL_HOTKEY)->EnableWindow(nHotkeySelCount > 0);
	GetDlgItem(IDC_BUTTON_EDIT_HOTKEY)->EnableWindow(nHotkeySelCount == 1);

	int nTaskSchedSelCount = m_lvSchedulers.GetSelectedCount();
	GetDlgItem(IDC_BUTTON_DEL_SCHEDULER)->EnableWindow(nTaskSchedSelCount > 0);
	GetDlgItem(IDC_BUTTON_EDIT_SCHEDULER)->EnableWindow(nTaskSchedSelCount == 1);

	BOOL fEnIDOK = m_sDescription != "";
	fEnIDOK = fEnIDOK && (m_lvActions.GetItemCount() > 0);
	GetDlgItem(IDOK)->EnableWindow(fEnIDOK);
}

void CCommandPropertiesDlg::CleanMemory()
{
	for(int i=0; i<m_lvActions.GetItemCount(); i++)
	{
		CAction * pAction = (CAction *)m_lvActions.GetItemData(i);
		delete pAction;
	}
	for(int i=0; i<m_lvHotkeys.GetItemCount(); i++)
	{
		CHotkey * pHotkey = (CHotkey *)m_lvHotkeys.GetItemData(i);
		delete pHotkey;
	}
	for(int i=0; i<m_lvSchedulers.GetItemCount(); i++)
	{
		CScheduledTask * pScheduledTask = (CScheduledTask *)m_lvSchedulers.GetItemData(i);
		delete pScheduledTask;
	}
}

void CCommandPropertiesDlg::OnCancel()
{
	// TODO: Add extra cleanup here
	CleanMemory();
	CDialog::OnCancel();
}

void CCommandPropertiesDlg::OnOK()
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	m_hkcommand.m_sDescription = m_sDescription;
	m_hkcommand.m_fActive      = m_fActive;
	m_hkcommand.m_fExecutionMinimized = m_fExecutionMinimized;
	
	m_hkcommand.RemoveAllActions();
	for(int i=0; i<m_lvActions.GetItemCount(); i++)
	{
		CAction * pAction = LV_ACTION_GET_ACTION_PTR(i);
		m_hkcommand.AddAction(*pAction);
	}

	m_hkcommand.RemoveAllHotkeys();
	for(int i=0; i<m_lvHotkeys.GetItemCount(); i++)
	{
		CHotkey * pHotkey = LV_HOTKEY_GET_HOTKEY_PTR(i);
		m_hkcommand.AddHotkey(*pHotkey);
	}

	m_hkcommand.RemoveAllScheduledTask();
	for(int i=0; i<m_lvSchedulers.GetItemCount(); i++)
	{
		CScheduledTask * pScheduledTask = LV_SCHEDUL_GET_SCHEDUL_PTR(i);
		m_hkcommand.AddScheduledTask(*pScheduledTask);
	}

	CleanMemory();
	CDialog::OnOK();
}

void CCommandPropertiesDlg::OnChangeEditDescription()
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO: Add your control notification handler code here
	EnableControls();
}

void CCommandPropertiesDlg::OnButtonAddAction()
{
	// TODO: Add your control notification handler code here
	CActionPropertiesDlg Dlg;

	if (Dlg.DoModal() == IDOK)
	{
		CAction * pAction = new CAction;
		*pAction = Dlg.m_action;
		LV_ACTION_INSERT_ITEM(100000,pAction);
	}
	EnableControls();
}

void CCommandPropertiesDlg::OnButtonExecuteAction() 
{
	// TODO: Add your control notification handler code here
	int nItemSel = m_lvActions.GetFirstSelectedItem();
	if (nItemSel < 0) return;
	CAction * pAction = LV_ACTION_GET_ACTION_PTR(nItemSel);
	CHotkeyDlg::ExecuteAction(this, pAction->GetActionString(), *pAction, FALSE);
}

void CCommandPropertiesDlg::OnButtonAddHotkey() 
{
	// TODO: Add your control notification handler code here
	CHotkeyPropertiesDlg Dlg;

	if (Dlg.DoModal() == IDOK)
	{
		CHotkey * pHotkey = new CHotkey;
		*pHotkey = Dlg.m_hotkey;
		LV_HOTKEY_INSERT_ITEM(10000,pHotkey);
	}
	EnableControls();
}

void CCommandPropertiesDlg::OnButtonAddScheduler() 
{
	// TODO: Add your control notification handler code here
	CTaskSchedulingDlg Dlg;

	Dlg.m_scheduled_task.SetTaskType(TASKPUNCTUAL);
	Dlg.m_scheduled_task.InitDefaultTime();

	if (Dlg.DoModal() == IDOK)
	{
		CScheduledTask * pScheduledTask = new CScheduledTask;
		*pScheduledTask = Dlg.m_scheduled_task;
		LV_SCHEDUL_INSERT_ITEM(10000,pScheduledTask);
	}
}

void CCommandPropertiesDlg::OnButtonEditAction()
{
	// TODO: Add your control notification handler code here
	int nItemSel = m_lvActions.GetFirstSelectedItem();
	if (nItemSel < 0) return;
	CAction * pAction = LV_ACTION_GET_ACTION_PTR(nItemSel);

	CActionPropertiesDlg Dlg;
	Dlg.m_action = *pAction;

	if (Dlg.DoModal() == IDOK)
	{
		*pAction = Dlg.m_action;
		LV_ACTION_SET_ITEM(nItemSel,pAction);
	}
	EnableControls();
}

void CCommandPropertiesDlg::OnButtonEditHotkey() 
{
	// TODO: Add your control notification handler code here
	int nItemSel = m_lvHotkeys.GetFirstSelectedItem();
	if (nItemSel < 0) return;
	CHotkey * pHotkey = LV_HOTKEY_GET_HOTKEY_PTR(nItemSel);
	
	CHotkeyPropertiesDlg Dlg;
	Dlg.m_hotkey = *pHotkey;

	if (Dlg.DoModal() == IDOK)
	{
		*pHotkey = Dlg.m_hotkey;
		LV_HOTKEY_SET_ITEM(nItemSel,pHotkey);
	}
	EnableControls();
}

void CCommandPropertiesDlg::OnButtonEditScheduler() 
{
	// TODO: Add your control notification handler code here
	int nItemSel = m_lvSchedulers.GetFirstSelectedItem();
	if (nItemSel < 0) return;
	CScheduledTask * pScheduledTask = LV_SCHEDUL_GET_SCHEDUL_PTR(nItemSel);

	CTaskSchedulingDlg Dlg;
	Dlg.m_scheduled_task = *pScheduledTask;

	if (Dlg.DoModal() == IDOK)
	{
		*pScheduledTask = Dlg.m_scheduled_task;
		LV_SCHEDUL_SET_ITEM(nItemSel,pScheduledTask);
	}
	EnableControls();
}
void CCommandPropertiesDlg::OnButtonDelAction()
{
	// TODO: Add your control notification handler code here
	int nItemSelected = m_lvActions.GetSelectedCount();
	if (nItemSelected < 1) return;
	if (MessageBox("Supprimer toutes les actions s�lectionn�es ?", "Confirmation", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) != IDYES) return;
	for(int i=m_lvActions.GetItemCount()-1; i>=0; i--)
	{
		if (m_lvActions.IsItemSelected(i))
		{
			CAction * pAction = LV_ACTION_GET_ACTION_PTR(i);
			delete pAction;
			m_lvActions.DeleteItem(i);
		}
	}
	m_lvActions.SetFocus();
	EnableControls();
}

void CCommandPropertiesDlg::OnButtonDelHotkey() 
{
	// TODO: Add your control notification handler code here
	int nItemSelected = m_lvHotkeys.GetSelectedCount();
	if (nItemSelected < 1) return;
	if (MessageBox("Supprimer toutes les s�quences de touches s�lectionn�es ?", "Confirmation", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) != IDYES) return;
	for(int i=m_lvHotkeys.GetItemCount()-1; i>=0; i--)
	{
		if (m_lvHotkeys.IsItemSelected(i))
		{
			CHotkey * pHotkey = LV_HOTKEY_GET_HOTKEY_PTR(i);
			delete pHotkey;
			m_lvHotkeys.DeleteItem(i);
		}
	}
	m_lvHotkeys.SetFocus();
	EnableControls();
}

void CCommandPropertiesDlg::OnButtonDelScheduler() 
{
	// TODO: Add your control notification handler code here
	int nItemSelected = m_lvSchedulers.GetSelectedCount();
	if (nItemSelected < 1) return;
	if (MessageBox("Supprimer toutes les plannifications s�lectionn�es ?", "Confirmation", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) != IDYES) return;
	for(int i=m_lvSchedulers.GetItemCount()-1; i>=0; i--)
	{
		if (m_lvSchedulers.IsItemSelected(i))
		{
			CScheduledTask * pScheduledTask = LV_SCHEDUL_GET_SCHEDUL_PTR(i);
			delete pScheduledTask;
			m_lvSchedulers.DeleteItem(i);
		}
	}
	m_lvSchedulers.SetFocus();
	EnableControls();
}

void CCommandPropertiesDlg::OnButtonActionDown()
{
	// TODO: Add your control notification handler code here
	int nItemSel = m_lvActions.GetFirstSelectedItem();
	if (nItemSel < 0) return;
	CAction * pAction = LV_ACTION_GET_ACTION_PTR(nItemSel);
	m_lvActions.DeleteItem(nItemSel);
	LV_ACTION_INSERT_ITEM(nItemSel+1,pAction);
	m_lvActions.SetItemSel(nItemSel+1);
	m_lvActions.SetFocus();
}

void CCommandPropertiesDlg::OnButtonActionUp()
{
	// TODO: Add your control notification handler code here
	int nItemSel = m_lvActions.GetFirstSelectedItem();
	if (nItemSel < 0) return;
	CAction * pAction = LV_ACTION_GET_ACTION_PTR(nItemSel);
	m_lvActions.DeleteItem(nItemSel);
	LV_ACTION_INSERT_ITEM(nItemSel-1,pAction);
	m_lvActions.SetItemSel(nItemSel-1);
	m_lvActions.SetFocus();
}

void CCommandPropertiesDlg::OnItemchangedListActions(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	EnableControls();
	*pResult = 0;
}

void CCommandPropertiesDlg::OnItemchangedListHotkeys(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	EnableControls();
	*pResult = 0;
}

void CCommandPropertiesDlg::OnItemchangedListScheduler(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	EnableControls();
	*pResult = 0;
}

void CCommandPropertiesDlg::OnDblclkListActions(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	OnButtonEditAction();
	*pResult = 0;
}

void CCommandPropertiesDlg::OnDblclkListHotkeys(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnButtonEditHotkey();	
	*pResult = 0;
}

void CCommandPropertiesDlg::OnDblclkListScheduler(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnButtonEditScheduler();	
	*pResult = 0;
}

void CCommandPropertiesDlg::FillActions()
{
	for(int i=0; i<m_hkcommand.GetActionCount(); i++)
	{
		CAction * pAction = new CAction;
		*pAction = m_hkcommand.GetActionAt(i);
		LV_ACTION_INSERT_ITEM(100000,pAction);
	}
}

void CCommandPropertiesDlg::FillHotkeys()
{
	for(int i=0; i<m_hkcommand.GetHotkeyCount(); i++)
	{
		CHotkey * pHotkey = new CHotkey;
		*pHotkey = m_hkcommand.GetHotkeyAt(i);
		LV_HOTKEY_INSERT_ITEM(100000,pHotkey);
	}
}

void CCommandPropertiesDlg::FillScheduledTasks()
{
	for(int i=0; i<m_hkcommand.GetScheduledTaskCount(); i++)
	{
		CScheduledTask * pScheduledTask = new CScheduledTask;
		*pScheduledTask = m_hkcommand.GetScheduledTaskAt(i);
		LV_SCHEDUL_INSERT_ITEM(100000,pScheduledTask);
	}
}
