#if !defined(AFX_COMMANDPROPERTIESDLG_H__204F9D17_6870_4C76_8B3A_DF9676192554__INCLUDED_)
#define AFX_COMMANDPROPERTIESDLG_H__204F9D17_6870_4C76_8B3A_DF9676192554__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommandPropertiesDlg.h : header file
//

#include <listctrlex.h>
#include <staticex.h>

#include "HotkeyDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CCommandPropertiesDlg dialog

class CCommandPropertiesDlg : public CDialog
{
// Construction
public:
	CCommandPropertiesDlg(CWnd* pParent = NULL);   // standard constructor
	~CCommandPropertiesDlg();
	CHKCommand m_hkcommand;
	CImageList * m_pLVActionsImageListSmall;
	int m_nImageOn;
	int m_nImageOff;

// Dialog Data
	//{{AFX_DATA(CCommandPropertiesDlg)
	enum { IDD = IDD_DIALOG_COMMAND_PROPERTIES };
	CListCtrlEx2	m_lvSchedulers;
	CListCtrlEx2	m_lvHotkeys;
	CListCtrlEx2	m_lvActions;
	BOOL	m_fActive;
	CString	m_sDescription;
	BOOL	m_fExecutionMinimized;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommandPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCommandPropertiesDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnChangeEditDescription();
	afx_msg void OnButtonAddAction();
	afx_msg void OnItemchangedListActions(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonEditAction();
	afx_msg void OnButtonDelAction();
	afx_msg void OnDblclkListActions(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonActionDown();
	afx_msg void OnButtonActionUp();
	afx_msg void OnButtonAddHotkey();
	afx_msg void OnButtonDelHotkey();
	afx_msg void OnDblclkListHotkeys(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedListHotkeys(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonEditHotkey();
	afx_msg void OnButtonAddScheduler();
	afx_msg void OnButtonEditScheduler();
	afx_msg void OnButtonDelScheduler();
	afx_msg void OnDblclkListScheduler(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedListScheduler(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonExecuteAction();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void FillScheduledTasks();
	void FillHotkeys();
	void FillActions();
	void CleanMemory();
	void EnableControls();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMANDPROPERTIESDLG_H__204F9D17_6870_4C76_8B3A_DF9676192554__INCLUDED_)
