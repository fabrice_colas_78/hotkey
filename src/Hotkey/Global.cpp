#include "stdafx.h"

#include "Action.h"
#include "Global.h"

tt_hotkey s_hotkeys[] = {
	{"F1",VK_F1},{"F2",VK_F2},{"F3",VK_F3},{"F4",VK_F4},{"F5",VK_F5},{"F6",VK_F6},
	{"F7",VK_F7},{"F8",VK_F8},{"F9",VK_F9},{"F10",VK_F10},{"F11",VK_F11},{"F12",VK_F12},
	{"Num0",VK_NUMPAD0},{"Num1",VK_NUMPAD1},{"Num2",VK_NUMPAD2},{"Num3",VK_NUMPAD3},{"Num4",VK_NUMPAD4},
	{"Num5",VK_NUMPAD5},{"Num6",VK_NUMPAD6},{"Num7",VK_NUMPAD7},{"Num8",VK_NUMPAD8},{"Num9",VK_NUMPAD9},
	{"A",'A'},{"B",'B'},{"C",'C'},{"D",'D'},{"E",'E'},{"F",'F'},{"G",'G'},{"H",'H'},{"I",'I'},{"J",'J'},
	{"K",'K'},{"L",'L'},{"M",'M'},{"N",'N'},{"O",'O'},{"P",'P'},{"Q",'Q'},{"R",'R'},{"S",'S'},{"T",'T'},
	{"U",'U'},{"V",'V'},{"W",'W'},{"X",'X'},{"Y",'Y'},{"Z",'Z'},
	{"Page p�c�dente",VK_PRIOR},{"Page suivante",VK_NEXT},{"D�but",VK_HOME},{"Fin",VK_END},
	{"Haut",VK_UP},{"Bas",VK_DOWN},{"Gauche",VK_LEFT},{"Droite",VK_RIGHT},
	{"*",VK_MULTIPLY},{"/",VK_DIVIDE},{"+",VK_ADD},{"-",VK_SUBTRACT},{".",VK_DECIMAL},
	{"Tabulation",VK_TAB},{"Suppr.",VK_DELETE},{"Insert.",VK_INSERT},
	{"Entr�e", VK_RETURN},
	{"Retour arr.",VK_BACK},
	{"Echap",VK_ESCAPE},
	{NULL, NULL},
	};

tt_hotkey s_modifiers[] = {
	{"Aucun",0},{"Alt", MOD_ALT},{"Ctrl", MOD_CONTROL},{"Shift", MOD_SHIFT},{"Win",MOD_WIN},
	{"Alt + Win", MOD_ALT+MOD_WIN}, 
	{"Ctrl + Win",MOD_CONTROL+MOD_WIN},
	{"Shift + Win",MOD_SHIFT+MOD_WIN},
	{"Ctrl + Alt", MOD_ALT|MOD_CONTROL},
	{"Alt + Shift", MOD_ALT|MOD_SHIFT},
	{"Ctrl + Shift", MOD_CONTROL|MOD_SHIFT},
	{"Ctrl + Alt + Shift", MOD_CONTROL|MOD_ALT|MOD_SHIFT},
	
	{NULL, NULL},
	};


tt_actions s_volume[] = {
	{"Augmenter le volume", SOUND_VOLUME_UP},
	{"Diminuer le volume", SOUND_VOLUME_DOWN},
	{"D�sactiver les hauts-parleurs", SOUND_MUTE_ON},
	{"Activer les hauts-parleurs", SOUND_MUTE_OFF},
	{"Basculer Activer/D�sactiver les hauts parleurs", SOUND_MUTE_TOGGLE},

	{NULL, NULL},
	};

tt_actions s_misc_actions[] = {
	{"Explorateur Windows", A_MISC_EXPLORER},
	{"Navigateur Web par d�faut", A_MISC_BROWSER_CLIENT},
	{"Calendrier", A_MISC_MAIL_CALENDAR},
	{"Contacts", A_MISC_MAIL_CONTACT},
	{"Corbeille", A_MISC_RECYCLE_BIN},
	{"Messagerie par d�faut", A_MISC_MAIL_CLIENT},
	{"Envoyer un nouveau message", A_MISC_MAIL_NEW_MESSAGE},
	{"Newsgroup", A_MISC_NEWSGROUP_CLIENT},
	{"Aller � l'URL...", A_MISC_GOTOURL},
	{"Lecture audio/video", A_MISC_MEDIA_CLIENT},
	{"Gestionnaire des t�ches Windows", A_MISC_TASK_MANAGER},
	{"Ouvrir le dossier \"Mes Documents\"", A_MISC_SF_MYDOCUMENTS},
	{"Ouvrir le dossier \"Mod�les de documents\"", A_MISC_SF_DOCTEMPLATES},
	{"Ouvrir le dossier \"Mes documents r�cents\"", A_MISC_SF_MRD_LIST},
	{"Ouvrir le dossier \"Voisinage r�seau\"", A_MISC_SF_NETHOOD},
	{"Fermeture de la session Windows", A_MISC_LOGOFF},
	{"Eteindre l'ordinateur", A_MISC_POWEROFF},
	{"Red�marrage de l'ordinateur", A_MISC_REBOOT},
	{"V�rrouiller la station", A_MISC_LOCK_WORKSTATION},
	{"Mettre en veille", A_MISC_SUSPEND},
	{"Mettre en veille prolong�e", A_MISC_HIBERNATE},
	{"Simuler un d�placement souris", A_MISC_MOUSEMOVE},
	{"Simuler un clique gauche souris", A_MISC_MOUSELEFTCLICK},
	{"Simuler un clique droit souris", A_MISC_MOUSERIGHTCLICK},
	{NULL, NULL},
	};

tt_actions s_winamp_actions[] = {
	{"Lecture", A_WINAMP_PLAY},
	{"Stop", A_WINAMP_STOP},
	{"Pause", A_WINAMP_PAUSE},
	{"Morceau pr�c�dent", A_WINAMP_PREVIOUS_TRACK},
	{"Morceau suivant", A_WINAMP_NEXT_TRACK},
	{"Augmenter le volume", A_WINAMP_VOLUME_UP},
	{"Diminuer le volume", A_WINAMP_VOLUME_DOWN},
	{NULL, NULL},
	};
