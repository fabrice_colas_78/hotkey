#pragma once

//-------------------------------------------
//
//	Logging system
//
//-------------------------------------------
//	CR/LF for static configuration
#define  S_LF	_T("\r\n")

#define _DT	//	No need to translate

#define LOGGER_BASE_PRODUCT_CLASS			LOGGER_BASE_CLASS _T(".Hotkey")

#define LOGGER_HOTKEY_MAINDLG_CLASS			LOGGER_BASE_PRODUCT_CLASS ".HotkeyDlg"				
#define LOGGER_HOTKEY_SCHEDULER_CLASS		LOGGER_BASE_PRODUCT_CLASS ".Scheduler"
#define LOGGER_HOTKEY_SCHEDULED_TASK_CLASS	LOGGER_BASE_PRODUCT_CLASS ".ScheduledTask"

#define LOGGER_HOTKEY_REGISTRY_CONFIG_CLASS	LOGGER_BASE_PRODUCT_CLASS ".RegistryConfig"

// **************************
//		Common
// **************************
#define LOGGER_INDWTSCTXAPI_CLASS		LOGGER_BASE_PRODUCT_CLASS _T(".IndWtsCtxApi")


//	Default appender
#define HOTKEY_DEFAULT_APPENDER			_T("DefaultAppender")


struct tt_hotkey
{
	const char * szLabel;
	UINT uVk;
};
extern tt_hotkey s_modifiers[];
extern tt_hotkey s_hotkeys[];


struct tt_actions
{
	const char * szLabel;
	UINT uId;
};
extern tt_actions s_misc_actions[];
extern tt_actions s_winamp_actions[];
extern tt_actions s_volume[];


CString GetModuleVersion();
CString GetAppVersion();
CString GetAppBuild();
CString GetAppBuildAndDate();
