// GotoURLDlg.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "GotoURLDlg.h"

#include "HotkeyDlg.h"
#include "registryconfig.h"

#include <registry.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGotoURLDlg dialog


CGotoURLDlg::CGotoURLDlg(CRegistryConfig& registryConfig, CWnd* pParent /*=NULL*/)
	: CDialog(CGotoURLDlg::IDD, pParent)
	, m_registryConfig(registryConfig)
{
	//{{AFX_DATA_INIT(CGotoURLDlg)
	m_sGotoURL = _T("");
	//}}AFX_DATA_INIT
	m_fQueryStringMode = false;
}


void CGotoURLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGotoURLDlg)
	DDX_Control(pDX, IDC_COMBO_URL, m_cbGotoURL);
	DDX_CBString(pDX, IDC_COMBO_URL, m_sGotoURL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGotoURLDlg, CDialog)
	//{{AFX_MSG_MAP(CGotoURLDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGotoURLDlg message handlers

BOOL CGotoURLDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if (m_sTitle != "")
		SetWindowText(m_sTitle);
	if (m_sItem != "")
		SetDlgItemText(IDC_STATIC_ITEM, m_sItem);

	CStringArray saValues;
	if (m_fQueryStringMode)
		m_registryConfig.RestoreQueryStrings(saValues, m_sGotoURL);
	else
		m_registryConfig.RestoreURLS(saValues, m_sGotoURL);
	for(int i=0; i<saValues.GetSize(); i++)
	{
		m_cbGotoURL.AddString(saValues[i]);
	}

	UpdateData(false);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGotoURLDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(true);
	AddURL(m_sGotoURL);

	CStringArray saValues;
	CString sURL, sKey;
	for(int i=0; i<m_cbGotoURL.GetCount(); i++)
	{
		m_cbGotoURL.GetLBText(i, sURL);
		saValues.Add(sURL);
	}

	if (m_fQueryStringMode)
		m_registryConfig.SaveQueryStrings(saValues, m_sGotoURL);
	else
		m_registryConfig.SaveURLS(saValues, m_sGotoURL);	
	CDialog::OnOK();
}

void CGotoURLDlg::AddURL(const char *sURL)
{
	if (m_cbGotoURL.FindString(0, sURL) == -1)
	{
		m_cbGotoURL.AddString(sURL);
	}
}
