#if !defined(AFX_GOTOURLDLG_H__950511B0_D0E5_48FE_805D_AF059B43E438__INCLUDED_)
#define AFX_GOTOURLDLG_H__950511B0_D0E5_48FE_805D_AF059B43E438__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GotoURLDlg.h : header file
//

#include "RegistryConfig.h"

/////////////////////////////////////////////////////////////////////////////
// CGotoURLDlg dialog

class CGotoURLDlg : public CDialog
{
// Construction
public:
	BOOL m_fQueryStringMode;
	void AddURL(const char * sURL);
	
	CGotoURLDlg(CRegistryConfig& registryConfig, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGotoURLDlg)
	enum { IDD = IDD_DIALOG_GOTOURL };
	CComboBox	m_cbGotoURL;
	CString	m_sGotoURL;
	//}}AFX_DATA
	CString m_sTitle;
	CString m_sItem;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGotoURLDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGotoURLDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegistryConfig& m_registryConfig;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GOTOURLDLG_H__950511B0_D0E5_48FE_805D_AF059B43E438__INCLUDED_)
