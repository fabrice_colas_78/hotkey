// GroupPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Hotkey.h"
#include "GroupPropertiesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGroupPropertiesDlg dialog


CGroupPropertiesDlg::CGroupPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGroupPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGroupPropertiesDlg)
	m_sGroupName = _T("");
	m_fRootGroup = FALSE;
	//}}AFX_DATA_INIT
	m_fEditMode = FALSE;
}


void CGroupPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGroupPropertiesDlg)
	DDX_Text(pDX, IDC_EDIT_GROUP_NAME, m_sGroupName);
	DDX_Check(pDX, IDC_CHECK_ROOT_GROUP, m_fRootGroup);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGroupPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CGroupPropertiesDlg)
	ON_EN_CHANGE(IDC_EDIT_GROUP_NAME, OnChangeEditGroupName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGroupPropertiesDlg message handlers

BOOL CGroupPropertiesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if (m_fEditMode)
	{
		GetDlgItem(IDC_CHECK_ROOT_GROUP)->ShowWindow(SW_HIDE);
	}
	UpdateData(FALSE);
	EnableControls();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGroupPropertiesDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	CDialog::OnOK();
}

void CGroupPropertiesDlg::OnChangeEditGroupName() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CGroupPropertiesDlg::EnableControls()
{
	UpdateData(TRUE);
	GetDlgItem(IDOK)->EnableWindow(m_sGroupName != "");
}
