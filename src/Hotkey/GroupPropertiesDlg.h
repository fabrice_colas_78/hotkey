#if !defined(AFX_GROUPPROPERTIESDLG_H__AD838302_F928_404D_B260_8DAB07C938E1__INCLUDED_)
#define AFX_GROUPPROPERTIESDLG_H__AD838302_F928_404D_B260_8DAB07C938E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupPropertiesDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGroupPropertiesDlg dialog

class CGroupPropertiesDlg : public CDialog
{
// Construction
public:
	BOOL m_fEditMode;
	CGroupPropertiesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGroupPropertiesDlg)
	enum { IDD = IDD_DIALOG_GROUP };
	CString	m_sGroupName;
	BOOL	m_fRootGroup;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGroupPropertiesDlg)
	afx_msg void OnChangeEditGroupName();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void EnableControls();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPPROPERTIESDLG_H__AD838302_F928_404D_B260_8DAB07C938E1__INCLUDED_)
