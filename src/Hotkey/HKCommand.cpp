#include "stdafx.h"

#include "ScheduledTask.h"

#include "HKCommand.h"


void CHKCommand::operator=(const CHKCommand& cmd)
{
	//LOG3F("CHotkey()::operator=()\n");
	m_sDescription        = (const char *)cmd.m_sDescription;
	m_fActive             = cmd.m_fActive;
	m_fExecutionMinimized = cmd.m_fExecutionMinimized;
	m_dwNumOfExec         = cmd.m_dwNumOfExec;

	m_dwId                = cmd.m_dwId;
	
	RemoveAllHotkeys();
	for(int i=0; i<cmd.GetHotkeyCount(); i++)
		AddHotkey(cmd.GetHotkeyAt(i));
	
	RemoveAllActions();
	for(int i=0; i<cmd.GetActionCount(); i++)
		AddAction(cmd.GetActionAt(i));
	
	RemoveAllScheduledTask();
	for(int i=0; i<cmd.GetScheduledTaskCount(); i++)
		AddScheduledTask(cmd.GetScheduledTaskAt(i));
}

BOOL CHKCommand::RegisterHotkeysAndScheduledTasks(HWND hWnd, CScheduler& scheduler)
{
	BOOL fRet = TRUE;
	for(int i=0; i<m_hotkey_array.GetSize(); i++)
	{
		CHotkey hk = m_hotkey_array[i];
		LOG1F("registering hotkey %s...\n", hk.GetHotkeyString());
		fRet = hk.RegisterHotkey(hWnd);
		if (! fRet) LOG1F("!Error: unable to register hotkey %s (%s)\n", hk.GetHotkeyString(), GetLastErrorString());
		else LOG1F("registering hotkey %s ok.\n", hk.GetHotkeyString());
		m_hotkey_array[i] = hk;
		LOG1F("Hotkey id=%d\n", m_hotkey_array[i].m_uHotkeyID);
	}
	for(int i=0; i<m_scheduled_task_array.GetSize(); i++)
	{
		m_scheduled_task_array[i].SetTaskId(scheduler.AddTask(m_scheduled_task_array[i]));
	}
	return TRUE;
}

void CHKCommand::UnRegisterHotkeysAndScheduledTasks(CScheduler& scheduler)
{
	for(int i=0; i<m_hotkey_array.GetSize(); i++)
	{
		CHotkey hk = m_hotkey_array[i];
		hk.UnRegisterHotkey();
		m_hotkey_array[i] = hk;
	}
	for(int i=0; i<m_scheduled_task_array.GetSize(); i++)
	{
		scheduler.DeleteTask(m_scheduled_task_array[i].GetTaskId());
	}
}


void CHKCommand::RemoveAllScheduledTask()
{
	m_scheduled_task_array.RemoveAll();
}
int CHKCommand::AddScheduledTask(CScheduledTask& scheduled_task)
{
	return m_scheduled_task_array.Add(scheduled_task);
}
CScheduledTask CHKCommand::GetScheduledTaskAt(int nIndex) const
{
	return m_scheduled_task_array.GetAt(nIndex);
}
void CHKCommand::SetScheduledTaskAt(int nIndex, CScheduledTask& scheduled_task)
{
	m_scheduled_task_array.SetAt(nIndex, scheduled_task);
}
int CHKCommand::GetScheduledTaskCount() const
{
	return m_scheduled_task_array.GetSize();
}
