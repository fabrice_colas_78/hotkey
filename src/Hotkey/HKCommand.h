#pragma once

#include <afxtempl.h>

#include "Action.h"
#include "Hotkeyc.h"

#define HKCMD_NOID		0

class CScheduler;
class CScheduledTask;

class CHKCommand
{
public:
	CHKCommand() 
	{ 
		m_sDescription        = ""; 
		m_fActive             = TRUE;
		m_fExecutionMinimized = FALSE;
		m_dwId                = HKCMD_NOID;
		m_dwNumOfExec         = 0;
#ifdef _DEBUG
		static int i;
		m_sDescription.Format("Commande %d", ++i);
		CAction action;
		action.m_sProgram = "c:\\program 1.exe";
		//AddAction(action);
#endif
	}
	~CHKCommand() {}
	void operator=(const CHKCommand& cmd);
	
	BOOL RegisterHotkeysAndScheduledTasks(HWND hWnd, CScheduler& scheduler);
	void UnRegisterHotkeysAndScheduledTasks(CScheduler& scheduler);
	
	//	Hotkeys
	void RemoveAllHotkeys()
	{
		m_hotkey_array.RemoveAll();
	}
	int AddHotkey(CHotkey& hotkey)
	{
		return (int)m_hotkey_array.Add(hotkey);
	}
	CHotkey GetHotkeyAt(int nIndex) const
	{
		return m_hotkey_array.GetAt(nIndex);
	}
	void SetHotkeyAt(int nIndex, CHotkey& hotkey)
	{
		m_hotkey_array.SetAt(nIndex, hotkey);
	}
	int GetHotkeyCount() const
	{
		return (int)m_hotkey_array.GetSize();
	}
	
	//	Scheduled task
	void RemoveAllScheduledTask();
	int AddScheduledTask(CScheduledTask& scheduled_task);
	CScheduledTask GetScheduledTaskAt(int nIndex) const;
	void SetScheduledTaskAt(int nIndex, CScheduledTask& scheduled_task);
	int GetScheduledTaskCount() const;

	//	Actions
	void RemoveAllActions()
	{
		m_action_array.RemoveAll();
	}
	int AddAction(CAction& action)
	{
		return (int)m_action_array.Add(action);
	}
	CAction GetActionAt(int nIndex) const
	{
		return m_action_array.GetAt(nIndex);
	}
	void SetActionAt(int nIndex, CAction& action)
	{
		m_action_array.SetAt(nIndex, action);
	}
	int GetActionCount() const
	{
		return (int)m_action_array.GetSize();
	}

	CString m_sDescription;
	BOOL m_fActive;
	BOOL m_fExecutionMinimized;
	DWORD m_dwId;
	DWORD m_dwNumOfExec;
private:
	CArray <CAction, CAction&> m_action_array;
	CArray <CHotkey, CHotkey&> m_hotkey_array;
	CArray <CScheduledTask, CScheduledTask&> m_scheduled_task_array;
};

