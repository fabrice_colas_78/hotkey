
// Hotkey.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Hotkey.h"
#include "HotkeyDlg.h"

#include <CommonLib.h>
#include <Log4CppLib.h>
#include <Mixer.h>
#include <Registry.h>
#include <SplashDlg.h>
#include <Sock.h>

#include "ScheduledTask.h"

#include "Global.h"
#include "Vers.h"

#pragma comment(lib, "HotkeyLib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CHotkeyApp

BEGIN_MESSAGE_MAP(CHotkeyApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CHotkeyApp construction

CHotkeyApp::CHotkeyApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CHotkeyApp object

CHotkeyApp theApp;

LPCTSTR GetHotkeyLogConfig()
{
	return
		_T("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>") S_LF
		S_LF
		_T("<logger:configuration replace=\"false\">") S_LF
		S_LF
		_T("    <appender name=\"") HOTKEY_DEFAULT_APPENDER _T("\" class=\"RollingFileAppender\">") S_LF
#ifndef _DEBUG
		_T("        <param name=File value=\"%TEMP%\\\\Hotkey\\\\Hotkey.log\"/>") S_LF
#else
		_T("        <param name=File value=\"%TEMP%\\\\Hotkey\\\\Hotkey-Dbg.log\"/>") S_LF
#endif
		_T("        <layout class=\"PatternLayout\" >") S_LF
		_T("           <param name=ConversionPattern value=\"%d [%-5p] %m %n\" />") S_LF
		_T("        </layout>") S_LF
		_T("    </appender>") S_LF
		S_LF
		_T("    <category name=\"") LOGGER_BASE_PRODUCT_CLASS _T("\" additivity=\"false\">") S_LF
		_T("       <priority value=\"info\" />") S_LF
		_T("       <appender-ref ref=\"") HOTKEY_DEFAULT_APPENDER _T("\"/>") S_LF
		_T("	 </category>") S_LF
		S_LF
		_T("    <root>") S_LF
		_T("       <priority value=\"error\" />") S_LF
		_T("       <appender-ref ref=\"") HOTKEY_DEFAULT_APPENDER _T("\"/>") S_LF
		_T("    </root>") S_LF
		S_LF
		_T("</logger:configuration>") S_LF;
}


IMPLEMENT_DEBUG_APPENDER


// CHotkeyApp initialization

BOOL CHotkeyApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	
	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	CSock::Startup();

	//LOGCREATEALL("Hotkey Manager");
	INIT_DEBUG_APPENDER("Hotkey");

#ifdef _DEBUG0
	CMixer mixer;
	if (mixer.Open())
	{
		LPCTSTR szDescription = "Desc";
		LPCTSTR szActionString = "Action";
		UINT uSubActionId = SOUND_VOLUME_UP;
		uSubActionId == SOUND_VOLUME_UP ? mixer.VolumeUp(3) : mixer.VolumeDown(3);

		DWORD dwVolValue, dwMaxBound;
		mixer.GetVolumeValue(dwVolValue, NULL, &dwMaxBound);
		BOOL fMuteStatus;
		mixer.GetMuteStatus(fMuteStatus);
		CString sTip;
		sTip.Format("%s - %s [%u%%,%u%%][%s]", szDescription, szActionString, (LOWORD(dwVolValue) * 100) / dwMaxBound, (HIWORD(dwVolValue) * 100) / dwMaxBound, fMuteStatus ? "HP D�sactiv�s" : "HP Activ�s");
		LDA("TIp: % s", sTip);
	}
	return false;
#endif

	//	Starting factory before creating DLG objec
	CString error;
	if (!g_logFactory.LoadFactory(_T("HotKey"), GetHotkeyLogConfig(), error, true))
	{
		MessageBox(NULL, error, _T("unable to load logging config"), MB_ICONEXCLAMATION);
	}

	LDA(_T("DumpedConfig:\n%s"), g_logFactory.GetConfiguration());

	if (!g_logFactory.StartFactory(error))
	{
		//	No display because of starting twice ISprintUsr generate this message
		//MessageBox(NULL, error, "unable to start logging config", MB_ICONEXCLAMATION);
	}

	//	Get logger for main category
	CLogger * m_logger = g_logFactory.GetLoggerPtr(LOGGER_BASE_PRODUCT_CLASS);

	CRegistry::EnableDebugAll(true);

	//CRegAccess::SaveApplicationPath();

	CHotkeyDlg dlg;

	dlg.m_sTitle.LoadString(IDS_MAIN_TITLE);
	CString sVersion;
	sVersion.Format(" - Vers %s - %s", GetModuleVersion(), GetAppBuildAndDate());
	LOGInfof("==========================================================");
	LOGInfof(sVersion);

	LDA("DumpedConfig:\n%s", g_logFactory.GetConfiguration());

	dlg.m_sTitle += sVersion;

	HWND hWndFind = FindWindow(NULL, dlg.m_sTitle);
	if (hWndFind)
	{
		ShowWindow(hWndFind, SW_RESTORE);
		SetForegroundWindow(hWndFind);
		return FALSE;
	}

#ifndef _DEBUG
	/*VERSION();
	BUILD();
	CString sAppliName;
	sAppliName.LoadString(IDS_MAIN_TITLE);
	CSplashDlg SD(NULL, sAppliName, strVersion, strBuild);
	SD.SetCenter();
	SD.SetIcon(AfxGetApp()->LoadIcon(MAKEINTRESOURCE(IDR_MAINFRAME)));*/

	CString sAppliName;
	sAppliName.LoadString(IDS_MAIN_TITLE);

	CSplashDlg SD;
	SD.SetInfos(sAppliName, GetAppVersion(), GetAppBuildAndDate());
	SD.SetIcon(AfxGetApp()->LoadIcon(MAKEINTRESOURCE(IDR_MAINFRAME)));
	SD.DoModeless(1500, TRUE);
#endif
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	}

	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

#ifndef _AFXDLL
	ControlBarCleanUp();
#endif

	g_logFactory.StopFactory(error);

	FREE_DEBUG_APPENDER;

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

