
// HotkeyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Hotkey.h"
#include "HotkeyDlg.h"
#include "afxdialogex.h"

#include "ScheduledTask.h"
//#include "x:\fcslib\lib\powrprof.h"

#include <mapi.h>

#include <log.h>
#include <macrosdef.h>
#include <registry.h>
#include <winerr.h>
#include <mapiex.h>
#include <splashdlg.h>
#include <specialfolder.h>
#include <shortcut.h>
#include <rasdial.h>
#include <dateref.h>
#include <IniFile.h>

#include "..\HotkeyLib\HotkeyLib.h"

#include "Global.h"
#include "vers.h"
#include "mixer.h"
#include "winampapi.h"

#include "CommandPropertiesDlg.h"
#include "GroupPropertiesDlg.h"
#include "PreferencesDlg.h"
#include "TaskSchedulingDlg.h"
#include "InfosDlg.h"
#include "SchedulDlg.h"
#include "GotoURLDlg.h"
#include "registryconfig.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define STR_APPNAME				"Hotkey Manager"

#define INIT_ITEM_SEL			HTREEITEM hItemSel = m_tvCommands.GetSelectedItem()

#define MB_ERR(text)		{CString sErr; sErr.Format("!Erreur: %s", text); MessageBox(sErr, STR_APPNAME, MB_ICONSTOP);}
#define MB_ERR_NULL(text)	{CString sErr; sErr.Format("!Erreur: %s", text); ::MessageBox(NULL, sErr, STR_APPNAME, MB_ICONSTOP);}

#define CHANGE_FOCUS()		m_tvCommands.SetFocus();

#define SHOW_VALUE			fExecutionMinimized?SW_MINIMIZE:SW_SHOWNORMAL


CWndTip CHotkeyDlg::m_wndtip;
CWindowNotifier CHotkeyDlg::m_wndNotifier;
DWORD CHotkeyDlg::m_dwShowTipTime;
BOOL CHotkeyDlg::m_fTipDisplayDetails;
BOOL CHotkeyDlg::m_fTipNotifier;
CString CHotkeyDlg::m_sPathPNICA;
CRegistryConfig CHotkeyDlg::m_registryConfig;

#define MY_LOGGER_NAME	LOGGER_HOTKEY_MAINDLG_CLASS

#include <LoggerMacros.h>

// CHotkeyDlg dialog
INITLogger(CHotkeyDlg::m_logger);

CHotkeyDlg::CHotkeyDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_HOTKEY_DIALOG, pParent)
{
	if (m_logger == NULL)
	{
		//	Get logger only the first time
		m_logger = g_logFactory.GetLoggerPtr(MY_LOGGER_NAME);
	}

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_sDebug = _T("");

	m_registryConfig.Init();

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_ilHotkeys.Create(16, 16, true, 1, 1);
	m_ilHotkeys.Add(AfxGetApp()->LoadIcon(IDI_ICON_TV_GROUP));
	m_ilHotkeys.Add(AfxGetApp()->LoadIcon(IDI_ICON_TV_COMMAND));
	m_ilHotkeys.Add(AfxGetApp()->LoadIcon(IDI_ICON_TV_ACTION));
	m_ilHotkeys.Add(AfxGetApp()->LoadIcon(IDI_ICON_TV_SCHEDUL));

	m_dwShowTipTime = 1500;
	m_fTipDisplayDetails = false;
	m_fTipNotifier = false;

	m_pDragImage = NULL;
	m_fDragging = FALSE;
	m_nDragTimerID = m_nHoverTimerID = 0;
	m_ptaskwindowlist = NULL;
}

CHotkeyDlg::~CHotkeyDlg()
{
}

void CHotkeyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHotkeyDlg)
	DDX_Control(pDX, IDC_TREE_COMMAND_LIST, m_tvCommands);
	DDX_Text(pDX, IDC_STATIC_DEBUG, m_sDebug);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CHotkeyDlg, CDialogEx)
	//ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_QUIT, OnButtonQuit)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_ABOUT, OnButtonAbout)
	ON_BN_CLICKED(IDC_BUTTON_ADD_COMMAND, OnButtonAddCommand)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_COMMAND, OnButtonEditCommand)
	ON_BN_CLICKED(IDC_BUTTON_DEL_COMMAND, OnButtonDelCommand)
	ON_BN_CLICKED(IDC_BUTTON_TEST_COMMAND, OnButtonTestCommand)
	ON_BN_CLICKED(IDC_BUTTON_COPY_COMMAND, OnButtonCopyCommand)
	ON_BN_CLICKED(IDC_CHECK_START_WITH_WINDOWS, OnCheckStartWithWindows)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_COMMAND_LIST, OnSelchangedTreeCommandList)
	ON_NOTIFY(NM_DBLCLK, IDC_TREE_COMMAND_LIST, OnDblclkTreeCommandList)
	ON_BN_CLICKED(IDC_BUTTON_ADD_GROUP, OnButtonAddGroup)
	ON_BN_CLICKED(IDC_BUTTON_DEL_GROUP, OnButtonDelGroup)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_GROUP, OnButtonEditGroup)
	ON_NOTIFY(TVN_BEGINDRAG, IDC_TREE_COMMAND_LIST, OnBegindragTreeCommandList)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_PREFERENCES, OnButtonPreferences)
	ON_BN_CLICKED(IDC_BUTTON_INFOS, OnButtonInfos)
	ON_BN_CLICKED(IDC_BUTTON_SCHEDUL, OnButtonSchedul)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_ICONNOTIFY, OnIconNotify)
	ON_MESSAGE(WM_HOTKEY, OnHotKey)
	ON_COMMAND(ID_HIDEWINDOW, OnHideWindow)
END_MESSAGE_MAP()


// CHotkeyDlg message handlers

void EnableShutdown()
{
	HANDLE hProcess = GetCurrentProcess();
	HANDLE hToken;
	LUID luid;
	TOKEN_PRIVILEGES priv;

	OpenProcessToken(hProcess, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);
	LookupPrivilegeValue("", "SeShutdownPrivilege", &luid);

	priv.PrivilegeCount = 1;
	priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	priv.Privileges[0].Luid = luid;

	// enable shutdown privilege for the current application
	AdjustTokenPrivileges(hToken, FALSE, &priv, 4 + (12 * priv.PrivilegeCount), NULL, NULL);
	CloseHandle(hToken);
}

BOOL CHotkeyDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

#if 0
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
#endif
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	SetWindowText(m_sTitle);

	m_wndNotifier.Create(this);
	m_wndNotifier.SetSkin(IDB_BITMAP_SKINMSN);
	m_wndNotifier.SetTextFont("Arial", 90, WN_TEXT_NORMAL, WN_TEXT_UNDERLINE);
	m_wndNotifier.SetTextColor(RGB(0, 0, 0), RGB(0, 255, 0));
	m_wndNotifier.SetTextRect(CRect(10, 30, m_wndNotifier.GetSkinWidth() - 10, m_wndNotifier.GetSkinHeight() - 10));

	EnableShutdown();
#ifdef _DEBUG
	m_nDebugTimerID = SetTimer(2000, 200, NULL);
#endif
	m_nDateTimeIdleTimerID = (UINT)SetTimer(1234, 1000, NULL);
	m_wndtip.Create();
	m_wndtip.SetAutomaticPos(CWndTip::AutoPosTopLeft);
	m_wndtip.SetOffset(3, 3);

	m_tvCommands.SetImageList(&m_ilHotkeys, TVSIL_NORMAL);
#ifdef _DEBUG0
	CAction action;

	HTREEITEM hGrp1 = AddGroup(TVI_ROOT, "Group 1");
	HTREEITEM hGrp2 = AddGroup(TVI_ROOT, "Group 2");
	HTREEITEM hGrp21 = AddGroup(hGrp2, "Group 2/1");

	CHKCommand hkcommand;

	hkcommand.m_sDescription = "Command1 Groupe1";
	hkcommand.m_uVirtualKey = 'B';
	action.m_uActionId = ACTION_OPEN_FILE;
	action.m_sProgram = "c:\\temp";
	hkcommand.AddAction(action);
	AddCommand(hGrp1, hkcommand);

	hkcommand.RemoveAllActions();
	hkcommand.m_sDescription = "Command1 Groupe2";
	hkcommand.m_uVirtualKey = 'C';
	action.m_sProgram = "%systemroot%";
	hkcommand.AddAction(action);
	AddCommand(hGrp2, hkcommand);

	hkcommand.RemoveAllActions();
	hkcommand.m_sDescription = "Command1 Groupe21";
	hkcommand.m_uVirtualKey = 'D';
	action.m_sProgram = "d:\\bin\\wsock32.exe";
	hkcommand.AddAction(action);
	AddCommand(hGrp21, hkcommand);
#endif

	//	Convertion Hotkeys V1
	m_registryConfig.ConvertHotkeysConfigV1(*this);
	//	Convertion Hotkeys V2
	m_registryConfig.ConvertHotkeysConfigV2(*this);

	//	Restau hotkey V3
	m_registryConfig.RestoreHotkeysConfig(*this);
	m_registryConfig.RestorePreferences(*this);

	//m_tvCommands.ExpandChildren(TVI_ROOT);

	DisplayDateTimeIdle();
	EnableControls();
	TrayIconShow(true);

#ifndef _DEBUG
	((CButton *)GetDlgItem(IDC_CHECK_START_WITH_WINDOWS))->SetCheck(CShortcut::IsRegisteredInStartupMenu(GetModuleFileNameStr()));
	if (m_tvCommands.GetCount() != 0)
	{
		PostMessage(WM_COMMAND, ID_HIDEWINDOW);
	}
	if (!::RegisterHotKey(*this, 0, MOD_ALT, VK_PAUSE))
	{
		CString sMsg;
		sMsg.Format("!Erreur: ajout de la sequence de touche 'Alt+Pause' impossible (%s)", GetLastErrorString());
		MessageBox(sMsg, STR_APPNAME, MB_ICONSTOP);
	}
	if (!::RegisterHotKey(*this, 1, MOD_ALT + MOD_SHIFT, VK_PAUSE))
	{
		CString sMsg;
		sMsg.Format("!Erreur: ajout de la sequence de touche 'Ctrl+Alt+Pause' impossible (%s)", GetLastErrorString());
		MessageBox(sMsg, STR_APPNAME, MB_ICONSTOP);
	}
#else
	GetDlgItem(IDC_CHECK_START_WITH_WINDOWS)->EnableWindow(FALSE);
	/*if (! ::RegisterHotKey(*this, 0, MOD_ALT|MOD_SHIFT, VK_PAUSE))
	{
	CString sMsg;
	sMsg.Format("!Erreur: ajout de la sequence de touche 'Alt+Pause' impossible (%s)", GetLastErrorString());
	MessageBox(sMsg, STR_APPNAME, MB_ICONSTOP);
	}*/
	/*
	SYSTEM_POWER_STATUS sps;
	GetSystemPowerStatus(&sps);
	CString s;
	s.Format("ACLineStatus=%d BatteryFlag=%d BatteryLifePercent=%d BatteryLifeTime=%d BatteryFullLifeTime=%d",
	sps.ACLineStatus, sps.BatteryFlag, sps.BatteryLifePercent, sps.BatteryLifeTime, sps.BatteryFullLifeTime);
	MessageBox(s);
	if (! SetSystemPowerState(true, true))
	{
	s.Format("Non:%s\n", GetLastErrorString());
	MessageBox(s);
	}
	*/
	//PostMessage(WM_COMMAND, IDC_BUTTON_PREFERENCES);
#endif
#ifdef _DEBUG
	if (!::RegisterHotKey(*this, 2, MOD_WIN | MOD_SHIFT, VK_ESCAPE))
#else
	if (!::RegisterHotKey(*this, 2, MOD_WIN, VK_ESCAPE))
#endif
	{
		CString sMsg;
		sMsg.Format("!Erreur: ajout de la sequence de touche 'Win+Esc' impossible (%s)", GetLastErrorString());
		MessageBox(sMsg, STR_APPNAME, MB_ICONSTOP);
	}

	//	Demarrage du scheduler
	m_scheduler.SetCallback(OnScheduledTask, (void *)this);
	m_scheduler.StartScheduler();

	//	Remote socket
	m_sockRemote.Startup();
	if (!m_sockRemote.CreateTCPListen(CSockAddr((DWORD)0), CSockPort(695)))
	{
		LOG1F("CreateSocket error '%s'\n", m_sockRemote.GetLastErrorString());
	}
	else
	{
		m_sockRemote.StartAsyncCallback(NULL, (LPARAM)this);
	}
#ifdef _DEBUG0
	CTaskSchedulingDlg Dlg;

	Dlg.m_scheduled_task.SetTaskType(TASKPUNCTUAL);
	Dlg.m_scheduled_task.InitDefaultTime();

	for (;;)
	{
		if (Dlg.DoModal() == IDCANCEL) break;
	}
	EndDialog(FALSE);
#endif
#ifdef _DEBUG
	//PostMessage(WM_COMMAND, IDC_BUTTON_SCHEDUL, 0);
#endif

	return TRUE;  // return TRUE  unless you set the focus to a control
}

#if 0
void CHotkeyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}
#endif

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CHotkeyDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CHotkeyDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CHotkeyDlg::OnHideWindow()
{
	ShowWindow(SW_HIDE);
}


void CHotkeyDlg::OnCancel()
{
	// TODO: Add extra cleanup here
#ifdef _DEBUG
	OnButtonQuit();
#else
	ShowWindow(SW_SHOWMINIMIZED);
#endif
}

void CHotkeyDlg::OnOK()
{
	// TODO: Add extra validation here
	//CDialog::OnOK();
}

void CHotkeyDlg::OnButtonQuit()
{
	// TODO: Add your control notification handler code here
#ifndef _DEBUG
	SetForegroundWindow();
	if (MessageBox("Etes vous s�r de vouloir quitter Hotkey Manager ?", STR_APPNAME, MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO) return;
#endif
	KillTimer(m_nDateTimeIdleTimerID);
	m_scheduler.StopScheduler();
	EndDialog(FALSE);
}

void CHotkeyDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	if (nType == SIZE_MINIMIZED) ShowWindow(SW_HIDE);
}

void CHotkeyDlg::OnDestroy()
{
	TrayIconShow(FALSE);
	CDialog::OnDestroy();

	// TODO: Add your message handler code here	
	CleanMemory();
}

void CHotkeyDlg::TrayIconShow(BOOL fShow)
{
	NOTIFYICONDATA NID;

	NID.cbSize = sizeof(NID);
	NID.hWnd = *this;
	NID.uID = 1;
	NID.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	NID.uCallbackMessage = WM_ICONNOTIFY;
	NID.hIcon = m_hIcon;
	CString sTitle;
	GetWindowText(sTitle);
	StringCchCopy(NID.szTip, sizeof(NID.szTip), sTitle);
	Shell_NotifyIcon(fShow ? NIM_ADD : NIM_DELETE, &NID);
}

LRESULT CHotkeyDlg::OnIconNotify(WPARAM, LPARAM lParam)
{
	if (NIM(lParam) == NIM_LBUTTONUP)
	{
		RestoreWindow();
	}
	return 0;
}

void CHotkeyDlg::DisplayDateTimeIdle()
{
	CString s;
	CDateRef drCur, drIdle;
	drCur.SetLang(DR_LANG_FRENCH);
	drCur.SetCurrentLocalTime();
	drIdle.SetTotalSeconds(HotkeyLibGetIdleSeconds());
	s.Format("%s [%s]", drCur.ToString(CDateRef::WD_DAY_MONTH_YEAR_HH_MI_SS), drIdle.ToString(CDateRef::Ellapsed));
	SetDlgItemText(IDC_STATIC_DATE_TIME, s);
}

void CHotkeyDlg::RestoreWindow()
{
	ShowWindow(SW_RESTORE);
	SetForegroundWindow();
}

BOOL CHotkeyDlg::SearchHotkey(HTREEITEM hItem, UINT uHotkeyID)
{
	hItem = m_tvCommands.GetChildItem(hItem);
	for (;;)
	{
		if (!hItem) break;
		INIT_ITEM_DATA(hItem);
		if (pItemData->m_nType == CItemData::Command)
		{
			for (int i = 0; i<pItemData->m_hkcommand.GetHotkeyCount(); i++)
			{
				CHotkey hotkey = pItemData->m_hkcommand.GetHotkeyAt(i);
				if (hotkey.m_uHotkeyID == uHotkeyID)
				{
					ExecuteCommand("Hotkey", pItemData->m_hkcommand);
					return true;
				}
			}
		}
		if (SearchHotkey(hItem, uHotkeyID)) return true;
		hItem = m_tvCommands.GetNextSiblingItem(hItem);
	}
	return FALSE;
}

BOOL CHotkeyDlg::SearchScheduledTask(HTREEITEM hItem, DWORD dwTaskId)
{
	hItem = m_tvCommands.GetChildItem(hItem);
	for (;;)
	{
		if (!hItem) break;
		INIT_ITEM_DATA(hItem);
		if (pItemData->m_nType == CItemData::Command)
		{
			for (int i = 0; i<pItemData->m_hkcommand.GetScheduledTaskCount(); i++)
			{
				CScheduledTask scheduled_task = pItemData->m_hkcommand.GetScheduledTaskAt(i);
				if (scheduled_task.GetTaskId() == dwTaskId)
				{
					ExecuteCommand("Scheduler", pItemData->m_hkcommand);
					return true;
				}
			}
		}
		if (SearchScheduledTask(hItem, dwTaskId)) return true;
		hItem = m_tvCommands.GetNextSiblingItem(hItem);
	}
	return FALSE;
}

LRESULT CHotkeyDlg::OnHotKey(WPARAM wParam, LPARAM lParam)
{
	UINT uIdHotKey = (UINT)wParam;			// identifier of hot key 
	UINT fuModifiers = (UINT)LOWORD(lParam);	// key-modifier flags 
	UINT uVirtKey = (UINT)HIWORD(lParam);	// virtual-key code

	if (uIdHotKey == 0)	//	Alt+Pause
	{
#ifdef _DEBUG
		ActionPasteText(this, "desc", "action", "coucoucou");
		return 0;
#endif
		RestoreWindow();
		return 0;
	}
	if (uIdHotKey == 1)	//	Alt+Shift+Pause
	{
		OnButtonQuit();
		return 0;
	}
	if (uIdHotKey == 2)	//	Win+Esc
	{
		m_ptaskwindowlist = new CTaskWindowList;
		m_ptaskwindowlist->Create();
		m_ptaskwindowlist->Show(true, true);
		return 0;
	}
	SearchHotkey(TVI_ROOT, uIdHotKey);
	//ASSERT(FALSE);
	return 0;
}

void WINAPI CHotkeyDlg::OnScheduledTask(DWORD dwTaskId, void * param)
{
	LOG3F("OnScheduledTask id:%d\n", dwTaskId);
	CHotkeyDlg& Dlg = *((CHotkeyDlg *)param);

	Dlg.SearchScheduledTask(TVI_ROOT, dwTaskId);
}

void CHotkeyDlg::EnableControls()
{
	INIT_ITEM_SEL;
	CItemData * pItemData = NULL;
	if (hItemSel) pItemData = (CItemData *)m_tvCommands.GetItemData(hItemSel);

	BOOL fGroupSel = FALSE,
		fHotkeySel = FALSE,
		fActionSel = FALSE;

	if (hItemSel) fGroupSel = pItemData->m_nType == CItemData::Group;
	if (hItemSel) fHotkeySel = pItemData->m_nType == CItemData::Command;
	if (hItemSel) fActionSel = pItemData->m_nType == CItemData::Action;

	LOG1F("%d %d %d %d\n", hItemSel, fGroupSel, fHotkeySel, pItemData ? pItemData->m_nType : -1);

	GetDlgItem(IDC_BUTTON_ADD_GROUP)->EnableWindow(!fHotkeySel && !fActionSel);
	GetDlgItem(IDC_BUTTON_ADD_COMMAND)->EnableWindow(fGroupSel);
	GetDlgItem(IDC_BUTTON_DEL_GROUP)->EnableWindow(fGroupSel);
	GetDlgItem(IDC_BUTTON_EDIT_GROUP)->EnableWindow(fGroupSel);

	GetDlgItem(IDC_BUTTON_EDIT_COMMAND)->EnableWindow(fHotkeySel);
	GetDlgItem(IDC_BUTTON_TEST_COMMAND)->EnableWindow(fHotkeySel);
	GetDlgItem(IDC_BUTTON_COPY_COMMAND)->EnableWindow(fHotkeySel);
	GetDlgItem(IDC_BUTTON_DEL_COMMAND)->EnableWindow(fHotkeySel);
}

BOOL SuspendState(BOOL fHibernate)
{
	return SetSystemPowerState(!fHibernate, true);
}

BOOL GetDefaultClient(CString& sDefClient, const char * szClientType)
{
	CString sRegKey = "SOFTWARE\\Clients";
	sRegKey += "\\";
	sRegKey += szClientType;
	CRegistry reg(HKEY_LOCAL_MACHINE, sRegKey);
	if (!reg.Open(FALSE)) return FALSE;
	char szDefClientName[100];
	if (!reg.QueryValue("", szDefClientName, sizeof(szDefClientName))) return FALSE;
	sRegKey += "\\";
	sRegKey += szDefClientName;
	sRegKey += "\\shell\\open\\command";
	CRegistry reg2(HKEY_LOCAL_MACHINE, sRegKey);
	if (!reg2.Open(FALSE)) return FALSE;
	if (!reg2.QueryValue("", sDefClient.GetBuffer(MAX_PATH + 1), MAX_PATH + 1)) return FALSE;
	return true;
}

void CHotkeyDlg::ExecuteDefClient(const char * szClientType, const char * szClientName, BOOL fExecutionMinimized)
{
	CString sDefClient;
	if (!GetDefaultClient(sDefClient, szClientType))
	{
		CString sMsg;
		sMsg.Format("!Erreur: Impossible de trouver le logiciel de %s par d�faut", szClientName);
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
		return;
	}
	if (!ExecuteModule(sDefClient, GetCurrentDirectoryStr(), FALSE, SHOW_VALUE))
	{
		CString sMsg;
		sMsg.Format("!Erreur: chargement du logiciel de %s impossible. (%s)", szClientName, GetLastErrorString());
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
	}
}

void CHotkeyDlg::ExecuteNewMailMessage()
{
	/*
	CMapiEx mapiex;
	CString sError;

	if (! mapiex.Init(&sError))
	{
	CString sMsg;
	sMsg.Format("!Erreur: Aucun logiciel de messagerie compatible MAPI ne peut �tre ex�cut� (%s)", mapiex.GetStrMAPILastError());
	::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION|MB_SYSTEMMODAL);
	return;
	}
	if (! mapiex.Send("", "", &sError, true))
	{
	if (mapiex.GetMAPILastError() != MAPI_USER_ABORT)
	{
	CString sMsg;
	sMsg.Format("!Erreur: l'�criture d'un nouveau message est impossible (%s)", mapiex.GetStrMAPILastError());
	::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION|MB_SYSTEMMODAL);
	}
	return;
	}
	*/
	ShellExecute(NULL, "open", "mailto:", NULL, NULL, SW_SHOWNORMAL);
}

void CHotkeyDlg::ExecProgram(const char * szProgram, const char * szTitle)
{
	if (ShellExecute(NULL, "open", szProgram, NULL, NULL, SW_SHOWNORMAL) < (HANDLE)32)
	{
		CString sMsg;
		sMsg.Format("!Erreur: chargement %s impossible. (%s)", szTitle, GetLastErrorString());
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
	}
}

BOOL CHotkeyDlg::ConfirmBox(HWND hWnd, const char * szText)
{
	CString sMsg;
	sMsg.Format("Voulez-vous d�finitivement %s ?", szText);
	return ::MessageBox(hWnd, sMsg, STR_APPNAME, MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 | MB_SYSTEMMODAL) == IDYES;
}

void CHotkeyDlg::OpenSpecialFolder(int nSFIndex)
{
	CSpecialFolder SF;
	CString sSF = SF.Get(nSFIndex);
	if (sSF != "")
	{
		ExecProgram(sSF, "dossier sp�cial");
	}
}

#if 0
typedef void (WINAPI * PLockWorkStation)();
void LockWorkStation()
{
	HINSTANCE hLib = LoadLibrary("USER32.DLL");
	if (!hLib) return;

	PLockWorkStation pLockWorkStation = (PLockWorkStation)GetProcAddress(hLib, "LockWorkStation");
	if (pLockWorkStation)
	{
		pLockWorkStation();
	}
	FreeLibrary(hLib);
}
#endif

void CHotkeyDlg::ActionMouseMove()
{
#define MOVE_OFFSET	2
	INPUT Input;
	Input.type = INPUT_MOUSE;
	Input.mi.mouseData = 0;
	Input.mi.dwExtraInfo = 0;
	Input.mi.dwFlags = MOUSEEVENTF_MOVE;
	Input.mi.time = 0;
	Input.mi.dx = MOVE_OFFSET;
	Input.mi.dy = MOVE_OFFSET;
	SendInput(1, &Input, sizeof(Input));
	POINT pt;
	GetCursorPos(&pt);
	LDA2("CursorPos-1: %d,%d", pt.x, pt.y);
	Sleep(200);
	Input.mi.dx = -MOVE_OFFSET;
	Input.mi.dy = -MOVE_OFFSET;
	SendInput(1, &Input, sizeof(Input));
	GetCursorPos(&pt);
	LDA2("CursorPos-2: %d,%d", pt.x, pt.y);
}

void CHotkeyDlg::ActionMouseLeftClick()
{
	INPUT Input;
	Input.type = INPUT_MOUSE;
	Input.mi.mouseData = 0;
	Input.mi.dwExtraInfo = 0;
	Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
	Input.mi.time = 0;
	Input.mi.dx = 0;
	Input.mi.dy = 0;
	SendInput(1, &Input, sizeof(Input));
	Sleep(200);
	Input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
	SendInput(1, &Input, sizeof(Input));
}

void CHotkeyDlg::ActionMouseRightClick()
{
	INPUT Input;
	Input.type = INPUT_MOUSE;
	Input.mi.mouseData = 0;
	Input.mi.dwExtraInfo = 0;
	Input.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
	Input.mi.time = 0;
	Input.mi.dx = 0;
	Input.mi.dy = 0;
	SendInput(1, &Input, sizeof(Input));
	Sleep(200);
	Input.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
	SendInput(1, &Input, sizeof(Input));
}

BOOL IsFileExecutble(const char * szFileName)
{
	if (StriFind(szFileName, ".exe") != NULL) return true;
	if (StriFind(szFileName, ".com") != NULL) return true;
	if (StriFind(szFileName, ".bat") != NULL) return true;
	if (StriFind(szFileName, ".cmd") != NULL) return true;
	return FALSE;
}

BOOL CHotkeyDlg::ActionOpenFile(const char * szDescription, const char * szActionString, const char * szProgram, BOOL fExecutionMinimized)
{
	CString sTip;
	if (CHotkeyDlg::m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	CString sMsg;
	if (IsDirectoryExist(TranslateEnv(szProgram, false)))
	{
		if (ShellExecute(NULL, "open", TranslateEnv(szProgram, false), NULL, NULL, SHOW_VALUE) < (HANDLE)32)
		{
			sMsg.Format("!Erreur: ouverture du dossier '%s' impossible. (%s)", szProgram, GetLastErrorString());
			::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
		}
	}
	else if (!IsFileExecutble(szProgram))
	{
		if (ShellExecute(NULL, "open", TranslateEnv(szProgram, false), NULL, NULL, SHOW_VALUE) < (HANDLE)32)
		{
			sMsg.Format("!Erreur: ouverture du fichier '%s' impossible. (%s)", szProgram, GetLastErrorString());
			::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
		}
	}
	else if (!ExecuteModule(TranslateEnv(szProgram, false), GetCurrentDirectoryStr(), FALSE, SHOW_VALUE))
		//else if (ShellExecute(NULL, "open", szProgram, NULL, SW_SHOWNORMAL) < (HANDLE)32)
	{
		sMsg.Format("!Erreur: ex�cution du programme '%s' impossible. (%s)", szProgram, GetLastErrorString());
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
	}
	return true;
}

BOOL CHotkeyDlg::ActionGotoURL(const char * szDescription, const char * szActionString, const char * szURL, BOOL fExecutionMinimized)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	CString sMsg;
	if (ShellExecute(NULL, "open", szURL, NULL, NULL, SHOW_VALUE) < (HANDLE)32)
	{
		sMsg.Format("!Erreur: connexion � l'URL '%s' impossible. (%s)", szURL, GetLastErrorString());
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
	}
	return true;
}

BOOL CHotkeyDlg::ActionGotoURLFavorite(const char * szDescription, const char * szActionString, const char * szURLFavorite, BOOL fExecutionMinimized)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	CSpecialFolder sf;
	CString sURLFileName = sf.Get(CSpecialFolder::Favorites) + "\\" + szURLFavorite + ".url";

	CIniFile urlfile(sURLFileName);
	CIniSection is(urlfile, "InternetShortcut");

	CString sMsg;
	char szURL[4000];
	if (!is.QueryValue("URL", szURL, sizeof(szURL)))
	{
		sMsg.Format("!Erreur: r�cup�ration de l'URL pour '%s' impossible.", szURLFavorite);
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
	}
	else
	{
		LOG1F("Execution: %d %d %d \n", SHOW_VALUE, SW_SHOWMINNOACTIVE, SW_SHOWNORMAL);
		if (ShellExecute(NULL, "open", szURL, NULL, NULL, SHOW_VALUE) < (HANDLE)32)
		{
			sMsg.Format("!Erreur: connexion � l'URL '%s' impossible. (%s)", szURL, GetLastErrorString());
			::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
		}
	}
	return true;
}

BOOL CHotkeyDlg::ActionSound(const char * szDescription, const char * szActionString, UINT uSubActionId)
{
	CString sTip;

	CString sMsg;
	switch (uSubActionId)
	{
	case SOUND_VOLUME_UP:
	case SOUND_VOLUME_DOWN:
	{
		CMixer mixer;
		if (mixer.Open())
		{
			uSubActionId == SOUND_VOLUME_UP ? mixer.VolumeUp(3) : mixer.VolumeDown(3);

			DWORD dwVolValue, dwMaxBound;
			mixer.GetVolumeValue(dwVolValue, NULL, &dwMaxBound);
			BOOL fMuteStatus;
			mixer.GetMuteStatus(fMuteStatus);
			if (m_fTipDisplayDetails)
				sTip.Format("%s - %s [%u%%,%u%%][%s]", szDescription, szActionString, (LOWORD(dwVolValue) * 100) / dwMaxBound, (HIWORD(dwVolValue) * 100) / dwMaxBound, fMuteStatus ? "HP D�sactiv�s" : "HP Activ�s");
			else
				sTip.Format("%s - %s [%u%%,%u%%][%s]", szDescription, szActionString, (LOWORD(dwVolValue) * 100) / dwMaxBound, (HIWORD(dwVolValue) * 100) / dwMaxBound, fMuteStatus ? "HP D�sactiv�s" : "HP Activ�s");
			ShowTip(sTip);
		}
		else
		{
			sTip.Format("%s - %s [%s]", szDescription, szActionString, "Volume non accessible");
			ShowTip(sTip);
		}
	}
	break;
	case SOUND_MUTE_ON:
	case SOUND_MUTE_OFF:
	case SOUND_MUTE_TOGGLE:
	{
		CMixer mixer;
		if (mixer.Open())
		{
			if (uSubActionId == SOUND_MUTE_TOGGLE)
			{
				mixer.ToggleMuteStatus();
			}
			else mixer.SetMuteStatus(uSubActionId == SOUND_MUTE_ON);

			DWORD dwVolValue, dwMaxBound;
			mixer.GetVolumeValue(dwVolValue, NULL, &dwMaxBound);
			BOOL fMuteStatus;
			mixer.GetMuteStatus(fMuteStatus);
			if (m_fTipDisplayDetails)
				sTip.Format("%s - %s [%u%%,%u%%][%s]", szDescription, szActionString, (LOWORD(dwVolValue) * 100) / dwMaxBound, (HIWORD(dwVolValue) * 100) / dwMaxBound, fMuteStatus ? "HP D�sactiv�s" : "HP Activ�s");
			else
				//sTip.Format("%s", szDescription);
				sTip.Format("%s - %s [%u%%,%u%%][%s]", szDescription, szActionString, (LOWORD(dwVolValue) * 100) / dwMaxBound, (HIWORD(dwVolValue) * 100) / dwMaxBound, fMuteStatus ? "HP D�sactiv�s" : "HP Activ�s");
			ShowTip(sTip);
		}
		else
		{
			sTip.Format("%s - %s [%s]", szDescription, szActionString, "Volume non accessible");
			ShowTip(sTip);
		}
	}
	break;
	default:
		ASSERT(FALSE);
		break;
	}
	return true;
}

BOOL CHotkeyDlg::ActionRASDialUp(const char * szDescription, const char * szActionString, const char * szEntry)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	RASENTRY * pRasEntry;
	RASDIALPARAMS RasDialParams;
	BOOL fPassword;

	if (!CRasDial::GetEntryDialParams(szEntry, &RasDialParams, &fPassword))
	{
		MB_ERR_NULL("R�cup�ration param�tres de la connexion impossible.");
		return FALSE;
	}

	if (!CRasDial::GetEntryProperties(szEntry, &pRasEntry))
	{
		MB_ERR_NULL("R�cup�ration param�tres de la connexion impossible.\n");
		return FALSE;
	}
	StringCchCopy(RasDialParams.szPhoneNumber, sizeof(RasDialParams.szPhoneNumber), pRasEntry->szLocalPhoneNumber);
	CRasDial::MemoryFree(pRasEntry);


	RASCREDENTIALS RasCredentials;
	RasCredentials.dwSize = sizeof(RASCREDENTIALS);
	RasCredentials.dwMask = RASCM_Password;
	DWORD dwRasCr = RasGetCredentials(NULL, szEntry, &RasCredentials);
	StringCchCopy(RasDialParams.szPassword, sizeof(RasDialParams.szPassword), RasCredentials.szPassword);
	CString sError;
	CRasDial RasDial(szEntry);
	if (!RasDial.Dial(&RasDialParams, NULL, &sError))
	{
		::MessageBox(NULL, sError, STR_APPNAME, MB_ICONSTOP);
		return FALSE;
	}
	return true;
}

BOOL CHotkeyDlg::ActionScreenSaver(const char * szDescription, const char * szActionString, const char * szScreenSaver)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	CString sCommand;
	sCommand.Format("%s\\system32\\%s", GetEnv("SYSTEMROOT"), szScreenSaver);
	//CString sPathPN;
	//sPathPN.Format("%s", m_sPathPNICA, szICAConnection);
	//if (ShellExecute(NULL, "open", sPathPN, sParameters, NULL, SHOW_VALUE) < (HANDLE)32)
	if (ShellExecute(NULL, "open", sCommand, NULL, NULL, SW_SHOWNORMAL) < (HANDLE)32)
	{
		CString sMsg;
		sMsg.Format("!Erreur: lancement de l'�conomiseur d'�cran '%s' impossible. (%s)", szScreenSaver, GetLastErrorString());
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
	}
	return true;
}

BOOL CHotkeyDlg::ActionICAConnection(const char * szDescription, const char * szActionString, const char * szICAConnection, BOOL fExecutionMinimized)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	CString sParameters;
	sParameters.Format("/APP \"%s\"", szICAConnection);
	//CString sPathPN;
	//sPathPN.Format("%s", m_sPathPNICA, szICAConnection);
	//if (ShellExecute(NULL, "open", sPathPN, sParameters, NULL, SHOW_VALUE) < (HANDLE)32)
	if (ShellExecute(NULL, "open", m_sPathPNICA, sParameters, NULL, SHOW_VALUE) < (HANDLE)32)
	{
		CString sMsg;
		sMsg.Format("!Erreur: lancement de la connexion ICA '%s' impossible. (%s)", szICAConnection, GetLastErrorString());
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
	}
	return true;
}

BOOL CHotkeyDlg::ActionTimer(const char * szDescription, const char * szActionString, DWORD dwTimerValue)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	CDateRef drStart, drCur;
	drStart.SetCurrentLocalTime();
	for (;;)
	{
		drCur.SetCurrentLocalTime();
		if (drCur - drStart >= dwTimerValue) break;
		Sleep(1);
	}
	return true;
}

BOOL CHotkeyDlg::ActionPasteText(CWnd * pWnd, const char * szDescription, const char * szActionString, const char * szPasteText)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	//ShowTip(sTip);
#ifdef _DEBUG
	LOG1F("ActionPasteText:%s\n", szPasteText);
	if (!::OpenClipboard(*pWnd)) return false;
	if (EmptyClipboard())
	{
		HANDLE hData = GlobalAlloc(GMEM_FIXED, _tcslen(szPasteText) + 1);
		LPSTR lpStr = (LPSTR)GlobalLock(hData);
		StringCchCopy(lpStr, _tcslen(szPasteText), szPasteText);
		GlobalUnlock(hData);
		SetClipboardData(CF_TEXT, hData);
	}
	CloseClipboard();

	/*AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(),NULL), GetCurrentThreadId(), TRUE);
	hWndFocused = ::GetFocus();
	LOG1F("hWndFocus:%08lX\n", hWndFocused);
	AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(),NULL), GetCurrentThreadId(), FALSE);
	*/
	//pWnd->SetForegroundWindow();

	BOOL fShiftInsert = true;
	AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(), NULL), GetCurrentThreadId(), TRUE);
	if (!fShiftInsert)
	{
		//	Send Ctrl+V to focused window
		keybd_event(VK_CONTROL, 0, 0, 0);
		keybd_event('V', 0, 0, 0);
		Sleep(10);
		keybd_event('V', 0, KEYEVENTF_KEYUP, 0);
		keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);
	}
	else
	{
		keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), 0, 0);
		keybd_event(VK_INSERT, MapVirtualKey(VK_INSERT, 0), KEYEVENTF_EXTENDEDKEY, 0);
		Sleep(10);
		keybd_event(VK_INSERT, MapVirtualKey(VK_INSERT, 0), KEYEVENTF_KEYUP, 0);
		keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), KEYEVENTF_KEYUP, 0);
	}
	AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(), NULL), GetCurrentThreadId(), FALSE);

	//::SetForegroundWindow(hWndFocused);
#endif
	return true;
}

BOOL CHotkeyDlg::ActionWinAmp(const char * szDescription, const char * szActionString, UINT uSubActionId)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	static struct winamp_map_cmd
	{
		UINT uHMCmd;
		WPARAM wWinampCmd;
	} s_winamp_map_cmd[] = {
		{ A_WINAMP_PLAY, WA_PLAY },
		{ A_WINAMP_PAUSE, WA_PAUSE },
		{ A_WINAMP_STOP, WA_STOP },
		{ A_WINAMP_NEXT_TRACK, WA_NEXT_TRACK },
		{ A_WINAMP_PREVIOUS_TRACK, WA_PREVIOUS_TRACK },
		{ A_WINAMP_VOLUME_UP, WA_VOLUME_UP },
		{ A_WINAMP_VOLUME_DOWN, WA_VOLUME_DOWN },
	};

	CWnd * pWndWinAmp = FindWindow(WINAMP_CLASS, NULL);
	if (!pWndWinAmp) return true;
	for (int i = 0; i<sizeof(s_winamp_map_cmd) / sizeof(s_winamp_map_cmd[0]); i++)
	{
		if (s_winamp_map_cmd[i].uHMCmd == uSubActionId)
		{
			pWndWinAmp->SendMessage(WM_COMMAND, s_winamp_map_cmd[i].wWinampCmd, 0);
			return true;
		}
	}
	ASSERT(FALSE);

	return true;
}

void ConvertString(CString& string)
{
	string.Replace(" ", "+");
}

BOOL CHotkeyDlg::ActionSearchEngine(const char * szDescription, const char * szActionString, const char * szSearchEngine)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	CGotoURLDlg dlg(m_registryConfig);
	dlg.m_fQueryStringMode = true;
	dlg.m_sTitle = "Rechercher sur le web...";
	dlg.m_sItem = "Cha�ne:";
	if (dlg.DoModal() != IDOK) return false;

	ConvertString(dlg.m_sGotoURL);
	//	http://www.google.fr/search?hl=fr&q=search+string&meta=
	CString sURL;
	sURL.Format("%s/search?hl=fr&q=%s&meta=", szSearchEngine, dlg.m_sGotoURL);
	if (ShellExecute(NULL, "open", sURL, NULL, NULL, SW_SHOWNORMAL) < (HANDLE)32)
	{
		CString sMsg;
		sMsg.Format("!Erreur: recharche sur '%s' impossible. (%s)", sURL, GetLastErrorString());
		::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
	}
	return true;
}

BOOL CHotkeyDlg::ActionDisplayMessage(const char * szDescription, const char * szActionString, const char * szDisplayMessage)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	::MessageBox(NULL, szDisplayMessage, "Hotkey : message", MB_ICONEXCLAMATION | MB_SYSTEMMODAL);
	return true;
}

BOOL CHotkeyDlg::GotoURL(BOOL fExecutionMinimized)
{
	CGotoURLDlg Dlg(m_registryConfig);

	if (Dlg.DoModal() == IDOK)
	{
		CString sURL;
		if (Dlg.m_sGotoURL.Find("http://") == -1)
		{
			sURL = "http://";
			sURL += Dlg.m_sGotoURL;
		}
		else
		{
			sURL = Dlg.m_sGotoURL;
		}
		if (ShellExecute(NULL, "open", sURL, NULL, NULL, SHOW_VALUE) < (HANDLE)32)
		{
			CString sMsg;
			sMsg.Format("!Erreur: connexion � l'URL '%s' impossible. (%s)", Dlg.m_sGotoURL, GetLastErrorString());
			::MessageBox(NULL, sMsg, STR_APPNAME, MB_ICONINFORMATION | MB_SYSTEMMODAL);
		}
	}
	return true;
}

BOOL CHotkeyDlg::ActionMisc(const char * szDescription, const char * szActionString, UINT uSubActionId, BOOL fExecutionMinimized)
{
	CString sTip;
	if (m_fTipDisplayDetails)
		sTip.Format("%s - %s", szDescription, szActionString);
	else
		sTip.Format("%s", szDescription);
	ShowTip(sTip);

	switch (uSubActionId)
	{
	case A_MISC_EXPLORER:
		ExecProgram("explorer.exe", "de explorateur Windows");
		break;
	case A_MISC_BROWSER_CLIENT:
		ExecuteDefClient("StartMenuInternet", "navigation", fExecutionMinimized);
		break;
	case A_MISC_MAIL_CLIENT:
		ExecuteDefClient("Mail", "messagerie", fExecutionMinimized);
		break;
	case A_MISC_MAIL_CONTACT:
		ExecuteDefClient("Contacts", "contact", fExecutionMinimized);
		break;
	case A_MISC_RECYCLE_BIN:
		OpenSpecialFolder(CSpecialFolder::RecycleBin);
		break;
	case A_MISC_MAIL_CALENDAR:
		ExecuteDefClient("Calendar", "calendrier", fExecutionMinimized);
		break;
	case A_MISC_NEWSGROUP_CLIENT:
		ExecuteDefClient("News", "newsgroup", fExecutionMinimized);
		break;
	case A_MISC_GOTOURL:
		GotoURL(fExecutionMinimized);
		break;
	case A_MISC_MAIL_NEW_MESSAGE:
		ExecuteNewMailMessage();
		break;
	case A_MISC_MEDIA_CLIENT:
		ExecuteDefClient("Media", "lecture audio/vid�o", fExecutionMinimized);
		break;
	case A_MISC_TASK_MANAGER:
		ExecProgram("taskmgr.exe", "du gestionnaire des t�ches Windows");
		break;
	case A_MISC_LOGOFF:
		if (ConfirmBox(NULL, "fermer la session Windows"))
			ExitWindowsEx(EWX_LOGOFF, 0);
		break;
	case A_MISC_POWEROFF:
		if (ConfirmBox(NULL, "�teindre l'odinateur"))
			ExitWindowsEx(EWX_POWEROFF, 0);
		break;
	case A_MISC_REBOOT:
		if (ConfirmBox(NULL, "red�marrer l'ordinateur"))
			ExitWindowsEx(EWX_REBOOT, 0);
		//SetSuspendState();
		break;
	case A_MISC_LOCK_WORKSTATION:
		LockWorkStation();
		break;
	case A_MISC_SUSPEND:
		if (!SuspendState(FALSE)) {}
		break;
	case A_MISC_HIBERNATE:
		if (!SuspendState(true)) {}
		break;
	case A_MISC_SF_MYDOCUMENTS:
		OpenSpecialFolder(CSpecialFolder::MyDocuments);
		break;
	case A_MISC_SF_DOCTEMPLATES:
		OpenSpecialFolder(CSpecialFolder::DocumentTemplates);
		break;
	case A_MISC_SF_MRD_LIST:
		OpenSpecialFolder(CSpecialFolder::RecentFiles);
		break;
	case A_MISC_SF_NETHOOD:
		OpenSpecialFolder(CSpecialFolder::NetworkNeighborhood);
		break;
	case A_MISC_MOUSEMOVE:
		ActionMouseMove();
		break;
	case A_MISC_MOUSELEFTCLICK:
		ActionMouseLeftClick();
		break;
	case A_MISC_MOUSERIGHTCLICK:
		ActionMouseRightClick();
		break;
	default:
		ASSERT(FALSE);
		break;
	}
	return true;
}

BOOL CHotkeyDlg::ExecuteAction(CWnd * pWnd, const char * szDescription, const CAction& action, BOOL fExecutionMinimized)
{
	CString sAction = (const char *)action.GetActionString();
	switch (action.m_uActionId)
	{
	case ACTION_OPEN_FILE:
		ActionOpenFile(szDescription, sAction, action.m_sProgram, fExecutionMinimized);
		break;
	case ACTION_GOTO_URL:
		ActionGotoURL(szDescription, sAction, action.m_sURL, fExecutionMinimized);
		break;
	case ACTION_GOTO_URL_FAVORITE:
		ActionGotoURLFavorite(szDescription, sAction, action.m_sURLFavorite, fExecutionMinimized);
		break;
	case ACTION_SOUND:
		ActionSound(szDescription, sAction, action.m_uSubActionId);
		break;
	case ACTION_RAS_DIALUP:
		ActionRASDialUp(szDescription, sAction, action.m_sRASEntry);
		break;
	case ACTION_ICA_CONNECTION:
		ActionICAConnection(szDescription, sAction, action.m_sICAConnection, fExecutionMinimized);
		break;
	case ACTION_SCREEN_SAVER:
		ActionScreenSaver(szDescription, sAction, action.m_sScreenSaver);
		break;
	case ACTION_MISC:
		ActionMisc(szDescription, sAction, action.m_uSubActionId, fExecutionMinimized);
		break;
	case ACTION_TIMER:
		ActionTimer(szDescription, sAction, action.m_dwTimerValue);
		break;
	case ACTION_PASTE_TEXT:
		ActionPasteText(pWnd, szDescription, sAction, action.m_sPasteText);
		break;
	case ACTION_WINAMP:
		ActionWinAmp(szDescription, sAction, action.m_uSubActionId);
		break;
	case ACTION_SEARCH_ENGINE:
		ActionSearchEngine(szDescription, sAction, action.m_sSearchEngine);
		break;
	case ACTION_DISPLAY_MESSAGE:
		ActionDisplayMessage(szDescription, sAction, action.m_sDisplayMessage);
		break;
	default:
		ASSERT(FALSE);
		break;
	}
	return true;
}

struct tt_execdata
{
	CHKCommand hkcommand;
	CWnd * pWnd;
};

DWORD WINAPI CHotkeyDlg::ExecutionThread(void *pParam)
{
	//CHKCommand * pHKCommand = (CHKCommand *)pParam;

	tt_execdata * pExecData = (tt_execdata *)pParam;
	CHKCommand * pHKCommand = &pExecData->hkcommand;

	for (int i = 0; i<pHKCommand->GetActionCount(); i++)
	{
		if (pHKCommand->GetActionAt(i).m_fDesactivated)
		{
			continue;
		}
		ExecuteAction(pExecData->pWnd, pHKCommand->m_sDescription, pHKCommand->GetActionAt(i), pHKCommand->m_fExecutionMinimized);
	}
	delete pHKCommand;
	return 0;
}

BOOL CHotkeyDlg::ExecuteCommand(const char * szTriggerAction, CHKCommand& hkcommand)
{
	//	Appeler suite au schedul d'une tache ou declenchement d'une execution manuelle
	//CHKCommand * pHKCommand = new CHKCommand;
	//*pHKCommand = hkcommand;
	//CloseHandle(NewThread(ExecutionThread, pHKCommand));
	if (!hkcommand.m_fActive)
	{
		LogExecution(szTriggerAction, "(d�sactiv�e-->ne sera pas ex�cut�e)", hkcommand);
		return false;
	}

	LogExecution(szTriggerAction, "", hkcommand);
	hkcommand.m_dwNumOfExec++;
	tt_execdata * pExecData = new tt_execdata;
	pExecData->hkcommand = hkcommand;
	pExecData->pWnd = this;
	CloseHandle(NewThread(ExecutionThread, pExecData));
	return true;
}

void CHotkeyDlg::OnButtonAddCommand()
{
	// TODO: Add your control notification handler code here
	INIT_ITEM_SEL;
	CCommandPropertiesDlg Dlg;

	int nRet = (int)Dlg.DoModal();
	if (!Dlg.m_hkcommand.RegisterHotkeysAndScheduledTasks(*this, m_scheduler))
	{
		MessageBox("!Erreur: Enregistrement hotkey impossible", STR_APPNAME, MB_ICONSTOP);
	}
	if (nRet == IDOK)
	{
		Dlg.m_hkcommand.m_dwId = m_registryConfig.GetID(m_tvCommands);
		HTREEITEM hNewItem = AddCommand(hItemSel, Dlg.m_hkcommand);
		m_tvCommands.SelectItem(hNewItem);
		m_tvCommands.ExpandBranch(hNewItem);
		m_registryConfig.SaveHotkeysConfig(*this);
		CHANGE_FOCUS();
	}
}

void CHotkeyDlg::OnButtonEditCommand()
{
	// TODO: Add your control notification handler code here
	INIT_ITEM_SEL;
	INIT_ITEM_DATA(hItemSel);
	CCommandPropertiesDlg Dlg;

	pItemData->m_hkcommand.UnRegisterHotkeysAndScheduledTasks(m_scheduler);
	Dlg.m_hkcommand = pItemData->m_hkcommand;
	int nRet = (int)Dlg.DoModal();
	if (!Dlg.m_hkcommand.RegisterHotkeysAndScheduledTasks(*this, m_scheduler))
	{
		MessageBox("!Erreur: Enregistrement hotkey impossible", STR_APPNAME, MB_ICONSTOP);
	}
	pItemData->m_hkcommand = Dlg.m_hkcommand;
	if (nRet == IDOK)
	{
		SetCommand(hItemSel);
		MoveCommand(m_tvCommands.GetParentItem(hItemSel), hItemSel);
		m_tvCommands.ExpandBranch(hItemSel);
		m_registryConfig.SaveHotkeysConfig(*this);
		CHANGE_FOCUS();
	}
}

void CHotkeyDlg::OnButtonCopyCommand()
{
	// TODO: Add your control notification handler code here

	CCommandPropertiesDlg Dlg;
	INIT_ITEM_SEL;
	INIT_ITEM_DATA(hItemSel);
	Dlg.m_hkcommand = pItemData->m_hkcommand;
	Dlg.m_hkcommand.RemoveAllHotkeys();

	int nRet = (int)Dlg.DoModal();
	if (!Dlg.m_hkcommand.RegisterHotkeysAndScheduledTasks(*this, m_scheduler))
	{
		MessageBox("!Erreur: Enregistrement hotkey impossible", STR_APPNAME, MB_ICONSTOP);
	}
	if (nRet == IDOK)
	{
		Dlg.m_hkcommand.m_dwId = m_registryConfig.GetID(m_tvCommands);
		HTREEITEM hNewItem = AddCommand(m_tvCommands.GetParentItem(hItemSel), Dlg.m_hkcommand);
		m_tvCommands.SelectItem(hNewItem);
		m_tvCommands.ExpandBranch(hNewItem);
		m_registryConfig.SaveHotkeysConfig(*this);
		CHANGE_FOCUS();
	}
}

void CHotkeyDlg::OnButtonDelCommand()
{
	// TODO: Add your control notification handler code here
	if (MessageBox("Suppression de la s�quence s�lectionn�e ?", "Confirmation", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO) return;
	INIT_ITEM_SEL;
	DeleteCommand(hItemSel);
#ifndef _DEBUG0
	m_registryConfig.SaveHotkeysConfig(*this);
#endif
	CHANGE_FOCUS();
}

void CHotkeyDlg::CleanMemory(HTREEITEM hItemParent, BOOL fCleanParent)
{
	HTREEITEM hItem = m_tvCommands.GetChildItem(hItemParent);
	CItemData * pItemData;
	for (;;)
	{
		if (hItem == NULL) break;
		CleanMemory(hItem);
		pItemData = (CItemData *)m_tvCommands.GetItemData(hItem);
		delete pItemData;
		hItem = m_tvCommands.GetNextSiblingItem(hItem);
	}
	if (fCleanParent)
	{
		pItemData = (CItemData *)m_tvCommands.GetItemData(hItemParent);
		delete pItemData;
	}
}

void CHotkeyDlg::OnButtonTestCommand()
{
	// TODO: Add your control notification handler code here
	INIT_ITEM_SEL;
	INIT_ITEM_DATA(hItemSel);
	if (!pItemData->m_hkcommand.m_fActive)
	{
		CString sMsg;
		sMsg.Format("La commande '%s' est actuellement d�sactiv�e", pItemData->m_hkcommand.m_sDescription);
		MessageBox(sMsg, "Information", MB_ICONINFORMATION);
		return;
	}
	ExecuteCommand("Interactive", pItemData->m_hkcommand);
}

LPCTSTR LABEL(int id)
{
	switch (id)
	{
	case 105:
		return "Version";
	case 8600:
		return "A propos de...";
	case 8601:
		return "&Fermer";
	case 8602:
		return "Historique...";
	case 8603:
		return "License Freeware";
	case 1890:
		return "Merci d'indiquer vos �ventuelles remarques, questions, remont�es de bug, etc...";
	default:
		ASSERT(false);
		return "[NoText]";
	}
}

//#include "RegAccess.h"

DEFINE_VERSION

CString GetModuleVersion()
{
	CString s;
	s.Format(_DT"%u.%u.%u", NUM_VERSION_MAJOR, NUM_VERSION_MINOR, NUM_VERSION_REVISION);
	if (NUM_VERSION_SUBREVISION > 0)
	{
		s += _DT".";
		TCHAR c = '0' + NUM_VERSION_SUBREVISION;
		s += c;
	}
	return s;
}

CString GetAppVersion()
{
	CString s;
	s.Format(_DT"%s %s", LABEL(105), GetModuleVersion());
	return s;
}

CString GetAppBuild()
{
	CString strBuild;
	strBuild.Format(_DT"(Build %d)", BUILD_NUMBER);
	return strBuild;
}

CString GetAppBuildAndDate()
{
	CString strBuild;
	strBuild.Format(_DT"%s (Build %d)", GetModuleDateTimeStr(NULL, false), BUILD_NUMBER);
	return strBuild;
}

void CHotkeyDlg::OnButtonAbout()
{
	// TODO: Add your control notification handler code here
#define CONTACT_URL		STR_DEF_CONTACT
#define STR_WEBSITE		STR_DEF_WEBSITE
#define STR_WEBSITE_URL	STR_DEF_WEBSITE_URL

	CString sAppliName; \
		sAppliName.LoadString(IDS_MAIN_TITLE); \
		CSplashDlg SD(this, sAppliName, GetAppVersion(), GetAppBuildAndDate(), LABEL(8603), STR_WEBSITE, STR_WEBSITE_URL); \
		SD.m_sWindowText = LABEL(8600); \
		SD.m_sButtonClose = LABEL(8601); \
		SD.m_sHistory = LABEL(8602); \
		SD.m_sContact = STR_DEF_CONTACT; \
		SD.m_sContactURL.Format(_DT"%s?subject=[%s] %s&body=%s", CONTACT_URL, sAppliName, GetAppVersion(), LABEL(1890)); \
		SD.SetCenter(); \
		SD.SetIcon(AfxGetApp()->LoadIcon(MAKEINTRESOURCE(IDR_MAINFRAME))); \
		SD.SetHistoResID(IDR_HISTO); \
		SD.m_fHistoryHtml = true; \
		SD.DoModal();

	/*VERSION();
	BUILD();
	CString sAppliName;
	sAppliName.LoadString(IDS_MAIN_TITLE);
	CSplashDlg SD(this, sAppliName, strVersion, strBuild);
	SD.m_sCopyright = STR_COPYRIGHT;
	SD.SetCenter();
	SD.SetIcon(AfxGetApp()->LoadIcon(MAKEINTRESOURCE(IDR_MAINFRAME)));
	SD.SetHistoResID(IDR_HISTO);
	SD.DoModal();*/
}

void CHotkeyDlg::OnCheckStartWithWindows()
{
	// TODO: Add your control notification handler code here
	CShortcut::RegisterInStartupMenu(GetModuleFileNameStr(), IsDlgButtonChecked(IDC_CHECK_START_WITH_WINDOWS));
}

HTREEITEM CHotkeyDlg::AddGroup(HTREEITEM hParent, const char * szGroupName)
{
	CItemData * pItemData = new CItemData(CItemData::Group);

	HTREEITEM hItem = m_tvCommands.GetChildItem(hParent);
	HTREEITEM hItemPos = TVI_FIRST;
	for (;;)
	{
		if (!hItem)  break;
		CString sItemText = m_tvCommands.GetItemText(hItem);
		INIT_ITEM_DATA(hItem);
		if ((_tcsicmp(szGroupName, sItemText) < 0) || (pItemData->m_nType != CItemData::Group))
		{
			break;
		}
		hItemPos = hItem;
		hItem = m_tvCommands.GetNextSiblingItem(hItem);
	}


	HTREEITEM hNewItem = m_tvCommands.InsertItem(szGroupName, 0, 0, hParent, hItemPos);
	m_tvCommands.SetItemData(hNewItem, (DWORD_PTR)pItemData);
	return hNewItem;
}

HTREEITEM CHotkeyDlg::MoveGroup(HTREEITEM hParent, HTREEITEM hItem)
{
	HTREEITEM hNewItem = AddGroup(hParent, m_tvCommands.GetItemText(hItem));
	m_tvCommands.CopySubItem(hNewItem, hItem);
	INIT_ITEM_DATA(hItem);
	delete pItemData;
	m_tvCommands.DeleteItem(hItem);
	return hNewItem;
}

void CHotkeyDlg::SetGroup(HTREEITEM hItem, const char * szGroupName)
{
	m_tvCommands.SetItemText(hItem, szGroupName);
}

void CHotkeyDlg::DeleteCommand(HTREEITEM hItem)
{
	INIT_ITEM_DATA(hItem);
	pItemData->m_hkcommand.UnRegisterHotkeysAndScheduledTasks(m_scheduler);
	CleanMemory(hItem, true);
	m_tvCommands.DeleteItem(hItem);
}

void CHotkeyDlg::DeleteGroupCommand(HTREEITEM hItemGroup)
{
	ASSERT(GetItemType(hItemGroup));
	HTREEITEM hItem = m_tvCommands.GetChildItem(hItemGroup);
	for (;;)
	{
		if (!hItem) break;
		if (GetItemType(hItem) == CItemData::Command)
		{
			INIT_ITEM_DATA(hItem);
			pItemData->m_hkcommand.UnRegisterHotkeysAndScheduledTasks(m_scheduler);
		}
		else if (GetItemType(hItem) == CItemData::Group)
		{
			DeleteGroupCommand(hItem);
		}
		hItem = m_tvCommands.GetNextSiblingItem(hItem);
	}
	CleanMemory(hItemGroup, true);
	m_tvCommands.DeleteItem(hItemGroup);
}

#ifndef _DEBUG
#define COMMAND_FORMAT(text,hk)		text.Format("%s [%s]", hk.m_sDescription, hk.m_fActive?"Activ�e":"D�sactiv�e"); \
									if (hk.GetHotkeyCount()>0) \
									{ text += " ["; \
									text += hk.GetHotkeyAt(0).GetHotkeyString(); \
									if (! hk.GetHotkeyAt(0).IsValid()) text += " !"; \
									text += "]"; \
									}
#else
#define COMMAND_FORMAT(text,hk)		text.Format("%s id:%d [%s]", hk.m_sDescription, hk.m_dwId, hk.m_fActive?"Activ�e":"D�sactiv�e"); \
									if (hk.GetHotkeyCount()>0) \
									{ text += " ["; \
									text += hk.GetHotkeyAt(0).GetHotkeyString(); \
									if (! hk.GetHotkeyAt(0).IsValid()) text += " !"; \
									text += "]"; \
									}
#endif

HTREEITEM CHotkeyDlg::AddCommand(HTREEITEM hParent, const CHKCommand& hk)
{
	CItemData * pItemData = new CItemData(CItemData::Command);
	pItemData->m_hkcommand = hk;
	CString sNewItemText;

	COMMAND_FORMAT(sNewItemText, pItemData->m_hkcommand);

	HTREEITEM hItem = m_tvCommands.GetChildItem(hParent);
	HTREEITEM hItemPos = TVI_FIRST;
	for (;;)
	{
		if (!hItem)  break;
		CString sItemText = m_tvCommands.GetItemText(hItem);
		INIT_ITEM_DATA(hItem);
		if ((_tcsicmp(sNewItemText, sItemText) < 0) && (pItemData->m_nType != CItemData::Group))
		{
			break;
		}
		hItemPos = hItem;
		hItem = m_tvCommands.GetNextSiblingItem(hItem);
	}
	HTREEITEM hNewItem = m_tvCommands.InsertItem(sNewItemText, 1, 1, hParent, hItemPos);
	m_tvCommands.SetItemData(hNewItem, (DWORD_PTR)pItemData);

	for (int i = 0; i<pItemData->m_hkcommand.GetActionCount(); i++)
	{
		CAction action = pItemData->m_hkcommand.GetActionAt(i);
		CString sAction = action.GetActionString();
		CItemData * pIDAction = new CItemData(CItemData::Action);
		HTREEITEM hItemAction = m_tvCommands.InsertItem(sAction, 2, 2, hNewItem);
		m_tvCommands.SetItemData(hItemAction, (DWORD_PTR)pIDAction);
	}
	for (int i = 0; i<pItemData->m_hkcommand.GetScheduledTaskCount(); i++)
	{
		CScheduledTask schedultask = pItemData->m_hkcommand.GetScheduledTaskAt(i);
		CString sTask = schedultask.GetScheduledTaskString();
		CItemData * pIDTask = new CItemData(CItemData::Task);
		HTREEITEM hItemTask = m_tvCommands.InsertItem(sTask, 3, 3, hNewItem);
		m_tvCommands.SetItemData(hItemTask, (DWORD_PTR)pIDTask);
	}
	return hNewItem;
}

void CHotkeyDlg::SetCommand(HTREEITEM hItem)
{
	INIT_ITEM_DATA(hItem);

	CString sText;
	COMMAND_FORMAT(sText, pItemData->m_hkcommand);
	m_tvCommands.SetItemText(hItem, sText);
	CleanMemory(hItem);
	m_tvCommands.DeleteChildren(hItem);

	for (int i = 0; i<pItemData->m_hkcommand.GetActionCount(); i++)
	{
		CAction action = pItemData->m_hkcommand.GetActionAt(i);
		CString sAction = action.GetActionString();
		CItemData * pIDAction = new CItemData(CItemData::Action);
		HTREEITEM hItemAction = m_tvCommands.InsertItem(sAction, 2, 2, hItem);
		m_tvCommands.SetItemData(hItemAction, (DWORD_PTR)pIDAction);
	}
}

HTREEITEM CHotkeyDlg::MoveCommand(HTREEITEM hParent, HTREEITEM hItemToMove)
{

	INIT_ITEM_DATA(hItemToMove);
	HTREEITEM hNewItem = AddCommand(hParent, pItemData->m_hkcommand);
	CleanMemory(hItemToMove, true);
	m_tvCommands.DeleteItem(hItemToMove);
	return hNewItem;
}

void CHotkeyDlg::OnSelchangedTreeCommandList(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	EnableControls();
	*pResult = 0;
}

void CHotkeyDlg::OnDblclkTreeCommandList(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	switch (GetItemType())
	{
	case CItemData::Group:
		OnButtonEditGroup();
		break;
	case CItemData::Command:
		OnButtonEditCommand();
		break;
	}
	*pResult = 1;
}

void CHotkeyDlg::OnButtonAddGroup()
{
	// TODO: Add your control notification handler code here
	INIT_ITEM_SEL;
	CGroupPropertiesDlg Dlg;
	if (Dlg.DoModal() == IDOK)
	{
		HTREEITEM hNewItem = AddGroup(Dlg.m_fRootGroup ? TVI_ROOT : hItemSel, Dlg.m_sGroupName);
		m_tvCommands.SelectItem(hNewItem);
		m_registryConfig.SaveHotkeysConfig(*this);
		CHANGE_FOCUS();
	}
}

void CHotkeyDlg::OnButtonEditGroup()
{
	// TODO: Add your control notification handler code here
	INIT_ITEM_SEL;
	CGroupPropertiesDlg Dlg;
	Dlg.m_sGroupName = m_tvCommands.GetItemText(hItemSel);
	Dlg.m_fEditMode = true;
	for (;;)
	{
		if (Dlg.DoModal() == IDCANCEL) break;
		if (!IsGroupExists(Dlg.m_sGroupName))
		{
			BOOL fIsExpanded = m_tvCommands.IsItemExpanded(hItemSel);
			LOG1F("fIsExpanded = %d\n", fIsExpanded);
			SetGroup(hItemSel, Dlg.m_sGroupName);
			HTREEITEM hNewItem = MoveGroup(m_tvCommands.GetParentItem(hItemSel), hItemSel);
			if (fIsExpanded)
				m_tvCommands.Expand(hNewItem, TVE_EXPAND);
			m_registryConfig.SaveHotkeysConfig(*this);
			CHANGE_FOCUS();
			break;
		}
		MessageBox("!Erreur: Ce nom de groupe existe d�j� dans le m�me niveau d'arborescence", "Attention", MB_ICONSTOP);
	}
}

void CHotkeyDlg::OnButtonDelGroup()
{
	// TODO: Add your control notification handler code here
	INIT_ITEM_SEL;
	CString sGroupName = m_tvCommands.GetItemText(hItemSel);
	CString sMsg;
	sMsg.Format("Etes vou s�r de vouloir supprimer le groupe '%s' et toute son arbrescence ?", sGroupName);

	if (MessageBox(sMsg, "Confirmation", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) != IDYES) return;

	DeleteGroupCommand(hItemSel);

	EnableControls();
#ifndef _DEBUG0
	m_registryConfig.SaveHotkeysConfig(*this);
#endif
	CHANGE_FOCUS();
}

int CHotkeyDlg::GetItemType(HTREEITEM hItem)
{
	if (!hItem) hItem = m_tvCommands.GetSelectedItem();
	CItemData * pItemData = (CItemData *)m_tvCommands.GetItemData(hItem);
	return pItemData->m_nType;
}

BOOL CHotkeyDlg::IsGroupExists(const char * szGroupName)
{
	//HTREEITEM hItem;
	return FALSE;
}

void CHotkeyDlg::OnBegindragTreeCommandList(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	m_hItemDrag = pNMTreeView->itemNew.hItem;
	int nItemType = GetItemType(m_hItemDrag);
	if (nItemType != CItemData::Group && nItemType != CItemData::Command) return;

	m_nDragTimerID = (int)SetTimer(1, 75, NULL);

	m_hItemDrop = NULL;
	m_pDragImage = m_tvCommands.CreateDragImage(m_hItemDrag);

	m_fDragging = true;
	m_pDragImage->BeginDrag(0, CPoint(10, 10));
	POINT pt = pNMTreeView->ptDrag;
	ClientToScreen(&pt);
	m_pDragImage->DragEnter(NULL, pt);
	SetCapture();

	*pResult = 0;
}

void CHotkeyDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	HTREEITEM hItem;
	UINT      flags;

	if (m_nHoverTimerID)
	{
		KillTimer(m_nHoverTimerID);
		m_nHoverTimerID = 0;
	}
	if (m_fDragging)
	{
		LOG1F("-------------------------\n");
		LOG1F("point.x=%d point.y=%d\n", point.x, point.y);
		POINT pt = point;
		ClientToScreen(&pt);
		CImageList::DragMove(pt);

		POINT c;
		GetCursorPos(&c);
		LOG1F("pt.x=%d pt.y=%d c.x=%d c.y=%d\n", pt.x, pt.y, c.x, c.y);
		m_tvCommands.ScreenToClient(&pt);

		LOG1F("pt.x=%d pt.y=%d\n", pt.x, pt.y);

		m_nHoverTimerID = (int)SetTimer(2, 750, NULL);
		m_pointHover = pt;

		if ((hItem = m_tvCommands.HitTest(pt, &flags)) != NULL)
		{
			CImageList::DragShowNolock(FALSE);
			m_tvCommands.SelectDropTarget(hItem);
			m_hItemDrop = hItem;
			CImageList::DragShowNolock(true);
		}
	}
	CDialog::OnMouseMove(nFlags, point);
}

void CHotkeyDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (m_fDragging)
	{
		KillTimer(m_nDragTimerID);
		m_fDragging = FALSE;
		CImageList::DragLeave(this);
		CImageList::EndDrag();
		ReleaseCapture();
		delete m_pDragImage;

		// Remove drop target highlighting
		m_tvCommands.SelectDropTarget(NULL);

		if (GAKS_SHIFT) m_hItemDrop = TVI_ROOT;
		if (m_hItemDrop == TVI_ROOT)
			if (GetItemType(m_hItemDrag) != CItemData::Group) return;
		if (m_hItemDrag == m_hItemDrop) return;
		if (m_hItemDrop != TVI_ROOT)
			if (GetItemType(m_hItemDrop) != CItemData::Group) return;
		if (m_tvCommands.GetParentItem(m_hItemDrag) == TVI_ROOT) return;
		if (m_tvCommands.GetParentItem(m_hItemDrag) == m_hItemDrop) return;

		LOG2F("Copie en cours...\n");
		int nItemType = GetItemType(m_hItemDrag);
		LOG1F("ItemDataType=%d\n", nItemType);
		HTREEITEM hNewItem;
		if (nItemType == CItemData::Command)
		{
			hNewItem = MoveCommand(m_hItemDrop, m_hItemDrag);
		}
		else
		{
			hNewItem = MoveGroup(m_hItemDrop, m_hItemDrag);
		}
		LOG2F("Apres move\n");
		m_tvCommands.SelectItem(hNewItem);
		LOG2F("Apres select\n");
		m_tvCommands.Expand(hNewItem, TVE_EXPAND);
		LOG2F("Apres expand\n");
		m_registryConfig.SaveHotkeysConfig(*this);
	}

	CDialog::OnLButtonUp(nFlags, point);
}

BOOL CHotkeyDlg::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	// TODO: Add your specialized code here and/or call the base class
	LPNMHDR pnmh = (LPNMHDR)lParam;
	//LOG1F("id=%d code=%08lX\n", pnmh->idFrom, pnmh->code);
	return CDialog::OnNotify(wParam, lParam, pResult);
}

void CHotkeyDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == m_nDragTimerID)
	{
		//	Auto scrolling
		m_nTimerTicks++;

		POINT pt;
		GetCursorPos(&pt);
		RECT rect;
		m_tvCommands.GetClientRect(&rect);
		m_tvCommands.ClientToScreen(&rect);

		// NOTE: Screen coordinate is being used because the call
		// to DragEnter had used the Desktop window.
		CImageList::DragMove(pt);

		HTREEITEM hFirstItemVisible = m_tvCommands.GetFirstVisibleItem();

		if (pt.y < rect.top + 10)
		{
			// We need to scroll up
			// Scroll slowly if cursor near the treeview control
			int slowscroll = 6 - (rect.top + 10 - pt.y) / 20;
			if (0 == (m_nTimerTicks % (slowscroll > 0 ? slowscroll : 1)))
			{
				CImageList::DragShowNolock(FALSE);
				m_tvCommands.SendMessage(WM_VSCROLL, SB_LINEUP);
				m_tvCommands.SelectDropTarget(hFirstItemVisible);
				m_hItemDrop = hFirstItemVisible;
				CImageList::DragShowNolock(true);
			}
		}
		else if (pt.y > rect.bottom - 10)
		{
			// We need to scroll down
			// Scroll slowly if cursor near the treeview control
			int slowscroll = 6 - (pt.y - rect.bottom + 10) / 20;
			if (0 == (m_nTimerTicks % (slowscroll > 0 ? slowscroll : 1)))
			{
				CImageList::DragShowNolock(FALSE);
				m_tvCommands.SendMessage(WM_VSCROLL, SB_LINEDOWN);
				int nCount = m_tvCommands.GetVisibleCount();
				for (int i = 0; i<nCount - 1; ++i)
					hFirstItemVisible = m_tvCommands.GetNextVisibleItem(hFirstItemVisible);
				if (hFirstItemVisible)
					m_tvCommands.SelectDropTarget(hFirstItemVisible);
				m_hItemDrop = hFirstItemVisible;
				CImageList::DragShowNolock(true);
			}
		}
	}
	if (nIDEvent == m_nHoverTimerID)
	{
		//	Auto expand
		LOG1F("nIDEvent == m_nHoverTimerID\n");
		KillTimer(m_nHoverTimerID);
		m_nHoverTimerID = 0;

		HTREEITEM	trItem = 0;
		UINT		uFlag = 0;
		trItem = m_tvCommands.HitTest(m_pointHover, &uFlag);
		LOG1F("trItem= %d\n", trItem);
		if (trItem)
		{
			m_tvCommands.SelectItem(trItem);
			m_tvCommands.Expand(trItem, TVE_EXPAND);
		}
	}
	if (nIDEvent == m_nDateTimeIdleTimerID)
	{
		DisplayDateTimeIdle();
	}
#ifdef _DEBUG
	if (nIDEvent == m_nDebugTimerID)
	{
		UpdateData(true);
		CWnd * pWnd = GetForegroundWindow();
		if (pWnd)
		{
			char szClass[200], szName[200];
			::GetClassName(*pWnd, szClass, sizeof(szClass));
			::GetWindowText(*pWnd, szName, sizeof(szName));
			m_sDebug.Format("Class='%s' Name='%s'", szClass, szName);
		}
		else
		{
			m_sDebug.Format("Indetermine");
		}
		UpdateData(FALSE);
	}
#endif

	CDialog::OnTimer(nIDEvent);
}

BOOL CHotkeyDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE && m_fDragging)
	{
		m_fDragging = FALSE;
		CImageList::DragLeave(NULL);
		CImageList::EndDrag();
		ReleaseCapture();
		m_tvCommands.SelectDropTarget(NULL);
		delete m_pDragImage;
		m_pDragImage = NULL;
		return true;		// DO NOT process further
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CHotkeyDlg::ShowTip(const char *szText)
{
	if (!m_fTipNotifier)
	{
		m_wndtip.SetText(szText);
		m_wndtip.Show(true, m_dwShowTipTime);
		m_wndtip.SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
	}
	else
	{
		m_wndNotifier.Show(szText);
		m_wndNotifier.SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
	}
}

void CHotkeyDlg::OnButtonPreferences()
{
	// TODO: Add your control notification handler code here
	CPreferencesDlg Dlg;

	Dlg.m_dwTipTimer = m_dwShowTipTime;
	Dlg.m_fTipDisplayDetails = m_fTipDisplayDetails;
	Dlg.m_wndtipSample = m_wndtip;
	Dlg.m_fNotifier = m_fTipNotifier;
	Dlg.m_sPathPNICA = m_sPathPNICA;

	if (Dlg.DoModal() == IDOK)
	{
		m_wndtip = Dlg.m_wndtipSample;
		m_dwShowTipTime = Dlg.m_dwTipTimer;
		m_fTipDisplayDetails = Dlg.m_fTipDisplayDetails;
		m_fTipNotifier = Dlg.m_fNotifier;
		m_sPathPNICA = Dlg.m_sPathPNICA;
		m_registryConfig.SavePreferences(*this);
	}
}

void CHotkeyDlg::OnButtonInfos()
{
	// TODO: Add your control notification handler code here
	CInfosDlg2 Dlg;
	Dlg.m_pscheduler = &m_scheduler;
	Dlg.m_ptvCommands = &m_tvCommands;
	Dlg.DoModal();
}


void CHotkeyDlg::OnButtonSchedul()
{
	// TODO: Add your control notification handler code here
	CSchedulDlg Dlg;

	Dlg.m_pscheduler = &m_scheduler;
	Dlg.m_ptvCommands = &m_tvCommands;
	Dlg.DoModal();
}


struct t_sockremoteparam
{
	LPARAM lParam;
	CSockAsync * pSock;
};

DWORD WINAPI CHotkeyDlg::RemoteSockThread(void * p)
{
	CHotkeyDlg& Dlg = *((CHotkeyDlg *)p);
	//LOG1F("

	return 0;
}

void CHotkeyDlg::LogExecution(const char *szTriggerAction, const char * szMoreInfo, const CHKCommand &hkcommand)
{
	CFileText ftLog;
	CString sLogFile = ConcatFileNameComposant(
		GetFileNameComposant(GetModuleFileNameStr(), GFNC_DIRECTORY), "HotkeyExec.log");
	if (!ftLog.OpenCreateAppend(sLogFile, CFile::modeWrite)) return;
	ftLog.Putsf("%s;%s%s;%s", CDateRef::GetCurrentDateTimeString(), szTriggerAction, szMoreInfo, hkcommand.m_sDescription);
	ftLog.Close();
}
