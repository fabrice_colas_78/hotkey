
// HotkeyDlg.h : header file
//

#pragma once
#include <afxdialogex.h>
#include <afxtempl.h>

#include <wndtip.h>
#include <treectrlex.h>
#include <log.h>
#include <winerr.h>
#include <registry.h>


#include "action.h"
#include "global.h"
#include "hotkeyc.h"
#include "ScheduledTask.h"

#include "WindowNotifier.h"
#include "TaskWindowList.h"

#include "SockRemote.h"

#include "ItemData.h"

#include "RegistryConfig.h"


// CHotkeyDlg dialog
#define WM_ICONNOTIFY	(WM_USER+1003)

class CHotkeyDlg : public CDialogEx
{
// Construction
public:
	CHotkeyDlg(CWnd* pParent = NULL);	// standard constructor
	~CHotkeyDlg();

	void LogExecution(const char * szTriggerAction, const char * szMoreInfo, const CHKCommand& khcommand);
	CString m_sTitle;
	void DisplayDateTimeIdle();
	static void WINAPI OnScheduledTask(DWORD dwTaskId, void * param);
	static DWORD WINAPI ExecutionThread(void * pParam);

	static BOOL ExecuteAction(CWnd * pWnd, const char * szDescription, const CAction& action, BOOL fExecutionMinimized);

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_HOTKEY_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);

	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	CTreeCtrlEx	m_tvCommands;
	CString	m_sDebug;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonQuit();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnButtonAbout();
	afx_msg void OnButtonAddCommand();
	afx_msg void OnButtonEditCommand();
	afx_msg void OnButtonDelCommand();
	afx_msg void OnButtonTestCommand();
	afx_msg void OnButtonCopyCommand();
	afx_msg void OnCheckStartWithWindows();
	afx_msg void OnSelchangedTreeCommandList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkTreeCommandList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonAddGroup();
	afx_msg void OnButtonDelGroup();
	afx_msg void OnButtonEditGroup();
	afx_msg void OnBegindragTreeCommandList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnButtonPreferences();
	afx_msg void OnButtonInfos();
	afx_msg void OnButtonSchedul();
	//}}AFX_MSG
	afx_msg LRESULT OnIconNotify(WPARAM, LPARAM);
	afx_msg LRESULT OnHotKey(WPARAM, LPARAM);
	afx_msg void OnHideWindow();
	DECLARE_MESSAGE_MAP()

private:
	CSockRemote m_sockRemote;
	BOOL IsGroupExists(const char * szGroupName);
	int GetItemType(HTREEITEM hItem = NULL);

public:
	HTREEITEM AddGroup(HTREEITEM hParent, const char * szGroupName);

private:
	HTREEITEM MoveGroup(HTREEITEM hParent, HTREEITEM hItemToMove);
	void DeleteGroupCommand(HTREEITEM hItemGroup);
	void SetGroup(HTREEITEM hItem, const char * szGroupName);

public:
	HTREEITEM AddCommand(HTREEITEM hParent, const CHKCommand& hk);
	void CleanMemory(HTREEITEM hItemParent = TVI_ROOT, BOOL fCleanParent = FALSE);

private:
	HTREEITEM MoveCommand(HTREEITEM hParent, HTREEITEM hItemToMove);
	void DeleteCommand(HTREEITEM hItem);
	void SetCommand(HTREEITEM hItem);//, const CHKCommand& hk);

	static void OpenSpecialFolder(int nSFIndex);
	static void ExecuteDefClient(const char * szClientType, const char * szClientName, BOOL fExecutionMinimized);
	static BOOL ConfirmBox(HWND hWnd, const char * szText);
	static void ExecuteNewMailMessage();
	static void ExecProgram(const char * szProgram, const char * szTitle);
	static BOOL ActionOpenFile(const char * szDescription, const char * szActionString, const char * szProgram, BOOL fExecutionMinimized);
	static BOOL ActionGotoURL(const char * szDescription, const char * szActionString, const char * szURL, BOOL fExecutionMinimized);
	static BOOL ActionGotoURLFavorite(const char * szDescription, const char * szActionString, const char * szURLFavorite, BOOL fExecutionMinimized);
	static BOOL ActionSound(const char * szDescription, const char * szActionString, UINT uSubActionId);
	static BOOL ActionRASDialUp(const char * szDescription, const char * szActionString, const char * szEntry);
	static BOOL ActionICAConnection(const char * szDescription, const char * szActionString, const char * szICAConnection, BOOL fExecutionMinimized);
	static BOOL ActionScreenSaver(const char * szDescription, const char * szActionString, const char * szScreenSaver);
	static BOOL ActionMisc(const char * szDescription, const char * szActionString, UINT uSubActionId, BOOL fExecutionMinimized);
	static BOOL ActionWinAmp(const char * szDescription, const char * szActionString, UINT uSubActionId);
	static BOOL ActionTimer(const char * szDescription, const char * szActionString, DWORD dwTimerValue);
	static BOOL ActionPasteText(CWnd * pWnd, const char * szDescription, const char * szActionString, const char * szPasteText);
	static BOOL ActionSearchEngine(const char * szDescription, const char * szActionString, const char * szSearchEngine);
	static BOOL ActionDisplayMessage(const char * szDescription, const char * szActionString, const char * szDisplayMessage);
	static void ActionMouseMove();
	static void ActionMouseLeftClick();
	static void ActionMouseRightClick();
	static BOOL GotoURL(BOOL fExecutionMinimized);
	BOOL ExecuteCommand(const char * szTriggerAction, CHKCommand& hkcommand);
	void EnableControls();
	BOOL SearchHotkey(HTREEITEM hItem, UINT uHotkeyID);
	BOOL SearchScheduledTask(HTREEITEM hItem, DWORD dwTaskId);

	/*DWORD SearchMaxID(HTREEITEM hItem);
	DWORD GetID();*/

	void RestoreWindow();
	void TrayIconShow(BOOL fShow);

	static void ShowTip(const char * szText);

	static int CALLBACK RemoteSockCallback(int nEvent, LPARAM lParam, LPCSTR pBuffer, int nMemSize);
	static DWORD WINAPI RemoteSockThread(void *);

	//	Drag and drop
	CImageList * m_pDragImage;
	BOOL m_fDragging;
	POINT m_pointHover;
	HTREEITEM m_hItemDrag, m_hItemDrop;
	int m_nTimerTicks;
	UINT m_nDragTimerID, m_nHoverTimerID;
	UINT m_nDateTimeIdleTimerID;
#ifdef _DEBUG
	UINT m_nDebugTimerID;
#endif

	CImageList m_ilHotkeys;

public:
	static BOOL m_fTipDisplayDetails;
	static DWORD m_dwShowTipTime;
	static CWndTip m_wndtip;
	static CWindowNotifier m_wndNotifier;
	static BOOL m_fTipNotifier;
	static CString m_sPathPNICA;

private:
	CTaskWindowList * m_ptaskwindowlist;


	static CLogger * m_logger;
public:
	CScheduler m_scheduler;
	static CRegistryConfig m_registryConfig;
};
