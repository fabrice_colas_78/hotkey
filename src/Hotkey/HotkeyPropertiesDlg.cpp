// HotkeyPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "HotkeyPropertiesDlg.h"

#include "global.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHotkeyPropertiesDlg dialog


CHotkeyPropertiesDlg::CHotkeyPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHotkeyPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHotkeyPropertiesDlg)
	m_sHotkeyUsed = _T("");
	m_fHotkeyUsed = FALSE;
	//}}AFX_DATA_INIT
}


void CHotkeyPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHotkeyPropertiesDlg)
	DDX_Control(pDX, IDC_STATIC_HOTKEY_USED, m_stHotkeyUsed);
	DDX_Control(pDX, IDC_COMBO_MODIFIER, m_cbModifier);
	DDX_Control(pDX, IDC_COMBO_HOTKEY, m_cbHotkey);
	DDX_Text(pDX, IDC_STATIC_HOTKEY_USED, m_sHotkeyUsed);
	DDX_Check(pDX, IDC_CHECK_HOTKEY_USED, m_fHotkeyUsed);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHotkeyPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CHotkeyPropertiesDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_HOTKEY, OnSelchangeComboHotkey)
	ON_CBN_SELCHANGE(IDC_COMBO_MODIFIER, OnSelchangeComboModifier)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHotkeyPropertiesDlg message handlers

BOOL CHotkeyPropertiesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_stHotkeyUsed.SetFontBold(TRUE);
	FillCombos();

	UpdateData(FALSE);
	SetTextHotkeyUsed();
	EnableControls();
#ifndef _DEBUG
	GetDlgItem(IDC_CHECK_HOTKEY_USED)->ShowWindow(SW_HIDE);
#endif
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHotkeyPropertiesDlg::EnableControls()
{
	UpdateData(TRUE);
	BOOL fEnIDOK = TRUE;

#ifndef _DEBUG
	fEnIDOK = !m_fHotkeyUsed;
#endif
	GetDlgItem(IDOK)->EnableWindow(fEnIDOK);
}

void CHotkeyPropertiesDlg::FillCombos()
{
	//	Hotkey
	for(int i=0;; i++)
	{
		if (s_hotkeys[i].szLabel == NULL) break;
		m_cbHotkey.AddString(s_hotkeys[i].szLabel);
		if (s_hotkeys[i].uVk == m_hotkey.m_uVirtualKey)
		{
			m_cbHotkey.SetCurSel(i);	
		}
	}
	if (m_cbHotkey.GetCurSel() == CB_ERR) m_cbHotkey.SetCurSel(0);

	//	Modifier
	for(int i=0;; i++)
	{
		if (s_modifiers[i].szLabel == NULL) break;
		m_cbModifier.AddString(s_modifiers[i].szLabel);
		if (s_modifiers[i].uVk == m_hotkey.m_uVkmodifier)
		{
			m_cbModifier.SetCurSel(i);	
		}
	}
	if (m_cbModifier.GetCurSel() == CB_ERR) m_cbModifier.SetCurSel(0);
}

UINT CHotkeyPropertiesDlg::GetVirtualKey()
{
	ASSERT(m_cbHotkey.GetCurSel()>=0);
	return s_hotkeys[m_cbHotkey.GetCurSel()].uVk;
}

UINT CHotkeyPropertiesDlg::GetKeyModifiers()
{
	ASSERT(m_cbModifier.GetCurSel()>=0);
	return s_modifiers[m_cbModifier.GetCurSel()].uVk;
}

void CHotkeyPropertiesDlg::SetTextHotkeyUsed()
{
	UpdateData(TRUE);
	m_fHotkeyUsed = !CHotkey::IsRegisterableHotkey(*this, GetVirtualKey(), GetKeyModifiers());
	m_sHotkeyUsed = !m_fHotkeyUsed?"Non utilis�e":"D�j� utilis�e";
	m_stHotkeyUsed.SetTextColor(!m_fHotkeyUsed?CStaticEx::DarkGreen:CStaticEx::Red);
	UpdateData(FALSE);
}

void CHotkeyPropertiesDlg::OnSelchangeComboHotkey() 
{
	// TODO: Add your control notification handler code here
	SetTextHotkeyUsed();
	EnableControls();
}

void CHotkeyPropertiesDlg::OnSelchangeComboModifier() 
{
	// TODO: Add your control notification handler code here
	SetTextHotkeyUsed();
	EnableControls();
}

void CHotkeyPropertiesDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	m_hotkey.m_uVirtualKey = GetVirtualKey();
	m_hotkey.m_uVkmodifier = GetKeyModifiers();
	CDialog::OnOK();
}
