#if !defined(AFX_HOTKEYPROPERTIESDLG_H__B6BE6D18_7B82_4DAC_9BF8_590D75F26F20__INCLUDED_)
#define AFX_HOTKEYPROPERTIESDLG_H__B6BE6D18_7B82_4DAC_9BF8_590D75F26F20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HotkeyPropertiesDlg.h : header file
//

#include <staticex.h>
#include "HotkeyDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CHotkeyPropertiesDlg dialog

class CHotkeyPropertiesDlg : public CDialog
{
// Construction
public:
	CHotkey m_hotkey;
	CHotkeyPropertiesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CHotkeyPropertiesDlg)
	enum { IDD = IDD_DIALOG_HOTKEY_PROPERTIES };
	CStaticEx	m_stHotkeyUsed;
	CComboBox	m_cbModifier;
	CComboBox	m_cbHotkey;
	CString	m_sHotkeyUsed;
	BOOL	m_fHotkeyUsed;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHotkeyPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHotkeyPropertiesDlg)
	afx_msg void OnSelchangeComboHotkey();
	afx_msg void OnSelchangeComboModifier();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void SetTextHotkeyUsed();
	UINT GetKeyModifiers();
	UINT GetVirtualKey();
	void FillCombos();
	void EnableControls();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOTKEYPROPERTIESDLG_H__B6BE6D18_7B82_4DAC_9BF8_590D75F26F20__INCLUDED_)
