// InfosDlg.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "hotkeydlg.h"
#include "InfosDlg.h"

#include "Global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInfosDlg2 dialog


CInfosDlg2::CInfosDlg2(CWnd* pParent /*=NULL*/)
: CDialog(CInfosDlg2::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInfosDlg2)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CInfosDlg2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInfosDlg2)
	DDX_Control(pDX, IDC_LIST_SCHEDULERS, m_lvSchedulers);
	DDX_Control(pDX, IDC_LIST_HOKEYS, m_lvHotkeys);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInfosDlg2, CDialog)
	//{{AFX_MSG_MAP(CInfosDlg2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInfosDlg2 message handlers

void CInfosDlg2::OnOK() 
{
	// TODO: Add extra validation here
	
	//CDialog::OnOK();
}

void CInfosDlg2::OnCancel() 
{
	// TODO: Add extra cleanup here
	m_lvSchedulers.RemoveAllSchedul();
	CDialog::OnCancel();
}

BOOL CInfosDlg2::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_lvHotkeys.SetHighlightType(HIGHLIGHT_ALLCOLUMNS);
	m_lvHotkeys.EnableSort(TRUE);
	m_lvHotkeys.InsertColumn(0, "Commande", LVCFMT_LEFT, 150);
	m_lvHotkeys.InsertColumn(1, "Dans", LVCFMT_LEFT, 200);
	m_lvHotkeys.InsertColumn(2, "Hotkey", LVCFMT_LEFT, 150);
	m_lvHotkeys.InsertColumn(3, "Affect�e", LVCFMT_LEFT, 40);
	ExtractHotkeys(TVI_ROOT, "");

	m_lvSchedulers.Init();
	m_lvSchedulers.FillScheduledTasks(*m_pscheduler, *m_ptvCommands, TVI_ROOT, "");
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CInfosDlg2::ExtractHotkeys(HTREEITEM hParent, const CString sParentPath)
{
	LOG1F("--------------------------------------\n");
	LOG1F("ExtractHotkeys '%s'\n", sParentPath);
	CTreeCtrlEx& tv = * m_ptvCommands;
	HTREEITEM hItem = tv.GetChildItem(hParent);
	for(;;)
	{
		if (! hItem) break;
		CString sPath = sParentPath;
		if (sParentPath != "")
		{
			sPath += "\\";
		}
		sPath += tv.GetItemText(hItem);
		LOG1F("sPath='%s'\n", sPath);
		CItemData * pItemData = (CItemData *)tv.GetItemData(hItem);
		if (pItemData->m_nType == CItemData::Command)
		{
			for(int i=0; i<pItemData->m_hkcommand.GetHotkeyCount(); i++)
			{
				CHotkey hotkey = pItemData->m_hkcommand.GetHotkeyAt(i);
				int nItem = m_lvHotkeys.InsertItem(10000, pItemData->m_hkcommand.m_sDescription);
				m_lvHotkeys.SetItemText(nItem, 1, sPath);
				m_lvHotkeys.SetItemText(nItem, 2, hotkey.GetHotkeyString());
#ifdef _DEBUG
				CString sHotkeyId;
				sHotkeyId.Format("%d %c", hotkey.m_uHotkeyID, hotkey.IsRegistered()?'*':' ');
				m_lvHotkeys.SetItemText(nItem, 3, sHotkeyId);
#else
				m_lvHotkeys.SetItemText(nItem, 3, hotkey.IsRegistered()?"*":"");
#endif
			}
		}
		else if (pItemData->m_nType == CItemData::Group)
		{
			ExtractHotkeys(hItem, sPath);
		}
		hItem = tv.GetNextSiblingItem(hItem);
	}
}
