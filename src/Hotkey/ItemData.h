#pragma once

#include "HKCommand.h"

class CItemData
{
public:
	enum {
		Group = 1,
		Command = 2,
		Action = 3,
		Task = 4,
	};
	CItemData(int nType) { m_nType = nType; }
	~CItemData() {};
	int m_nType;
	CHKCommand m_hkcommand;
};


#define INIT_ITEM_DATA(item)	CItemData * pItemData = (CItemData *)m_tvCommands.GetItemData(item)
