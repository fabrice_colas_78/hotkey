// ListBoxWindows.cpp : implementation file
//

#include "stdafx.h"
#include "ListBoxWindows.h"

#include <log.h>
#include <macrosdef.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListBoxWindows

CListBoxWindows::CListBoxWindows()
{
	m_font.CreateFont(CMyFont::ConvertFontSizeToFontHeight(8),0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, FF_DONTCARE, _T("Tahoma"));
}

CListBoxWindows::~CListBoxWindows()
{
}


BEGIN_MESSAGE_MAP(CListBoxWindows, CListBox)
	//{{AFX_MSG_MAP(CListBoxWindows)
	ON_WM_CHAR()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListBoxWindows message handlers

void CListBoxWindows::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	// TODO: Add your code to determine the size of specified item
	lpMeasureItemStruct->itemHeight = GetItemSize().cy;
}

void CListBoxWindows::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your code to draw the specified item
	CRect rc = lpDrawItemStruct->rcItem;
	if (! IsWindowEnabled()) return;
	if (lpDrawItemStruct->itemID != LB_ERR)
	{
		CRect rc(lpDrawItemStruct->rcItem);
		CDC dc;
		dc.Attach(lpDrawItemStruct->hDC);
		if (BitTest(lpDrawItemStruct->itemState, ODS_SELECTED))
		{
			//	Item selected --> Draw focus rectangle
			dc.FillSolidRect(&rc, GetSysColor(COLOR_HIGHLIGHT));
			dc.SetTextColor(GetSysColor(COLOR_HIGHLIGHTTEXT));
		}
		else
		{
			dc.FillSolidRect(&rc, GetSysColor(COLOR_WINDOW));
			dc.SetTextColor(GetSysColor(COLOR_WINDOWTEXT));
		}
		if (BitTest(lpDrawItemStruct->itemState, ODS_FOCUS))
		{
			dc.DrawFocusRect(&rc);
		}
		/*LOGFONT lf;
		CMyFont mf;
		mf.GetLogFont(&lf);
		CFont * pfont = new CFont;
		pfont->CreateFontIndirect(&lf);
		HFONT hf = (HFONT)::SelectObject(dc, mf);
		LOG1F("hf=%d\n", hf);
		*/
		dc.SelectObject(m_font);
		
		//	Determine 1 character size
		CSize sizeChar = dc.GetTextExtent("0", 1);
		rc.InflateRect(-1, -1);
		CItemDatas * id = (CItemDatas *)GetItemData(lpDrawItemStruct->itemID);
		
		//	Draw icon
		CRect rcIcon(0, 0, 32, 32);
		CSize sizeIcon = GetIconSize();
		CDC memdc;
		CBitmap bm;
		memdc.CreateCompatibleDC(&dc);
		bm.CreateCompatibleBitmap(&dc, rcIcon.Width(), rcIcon.Height());
		memdc.SelectObject(bm);
		if (BitTest(lpDrawItemStruct->itemState, ODS_SELECTED))
			memdc.FillSolidRect(&rcIcon, GetSysColor(COLOR_HIGHLIGHT));
		else
			memdc.FillSolidRect(&rcIcon, GetSysColor(COLOR_WINDOW));
		memdc.DrawIcon(0, 0, id->m_hIcon);
		dc.StretchBlt(rc.left, rc.top, GetIconSize().cx, GetIconSize().cy, &memdc, 0, 0, rcIcon.Width(), rcIcon.Height(), SRCCOPY);

		//	Draw text
		rc.left += GetIconSize().cx + 4;
		rc.top += rc.Height()/2 - sizeChar.cy/2;
		dc.DrawText(id->m_sWindowName, -1, &rc, DT_LEFT);

		dc.Detach();
	}
}

int CListBoxWindows::CompareItem(LPCOMPAREITEMSTRUCT lpCompareItemStruct) 
{
	// TODO: Add your code to determine the sorting order of the specified items
	// return -1 = item 1 sorts before item 2
	// return 0 = item 1 and item 2 sort the same
	// return 1 = item 1 sorts after item 2
	CItemDatas * item1 = (CItemDatas *)lpCompareItemStruct->itemData1;
	CItemDatas * item2 = (CItemDatas *)lpCompareItemStruct->itemData2;
	int nCmp = _tcsicmp(item1->m_sClassName, item2->m_sClassName);
	if (nCmp != 0) return nCmp;
	return _tcsicmp(item1->m_sWindowName, item2->m_sWindowName);
}

int CListBoxWindows::AddItem(HWND hWnd, const char *szWindowName, const char * szClassName, HICON hIcon)
{
	CString sText;
#ifdef _DEBUG
	sText.Format("%s {%08lX} [%s]", szWindowName,	hIcon, szClassName);
#else
	sText.Format("%s", szWindowName);
#endif
	CItemDatas * id = new CItemDatas;
	id->m_hWnd        = hWnd;
	id->m_hIcon       = hIcon;
	id->m_sWindowName = sText;
	id->m_sClassName  = szClassName;
	int nIndex = AddString((char *)id);
	//SetItemData(nIndex, (LONG)id);
	return nIndex;
}

void CListBoxWindows::DeleteItem(LPDELETEITEMSTRUCT lpDeleteItemStruct) 
{
	// TODO: Add your specialized code here and/or call the base class
	CItemDatas * id = (CItemDatas *)lpDeleteItemStruct->itemData;
	DestroyIcon(id->m_hIcon);
	delete id;
	CListBox::DeleteItem(lpDeleteItemStruct);
}

CSize CListBoxWindows::GetItemSize()
{
	return CSize(400, GetIconSize().cy + 2);
}

CSize CListBoxWindows::GetIconSize()
{
	return CSize(32, 32);
}

HWND CListBoxWindows::GetWindowHandleCurrent()
{
	LOG1F("CListBoxWindows::GetWindowHandleCurrent\n");
	int nCur = LB_ERR;
	for(int i=0; i<GetCount(); i++)
	{
		if (GetSel(i) > 0)
		{
			LOG1F("nCur=%d\n", i);
			nCur = i;
			break;
		}
	}
	if (nCur == LB_ERR) return NULL;
	CItemDatas * id = (CItemDatas *)GetItemData(nCur);
	return id->m_hWnd;
}

HWND CListBoxWindows::GetWindowHandle(int nIndex)
{
	if (nIndex < 0 || nIndex >= GetCount()) return NULL;
	CItemDatas * id = (CItemDatas *)GetItemData(nIndex);
	return id->m_hWnd;
}

void CListBoxWindows::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	if (nChar == VK_ESCAPE) GetParent()->PostMessage(WM_CLOSE, 0, 0);	
	if (nChar == VK_RETURN) GetParent()->SendMessage(WM_COMMAND, MAKELONG(GetWindowLong(*this, GWL_ID), LBN_DBLCLK), (LPARAM)(HWND)*this);
	CListBox::OnChar(nChar, nRepCnt, nFlags);
}

void CListBoxWindows::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	POINT Pt;
	::GetCursorPos(&Pt);
	CMenu popupmenu;
	popupmenu.CreatePopupMenu();
	
	popupmenu.AppendMenu(MF_STRING, 102, "&R�duire toutes les fen�tres s�lectionn�es");
	popupmenu.AppendMenu(MF_SEPARATOR);
	popupmenu.AppendMenu(MF_STRING, 103, "&Fermer toutes les fen�tres s�lectionn�es");

	popupmenu.TrackPopupMenu(TPM_LEFTALIGN, Pt.x-5, Pt.y-5, GetParent());
	//CListBox::OnRButtonUp(nFlags, point);
}

void CListBoxWindows::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
#ifdef _DEBUG
	BOOL fOutside;
	UINT uItem = ItemFromPoint(point, fOutside);
	if (!fOutside)
	{
		SetCurSel(uItem);
	}
#endif
	CListBox::OnMouseMove(nFlags, point);
}
