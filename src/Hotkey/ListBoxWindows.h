#if !defined(AFX_LISTBOXWINDOWS_H__51C1D079_5F3C_4394_A6D5_0C65AC4F06E0__INCLUDED_)
#define AFX_LISTBOXWINDOWS_H__51C1D079_5F3C_4394_A6D5_0C65AC4F06E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListBoxWindows.h : header file
//

#include <myfont.h>

/////////////////////////////////////////////////////////////////////////////
// CListBoxWindows window

class CListBoxWindows : public CListBox
{
	class CItemDatas
	{
	public:
		HWND    m_hWnd;
		HICON   m_hIcon;
		CString m_sWindowName;
		CString m_sClassName;
	};

// Construction
public:
	CListBoxWindows();
	~CListBoxWindows();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CListBoxWindows)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual void DeleteItem(LPDELETEITEMSTRUCT lpDeleteItemStruct);
	virtual int CompareItem(LPCOMPAREITEMSTRUCT lpCompareItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	HWND GetWindowHandleCurrent();
	HWND GetWindowHandle(int nIndex);
	int AddItem(HWND hWnd, const char * szWindowName, const char * szClassName, HICON hIcon);
	CSize GetItemSize();
	CSize GetIconSize();

	// Generated message map functions
protected:
	//{{AFX_MSG(CListBoxWindows)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CMyFont m_font;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTBOXWINDOWS_H__51C1D079_5F3C_4394_A6D5_0C65AC4F06E0__INCLUDED_)
