// ListSchedul.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "ListSchedul.h"

#include "HotkeyDlg.h"

#include <log.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListSchedul

CListSchedul::CListSchedul()
{
	m_fSortAscending = true;
	m_nSortIndex     = -1;
}

CListSchedul::~CListSchedul()
{
}


BEGIN_MESSAGE_MAP(CListSchedul, CListCtrl)
	//{{AFX_MSG_MAP(CListSchedul)
	//}}AFX_MSG_MAP
	ON_NOTIFY(HDN_ITEMCLICKA, 0, OnHeaderClicked) 
	ON_NOTIFY(HDN_ITEMCLICKW, 0, OnHeaderClicked)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListSchedul message handlers

void CListSchedul::Init()
{
	SetExtendedStyle(GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_FLATSB | LVS_EX_INFOTIP);
	InsertColumn(CI_COMMAND, "Commande", LVCFMT_LEFT, 150);
	InsertColumn(CI_FOLDER, "Dans", LVCFMT_LEFT, 50);
	InsertColumn(CI_NUMEXEC, "Nombre d'ex�cution", LVCFMT_RIGHT, 50);
	InsertColumn(CI_DATE, "Quand", LVCFMT_LEFT, 220);
	InsertColumn(CI_NEXT, "Prochaine", LVCFMT_LEFT, 220);
	InsertColumn(CI_PREVIOUS, "Pr�c�dente", LVCFMT_LEFT, 220);
	m_nSortIndex = 0;
}

int CListSchedul::InsertSchedul(int nIndex, const CString& sCommand, const CString& sFolder, DWORD dwNumExec, const CString& sDate, CDateRef& drNext, CDateRef& drPrevious)
{
	nIndex = InsertItem(nIndex, sCommand);
	SetItemText(nIndex, CI_FOLDER, sFolder);
	CString sNumExec;
	sNumExec.Format("%lu", dwNumExec);
	SetItemText(nIndex, CI_NUMEXEC, sNumExec);
	SetItemText(nIndex, CI_DATE, sDate);
	SetItemText(nIndex, CI_NEXT, 
		drNext.IsUndefined()?"Expir�e":drNext.ToString(CDateRef::WD_DAY_MONTH_YEAR_HH_MI_SS));
	SetItemText(nIndex, CI_PREVIOUS, 
		drPrevious.IsUndefined()?"Jamais lib�r�e":drPrevious.ToString(CDateRef::WD_DAY_MONTH_YEAR_HH_MI_SS));
	SetItemData(nIndex, (LPARAM)(new CLSData(sCommand, sFolder, dwNumExec, sDate, drNext, drPrevious)));
	SortItems(Compare, (m_nSortIndex<<8) | m_fSortAscending);
	return 0;
}

void CListSchedul::FillScheduledTasks(CScheduler& scheduler, CTreeCtrlEx& tv, HTREEITEM hParent, const CString sParentPath)
{
	HTREEITEM hItem = tv.GetChildItem(hParent);
	for(;;)
	{
		if (! hItem) break;
		CString sPath = sParentPath;
		if (sParentPath != "")
		{
			sPath += "\\";
		}
		sPath += tv.GetItemText(hItem);
		CItemData * pItemData = (CItemData *)tv.GetItemData(hItem);
		if (pItemData->m_nType == CItemData::Command)
		{
			CString sScheduler;
			for(int i=0; i<pItemData->m_hkcommand.GetScheduledTaskCount(); i++)
			{
				CScheduledTask scheduled_task = scheduler.GetTaskFromId(pItemData->m_hkcommand.GetScheduledTaskAt(i).GetTaskId());
#ifdef _DEBUG
				sScheduler.Format("%s (id:%d)", scheduled_task.GetScheduledTaskString(), scheduled_task.GetTaskId());
#else
				sScheduler = (const char *)scheduled_task.GetScheduledTaskString();
#endif
				int nItem = InsertSchedul(10000, pItemData->m_hkcommand.m_sDescription, sPath,  pItemData->m_hkcommand.m_dwNumOfExec, sScheduler,
					scheduled_task.m_drNext,
					scheduled_task.m_drPrevious);
			}
		}
		else if (pItemData->m_nType == CItemData::Group)
		{
			FillScheduledTasks(scheduler, tv, hItem, sPath);
		}
		hItem = tv.GetNextSiblingItem(hItem);
	}
}

void CListSchedul::RemoveAllSchedul()
{
	LockWindowUpdate();
	for(int i=GetItemCount()-1; i>=0; i--)
	{
		CLSData * pData = (CLSData *)GetItemData(i);
		if (pData) 
		{
			delete pData;
		}
		DeleteItem(i);
	}
	UnlockWindowUpdate();
}

void CListSchedul::OnHeaderClicked(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LOG1F("OnHeaderClikcked\n");
	HD_NOTIFY *phdn = (HD_NOTIFY *) pNMHDR;
	
	if(phdn->iButton == 0)// Button is the left mouse button 
	{
		// User clicked on header using left mouse button
		if(phdn->iItem == m_nSortIndex)
			m_fSortAscending = !m_fSortAscending;
		else
			m_fSortAscending = TRUE;

		m_nSortIndex = phdn->iItem;
		//LOG1F("SortIndex:%d\n", m_nSortIndex);
		SortItems(Compare, (m_nSortIndex<<8) | m_fSortAscending);
	}
	*pResult = 0;
}

int CALLBACK CListSchedul::Compare(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	int nSortIndex = (int)(lParamSort>>8);
	BOOL fSortAscending = lParamSort&1;
	//LOG1F("CListSchedul::Compare Index:%d SortAscending:%d\n", nSortIndex, fSortAscending);
	CLSData * pData1 = (CLSData *)lParam1;
	CLSData * pData2 = (CLSData *)lParam2;
	switch(nSortIndex)
	{
	case CI_COMMAND:
		return fSortAscending?_tcsicmp(pData1->m_sCommand, pData2->m_sCommand):_tcsicmp(pData2->m_sCommand, pData1->m_sCommand);
	case CI_FOLDER:
		return fSortAscending?_tcsicmp(pData1->m_sFolder, pData2->m_sFolder):_tcsicmp(pData2->m_sFolder, pData1->m_sFolder);
	case CI_NUMEXEC:
		return fSortAscending?(pData1->m_dwNumExec-pData2->m_dwNumExec):(pData2->m_dwNumExec-pData1->m_dwNumExec);
	case CI_DATE:
		return fSortAscending?_tcsicmp(pData1->m_sDate, pData2->m_sDate):_tcsicmp(pData2->m_sDate, pData1->m_sDate);
	case CI_NEXT:
		return fSortAscending?(pData1->m_drNext-pData2->m_drNext):(pData2->m_drNext-pData1->m_drNext);
	case CI_PREVIOUS:
		return fSortAscending?(pData1->m_drPrevious-pData2->m_drPrevious):(pData2->m_drPrevious-pData1->m_drPrevious);
	}
	return 0;
}
