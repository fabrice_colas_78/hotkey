#if !defined(AFX_LISTSCHEDUL_H__B61A27B3_A5AF_44F2_B857_F3A46EE8B7E7__INCLUDED_)
#define AFX_LISTSCHEDUL_H__B61A27B3_A5AF_44F2_B857_F3A46EE8B7E7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListSchedul.h : header file
//

//#include <ListCtrlEx.h>
#include <TreeCtrlEx.h>

#include "ScheduledTask.h"

/////////////////////////////////////////////////////////////////////////////
// CListSchedul window

class CListSchedul : public CListCtrl
{
	class CLSData
	{
	public:
		CLSData(const char * szCommand, const char * szFolder, DWORD dwNumExec, const char * szData, CDateRef& drNext, CDateRef& drPrevious)
		{
			m_sCommand = szCommand;
			m_sFolder  = szFolder;
			m_dwNumExec = dwNumExec; 
			m_sDate    = szData;
			m_drNext   = drNext;
			m_drPrevious = drPrevious;
		}
		CString m_sCommand, m_sFolder, m_sDate;
		CDateRef m_drNext, m_drPrevious;
		DWORD m_dwNumExec;
	};

	enum {
		CI_COMMAND = 0,
		CI_FOLDER,
		CI_NUMEXEC,
		CI_DATE, 
		CI_NEXT,
		CI_PREVIOUS,
	};
// Construction
public:
	CListSchedul();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CListSchedul)
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	void RemoveAllSchedul();
	int InsertSchedul(int nIndex, const CString& sCommand, const CString& sFolder, DWORD dwNumExec, const CString& sDate, CDateRef& drNext, CDateRef& drPrevious);
	void FillScheduledTasks(CScheduler& scheduler, CTreeCtrlEx& tv, HTREEITEM hParent, const CString sParentPath);
	void Init();
	virtual ~CListSchedul();
	static int CALLBACK Compare(LPARAM lParam1, LPARAM lParam2, LPARAM lParSortAsc);

	// Generated message map functions
protected:
	//{{AFX_MSG(CListSchedul)
	//}}AFX_MSG
	afx_msg void OnHeaderClicked(NMHDR* pNMHDR, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()
private:
	int m_nSortIndex;
	BOOL m_fSortAscending;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTSCHEDUL_H__B61A27B3_A5AF_44F2_B857_F3A46EE8B7E7__INCLUDED_)
