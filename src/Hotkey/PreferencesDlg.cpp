// PreferencesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "PreferencesDlg.h"

#include <myfiledialog.h>

#include "HotkeyPropertiesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


struct tt_tip_timer
{
	const char * text;
	DWORD value;
} g_tip_timer[] = {
	{"0,5 s", 500},
	{"1 s", 1000},
	{"1,5 s", 1500},
	{"2 s", 2000},
	{"2,5 s", 2500},
	{"3 s", 3000},
	{"3,5 s", 3500},
	{"4 s", 4000},
	{"4,5 s", 4500},
	{"5 s", 5000},
};


#define DISPLAY_FONT_NAME()	m_sTipFont.Format("%s, %s%s%s%s%d points", \
	m_wndtipSample.m_wndtipfont.m_font.GetFontFaceName(), \
	m_wndtipSample.m_wndtipfont.m_font.GetFontWeight() >= FW_BOLD?"gras, ":"", \
	m_wndtipSample.m_wndtipfont.m_font.GetFontItalic()?"italique, ":"", \
	m_wndtipSample.m_wndtipfont.m_font.GetFontStrikeOut()?"barr�, ":"",  \
	m_wndtipSample.m_wndtipfont.m_font.GetFontUnderline()?"soulign�, ":"", \
	CMyFont::ConvertFontHeightToFontSize(m_wndtipSample.m_wndtipfont.m_font.GetFontHeight()));

/////////////////////////////////////////////////////////////////////////////
// CPreferencesDlg dialog


CPreferencesDlg::CPreferencesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPreferencesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPreferencesDlg)
	m_fTipDisplayDetails = FALSE;
	m_sTipFont = _T("");
	m_fTaskSelector = FALSE;
	m_sTaskSelectorHotkey = _T("");
	m_sPathPNICA = _T("");
	//}}AFX_DATA_INIT
	m_dwTipTimer = 1500;
	m_fNotifier = false;
}


void CPreferencesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPreferencesDlg)
	DDX_Control(pDX, IDC_COMBO_TIP_TIMER, m_cbTipTimer);
	DDX_Control(pDX, IDC_COMBO_TIP_TEXT_COLOR, m_cbTipTextColor);
	DDX_Control(pDX, IDC_COMBO_TIP_BKTEXT_COLOR, m_cbTipBkTextColor);
	DDX_Check(pDX, IDC_CHECK_TIP_DISPLAY_DETAILS, m_fTipDisplayDetails);
	DDX_Text(pDX, IDC_STATIC_TIP_FONT, m_sTipFont);
	DDX_Check(pDX, IDC_CHECK_TASK_SELECTOR, m_fTaskSelector);
	DDX_Text(pDX, IDC_STATIC_TASK_SELECTOR_HOTKEY, m_sTaskSelectorHotkey);
	DDX_Text(pDX, IDC_EDIT_PATH_PN_EXE, m_sPathPNICA);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPreferencesDlg, CDialog)
	//{{AFX_MSG_MAP(CPreferencesDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_TIP_BKTEXT_COLOR, OnSelchangeComboTipBktextColor)
	ON_CBN_SELCHANGE(IDC_COMBO_TIP_TEXT_COLOR, OnSelchangeComboTipTextColor)
	ON_BN_CLICKED(IDC_BUTTON_CHOOSE_FONT, OnButtonChooseFont)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_BOTTOM_LEFT, OnRadioTipPosBottomLeft)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_BOTTOM_MID, OnRadioTipPosBottomMid)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_BOTTOM_RIGHT, OnRadioTipPosBottomRight)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_MID_LEFT, OnRadioTipPosMidLeft)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_MID_MID, OnRadioTipPosMidMid)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_MID_RIGHT, OnRadioTipPosMidRight)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_TOP_LEFT, OnRadioTipPosTopLeft)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_TOP_MID, OnRadioTipPosTopMid)
	ON_BN_CLICKED(IDC_RADIO_TIP_POS_TOP_RIGHT, OnRadioTipPosTopRight)
	ON_BN_CLICKED(IDC_BUTTON_CHOOSE_TASK_SELECTOR_HOTKEY, OnButtonChooseTaskSelectorHotkey)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_PN_EXE, OnButtonBrowsePnExe)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPreferencesDlg message handlers

BOOL CPreferencesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_cbTipTextColor.EnableAutomatic(GetSysColor(COLOR_INFOTEXT));
	m_cbTipBkTextColor.EnableAutomatic(GetSysColor(COLOR_INFOBK));
	m_cbTipTextColor.SetCurSelColor(m_wndtipSample.m_wndtipcolor.GetTextColor());
	m_cbTipBkTextColor.SetCurSelColor(m_wndtipSample.m_wndtipcolor.GetBkTextColor());

	CheckDlgButton(IDC_RADIO_BULLTIP, !m_fNotifier);
	CheckDlgButton(IDC_RADIO_NOTIFIER, m_fNotifier);

	for(int i=0; i<sizeof(g_tip_timer)/sizeof(g_tip_timer[0]); i++)
	{
		m_cbTipTimer.AddString(g_tip_timer[i].text);
		if (m_dwTipTimer == g_tip_timer[i].value)
		{
			m_cbTipTimer.SetCurSel(i);
		}
	}
	if (m_cbTipTimer.GetCurSel() == CB_ERR) m_cbTipTimer.SetCurSel(2);
	
	DISPLAY_FONT_NAME();

	SetAutomaticPos(m_wndtipSample.GetAutomaticPos());

	UpdateData(FALSE);
	m_wndtipSample.Create();
	m_wndtipSample.SetText("Exemple commande");
	m_wndtipSample.Show(TRUE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPreferencesDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(true);
	m_dwTipTimer = g_tip_timer[m_cbTipTimer.GetCurSel()].value;
	m_fNotifier  = IsDlgButtonChecked(IDC_RADIO_NOTIFIER);
	CDialog::OnOK();
}

void CPreferencesDlg::OnSelchangeComboTipBktextColor() 
{
	// TODO: Add your control notification handler code here
	m_wndtipSample.m_wndtipcolor.SetBkTextColor(m_cbTipBkTextColor.GetCurSelColor());
	m_wndtipSample.RedrawWindow();
}

void CPreferencesDlg::OnSelchangeComboTipTextColor() 
{
	// TODO: Add your control notification handler code here
	m_wndtipSample.m_wndtipcolor.SetTextColor(m_cbTipTextColor.GetCurSelColor());
	m_wndtipSample.RedrawWindow();
}

void CPreferencesDlg::OnButtonChooseFont() 
{
	// TODO: Add your control notification handler code here
	LOGFONT lf;
	m_wndtipSample.m_wndtipfont.m_font.GetLogFont(&lf);
	CFontDialog Dlg(&lf, CF_EFFECTS | CF_SCREENFONTS, NULL, this);

	if (Dlg.DoModal() == IDOK)
	{
		m_wndtipSample.m_wndtipfont.m_font = lf;
		m_wndtipSample.RedrawWindow();

		UpdateData(TRUE);
		DISPLAY_FONT_NAME();
		UpdateData(FALSE);
	}
}

void CPreferencesDlg::OnRadioTipPosBottomLeft() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnRadioTipPosBottomMid() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnRadioTipPosBottomRight() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnRadioTipPosMidLeft() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnRadioTipPosMidMid() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnRadioTipPosMidRight() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnRadioTipPosTopLeft() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnRadioTipPosTopMid() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnRadioTipPosTopRight() 
{
	// TODO: Add your control notification handler code here
	OnTipPosChanged();
}

void CPreferencesDlg::OnTipPosChanged()
{
	m_wndtipSample.SetAutomaticPos(GetAutomaticPos());
}

struct tt_pos
{
	int nId;
	int nVal;
} tippos[] = {
	{ IDC_RADIO_TIP_POS_TOP_LEFT, CWndTip::AutoPosTopLeft},
	{ IDC_RADIO_TIP_POS_TOP_MID, CWndTip::AutoPosTopMid},
	{ IDC_RADIO_TIP_POS_TOP_RIGHT, CWndTip::AutoPosTopRight},
	{ IDC_RADIO_TIP_POS_MID_LEFT, CWndTip::AutoPosMidLeft},
	{ IDC_RADIO_TIP_POS_MID_MID, CWndTip::AutoPosMidMid},
	{ IDC_RADIO_TIP_POS_MID_RIGHT, CWndTip::AutoPosMidRight},
	{ IDC_RADIO_TIP_POS_BOTTOM_LEFT, CWndTip::AutoPosBottomLeft},
	{ IDC_RADIO_TIP_POS_BOTTOM_MID, CWndTip::AutoPosBottomMid},
	{ IDC_RADIO_TIP_POS_BOTTOM_RIGHT, CWndTip::AutoPosBottomRight},
	};

int CPreferencesDlg::GetAutomaticPos()
{

	for(int i=0; i<sizeof(tippos)/sizeof(tippos[0]); i++)
	{
		if (IsDlgButtonChecked(tippos[i].nId)) return tippos[i].nVal;
	}
	return tippos[0].nVal;
}

void CPreferencesDlg::SetAutomaticPos(int nPos)
{
	for(int i=0; i<sizeof(tippos)/sizeof(tippos[0]); i++)
	{
		if (nPos == tippos[i].nVal) 
		{
			CheckDlgButton(tippos[i].nId, 1);
			return;
		}
	}
	ASSERT(FALSE);
	CheckDlgButton(tippos[0].nId, 1);
}

void CPreferencesDlg::OnButtonChooseTaskSelectorHotkey() 
{
	// TODO: Add your control notification handler code here
	CHotkeyPropertiesDlg Dlg;
	
	/*CHotkey * pHotkey = LV_HOTKEY_GET_HOTKEY_PTR(nItemSel);
	Dlg.m_hotkey = *pHotkey;

	if (Dlg.DoModal() == IDOK)
	{
		*pHotkey = Dlg.m_hotkey;
		//LV_HOTKEY_SET_ITEM(nItemSel,pHotkey);
	}*/
}

void CPreferencesDlg::OnButtonBrowsePnExe() 
{
	// TODO: Add your control notification handler code here
	CMyFileDialog Dlg(this, TRUE, "Tous les executables|*.exe|Tous les fichiers|*.*||", "");
	
	if (Dlg.DoModal() == IDOK)
	{
		UpdateData(TRUE);
		m_sPathPNICA = Dlg.m_ofn.lpstrFile;
		UpdateData(FALSE);
		//EnableControls();
	}
}
