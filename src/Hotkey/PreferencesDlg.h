#if !defined(AFX_PREFERENCESDLG_H__7AFE6B56_B009_4754_8721_2730EEE47E02__INCLUDED_)
#define AFX_PREFERENCESDLG_H__7AFE6B56_B009_4754_8721_2730EEE47E02__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreferencesDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPreferencesDlg dialog

#include <ComboBoxColor.h>
#include <wndtip.h>

class CPreferencesDlg : public CDialog
{
// Construction
public:
	void SetAutomaticPos(int nPos);
	int GetAutomaticPos();
	CWndTip m_wndtipSample;
	DWORD m_dwTipTimer;
	BOOL m_fNotifier;
	CPreferencesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPreferencesDlg)
	enum { IDD = IDD_DIALOG_PREFERENCES };
	CComboBox	m_cbTipTimer;
	CComboBoxColor	m_cbTipTextColor;
	CComboBoxColor	m_cbTipBkTextColor;
	BOOL	m_fTipDisplayDetails;
	CString	m_sTipFont;
	BOOL	m_fTaskSelector;
	CString	m_sTaskSelectorHotkey;
	CString	m_sPathPNICA;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreferencesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPreferencesDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeComboTipBktextColor();
	afx_msg void OnSelchangeComboTipTextColor();
	afx_msg void OnButtonChooseFont();
	afx_msg void OnRadioTipPosBottomLeft();
	afx_msg void OnRadioTipPosBottomMid();
	afx_msg void OnRadioTipPosBottomRight();
	afx_msg void OnRadioTipPosMidLeft();
	afx_msg void OnRadioTipPosMidMid();
	afx_msg void OnRadioTipPosMidRight();
	afx_msg void OnRadioTipPosTopLeft();
	afx_msg void OnRadioTipPosTopMid();
	afx_msg void OnRadioTipPosTopRight();
	afx_msg void OnButtonChooseTaskSelectorHotkey();
	afx_msg void OnButtonBrowsePnExe();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void OnTipPosChanged();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREFERENCESDLG_H__7AFE6B56_B009_4754_8721_2730EEE47E02__INCLUDED_)
