#include "stdafx.h"
#include "resource.h"

#include "RegistryConfig.h"

#include "HotkeyDlg.h"


#ifdef _DEBUG
#define STR_REG_ROOT				"Hotkey Manager(D)"
#else
#define STR_REG_ROOT				"Hotkey Manager"
#endif

#define STR_REG_GOTOURLLIST			STR_REG_ROOT"\\GotoURLList"
#define STR_REG_QUERYSTRING			STR_REG_ROOT"\\QueryStringList"

#define STR_REG_HK_TYPE				"Type"

#define STR_REG_HK_GROUP_NAME		"Group"

#define STR_REG_HK_DESCRIPTION		"Description"
#define STR_REG_HK_VIRTUALKEY		"VirtualKey"
#define STR_REG_HK_MODIFIER			"Modifier"
#define STR_REG_HK_ACTIVATE			"Active"
#define STR_REG_HK_EXECUTION_MINIMIZED	"ExecutionMinimized"
#define STR_REG_HK_ID				"ID"

#define STR_REG_HK_PROGRAM			"Program"
#define STR_REG_HK_URL				"URL"
#define STR_REG_HK_URL_FAVORITE		"URLFavorite"
#define STR_REG_HK_RAS_ENTRY		"RASEntry"
#define STR_REG_HK_ICA_CONNECTION	"ICAConnection"
#define STR_REG_HK_SCREEN_SAVER		"ScreenSaver"
#define STR_REG_HK_ACTION_ID		"ActionType"
#define STR_REG_HK_SUB_ACTION_ID	"SubActionType"
#define STR_REG_HK_TIMER_VALUE		"TimerValue"
#define STR_REG_HK_PASTE_TEXT		"PasteText"
#define STR_REG_HK_SEARCH_ENGINE	"SearchEngine"
#define STR_REG_HK_DISPLAY_MESSAGE	"DisplayMessage"
#define STR_REG_HK_DESACTIVATED		"Desactivated"

#define STR_REG_SCHEDUL_TASK_TYPE			"SchedTaskType"
#define STR_REG_SCHEDUL_SECOND				"SchedSecond"
#define STR_REG_SCHEDUL_MINUTE				"SchedMinute"
#define STR_REG_SCHEDUL_HOUR				"SchedHour"
#define STR_REG_SCHEDUL_DAY_OF_MONTH		"SchedDayOfMonth"
#define STR_REG_SCHEDUL_MONTH				"SchedMonth"
#define STR_REG_SCHEDUL_YEAR				"SchedYear"
#define STR_REG_SCHEDUL_DAY_OF_WEEK_MASK	"SchedDayOfWeekMask"
#define STR_REG_SCHEDUL_DAY_OF_MONTH_MASK	"SchedDayOfMonthMask"
#define STR_REG_SCHEDUL_MONTHS_MASK			"SchedMonthsMask"
#define STR_REG_SCHEDUL_TASK_IDLE			"SchedTaskIdle"
#define STR_REG_SCHEDUL_SECOND_IDLE			"SchedSecondIdle"
#define STR_REG_SCHEDUL_MINUTE_IDLE			"SchedMinuteIdle"
#define STR_REG_SCHEDUL_HOUR_IDLE			"SchedHourIdle"
#define STR_REG_SCHEDUL_REPETITION			"SchedRepetition"


#define STR_GROUP					"Group"
#define STR_GROUP_FORMAT			STR_GROUP"%03d"
#define STR_COMMAND					"Command"
#define STR_COMMAND_FORMAT			STR_COMMAND"%03d"
#define STR_ACTION					"Action"
#define STR_ACTION_FORMAT			STR_ACTION"%03d"
#define STR_HOTKEY					"Hotkey"
#define STR_HOTKEY_FORMAT			STR_HOTKEY"%03d"
#define STR_SCHEDUL					"ScheduledTask"
#define STR_SCHEDUL_FORMAT			STR_SCHEDUL"%03d"




#define MY_LOGGER_NAME	LOGGER_HOTKEY_REGISTRY_CONFIG_CLASS

#include <LoggerMacros.h>
INITLogger(CRegistryConfig::m_logger);

CRegistryConfig::CRegistryConfig()
{
	//m_fContinue = true;
}

void CRegistryConfig::Init()
{
	if (m_logger == NULL)
	{
		//	Get logger only the first time
		m_logger = g_logFactory.GetLoggerPtr(MY_LOGGER_NAME);
	}
}

void CRegistryConfig::SaveScheduledTask(CRegistry& reg, const CScheduledTask& scheduled_task)
{
	reg.SetValue(STR_REG_SCHEDUL_TASK_TYPE, scheduled_task.GetTaskType());
	reg.SetValue(STR_REG_SCHEDUL_SECOND, scheduled_task.GetSecond());
	reg.SetValue(STR_REG_SCHEDUL_MINUTE, scheduled_task.GetMinute());
	reg.SetValue(STR_REG_SCHEDUL_HOUR, scheduled_task.GetHour());
	reg.SetValue(STR_REG_SCHEDUL_DAY_OF_MONTH, scheduled_task.GetDayOfMonth());
	reg.SetValue(STR_REG_SCHEDUL_MONTH, scheduled_task.GetMonth());
	reg.SetValue(STR_REG_SCHEDUL_YEAR, scheduled_task.GetYear());
	reg.SetValue(STR_REG_SCHEDUL_DAY_OF_WEEK_MASK, scheduled_task.GetDaysOfWeekMask());
	reg.SetValue(STR_REG_SCHEDUL_DAY_OF_MONTH_MASK, scheduled_task.GetDaysOfMonthMask());
	reg.SetValue(STR_REG_SCHEDUL_MONTHS_MASK, scheduled_task.GetMonthsMask());
	reg.SetValue(STR_REG_SCHEDUL_TASK_IDLE, scheduled_task.IsIdle());
	reg.SetValue(STR_REG_SCHEDUL_SECOND_IDLE, scheduled_task.GetSecondIdle());
	reg.SetValue(STR_REG_SCHEDUL_MINUTE_IDLE, scheduled_task.GetMinuteIdle());
	reg.SetValue(STR_REG_SCHEDUL_HOUR_IDLE, scheduled_task.GetHourIdle());
	reg.SetValue(STR_REG_SCHEDUL_REPETITION, scheduled_task.GetRepetition());
}

void CRegistryConfig::SaveHotkey(CRegistry& reg, const CHotkey& hotkey)
{
	reg.SetValue(STR_REG_HK_VIRTUALKEY, (DWORD)hotkey.m_uVirtualKey);
	reg.SetValue(STR_REG_HK_MODIFIER, (DWORD)hotkey.m_uVkmodifier);
}

void CRegistryConfig::SaveAction(CRegistry& reg, const CAction& action)
{
	reg.SetValue(STR_REG_HK_DESACTIVATED, action.m_fDesactivated);
	reg.SetValue(STR_REG_HK_TYPE, CItemData::Action);
	reg.SetValue(STR_REG_HK_PROGRAM, action.m_sProgram);
	reg.SetValue(STR_REG_HK_URL, action.m_sURL);
	reg.SetValue(STR_REG_HK_URL_FAVORITE, action.m_sURLFavorite);
	reg.SetValue(STR_REG_HK_RAS_ENTRY, action.m_sRASEntry);
	reg.SetValue(STR_REG_HK_ICA_CONNECTION, action.m_sICAConnection);
	reg.SetValue(STR_REG_HK_SCREEN_SAVER, action.m_sScreenSaver);
	reg.SetValue(STR_REG_HK_ACTION_ID, (DWORD)action.m_uActionId);
	reg.SetValue(STR_REG_HK_SUB_ACTION_ID, (DWORD)action.m_uSubActionId);
	reg.SetValue(STR_REG_HK_TIMER_VALUE, action.m_dwTimerValue);
	reg.SetValue(STR_REG_HK_PASTE_TEXT, action.m_sPasteText);
	reg.SetValue(STR_REG_HK_SEARCH_ENGINE, action.m_sSearchEngine);
	reg.SetValue(STR_REG_HK_DISPLAY_MESSAGE, action.m_sDisplayMessage);
}

void CRegistryConfig::SaveCommand(CRegistry& reg, HTREEITEM hItem, CHotkeyDlg& dlg)
{
	CTreeCtrlEx& m_tvCommands = dlg.m_tvCommands;
	INIT_ITEM_DATA(hItem);
	LOGInfof("    Saving command (in %s) %s ...", reg.GetSubKeyName(), pItemData->m_hkcommand.m_sDescription);
	reg.SetValue(STR_REG_HK_TYPE, CItemData::Command);
	reg.SetValue(STR_REG_HK_DESCRIPTION, pItemData->m_hkcommand.m_sDescription);
	reg.SetValue(STR_REG_HK_ACTIVATE, pItemData->m_hkcommand.m_fActive);
	reg.SetValue(STR_REG_HK_EXECUTION_MINIMIZED, pItemData->m_hkcommand.m_fExecutionMinimized);
	reg.SetValue(STR_REG_HK_ID, pItemData->m_hkcommand.m_dwId);

	CString sIndex;

	int nActionIndex = 0;
	for(int i=0; i<pItemData->m_hkcommand.GetActionCount(); i++)
	{
		sIndex.Format(STR_ACTION_FORMAT, nActionIndex++);
		CRegistry regAction(reg, sIndex);
		regAction.Open(true);
		PurgeRegEntry(regAction);
		SaveAction(regAction, pItemData->m_hkcommand.GetActionAt(i));
	}
	int nHotkeyIndex = 0;
	for(int i=0; i<pItemData->m_hkcommand.GetHotkeyCount(); i++)
	{
		sIndex.Format(STR_HOTKEY_FORMAT, nHotkeyIndex++);
		CRegistry regHotkey(reg, sIndex);
		regHotkey.Open(true);
		PurgeRegEntry(regHotkey);
		SaveHotkey(regHotkey, pItemData->m_hkcommand.GetHotkeyAt(i));
	}
	int nSchedulIndex = 0;
	for(int i=0; i<pItemData->m_hkcommand.GetScheduledTaskCount(); i++)
	{
		sIndex.Format(STR_SCHEDUL_FORMAT, nSchedulIndex++);
		CRegistry regSchedul(reg, sIndex);
		regSchedul.Open(true);
		PurgeRegEntry(regSchedul);
		SaveScheduledTask(regSchedul, pItemData->m_hkcommand.GetScheduledTaskAt(i));
	}
}

void CRegistryConfig::PurgeRegEntry(CRegistry& reg)
{
	reg.DeleteAllValues();
	reg.DeleteSubKeys();

	int nCountKeys = reg.CountKeys();
	int nCountValues = reg.CountValues();
	LOGTracef("%s CountKeys:%d, CountValues:%d", reg.GetSubKeyName(), nCountKeys, nCountValues);
	if (nCountKeys>0 || nCountValues>0)
	{
		LOGErrorf("%s CountSubkeys>0(%d) || CountValues>0(%d)", reg.GetSubKeyName(), nCountKeys, nCountValues);
	}
}

int CRegistryConfig::SaveGroup(CRegistry& reg, HTREEITEM hItem, CHotkeyDlg& dlg)
{
	LOGInfof("  Saving group (in %s) %s...", reg.GetSubKeyName(), dlg.m_tvCommands.GetItemText(hItem));

	reg.SetValue(STR_REG_HK_TYPE, CItemData::Group);
	reg.SetValue(STR_REG_HK_GROUP_NAME, dlg.m_tvCommands.GetItemText(hItem));


	int nGrpIndex = 0,
		nCmdIndex = 0;
	CString sIndex;
	CTreeCtrlEx& m_tvCommands = dlg.m_tvCommands;
	hItem = m_tvCommands.GetChildItem(hItem);
	for(;;)
	{
		if (! hItem) break;
		INIT_ITEM_DATA(hItem);
		if (pItemData->m_nType == CItemData::Group)
		{
			sIndex.Format(STR_GROUP_FORMAT, nGrpIndex++);
			CRegistry regGrp(reg, sIndex);
			LOGTracef("    Opening registry key: %s...", sIndex);
			if (! regGrp.Open(true))
			{
				LOGErrorf("unable to open registry key: %s (%s)", sIndex, regGrp.GetLastErrorString());
				continue;
			}
			PurgeRegEntry(regGrp);
			SaveGroup(regGrp, hItem, dlg);
		}
		else if (pItemData->m_nType == CItemData::Command)
		{
			sIndex.Format(STR_COMMAND_FORMAT, nCmdIndex++);
			CRegistry regCmd(reg, sIndex);
			LOGTracef("    Opening registry key: %s...", sIndex);
			if (! regCmd.Open(true))
			{
				LOGErrorf("unable to open registry key: %s (%s)", sIndex, regCmd.GetLastErrorString());
				continue;
			}
			PurgeRegEntry(regCmd);
			/*
			if (m_fContinue && (MessageBox(NULL, "Continue ?", "Confirmation", MB_YESNO) == IDNO))
			{
				m_fContinue = false;
			}
			*/
			SaveCommand(regCmd, hItem, dlg);
		}
		hItem = m_tvCommands.GetNextSiblingItem(hItem);
	}
	return nCmdIndex;
}

void CRegistryConfig::SaveHotkeysConfig(CHotkeyDlg& dlg)
{
#ifdef _DEBUG
	//return;
#endif
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_ROOT "\\Hotkeys";
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);

	if (! reg.Open(true)) return;
	LOGInfof("Deleting all subkeys in %s...", sRegkey);
	reg.DeleteSubKeys();
	LOGInfof("All subkeys deleted in %s.", sRegkey);

	LOGInfof("Checking for subkeys in %s...", sRegkey);
	int keysCount = reg.CountKeys();
	if (keysCount > 0)
		LOGErrorf("%d subkeys left.", keysCount);
	else
		LOGInfof("All subkeys have been deleted.");
	
	//MessageBox(NULL, "Attente insertion dans registre", "HotKey DEBUG", 0);
	
	int nGrpIndex = 0;
	CString sIndex;
	HTREEITEM hItem = dlg.m_tvCommands.GetChildItem(TVI_ROOT);
	
	for(;;)
	{
		if (! hItem) break;
		sIndex.Format(STR_GROUP_FORMAT, nGrpIndex++);
		CRegistry regGrp(reg, sIndex);
		LOGTracef("  Opening registry key: %s...", sIndex);
		if (! regGrp.Open(true))
		{
			LOGErrorf("unable to open registry key: %s (%s)", sIndex, regGrp.GetLastErrorString());
			continue;
		}
		PurgeRegEntry(regGrp);
		SaveGroup(regGrp, hItem, dlg);

		hItem = dlg.m_tvCommands.GetNextSiblingItem(hItem);
	}
}


BOOL CRegistryConfig::RestoreScheduledTask(CRegistry& reg, CScheduledTask& scheduled_task)
{
	int nTaskType;
	WORD wYear, wMonth, wDayOfMonth, wHour, wMinute, wSecond;
	WORD wHourIdle = 0, wMinuteIdle = 30, wSecondIdle = 0;
	WORD wRepetition = 10;
	WORD wDaysOfWeekMask;
	DWORD dwDaysOfMonthMask;
	WORD wMonthsMask;
	BOOL fIdle = FALSE;
	if (! reg.QueryValue(STR_REG_SCHEDUL_TASK_TYPE, &nTaskType) ||
		! reg.QueryValue(STR_REG_SCHEDUL_SECOND, &wSecond) ||
		! reg.QueryValue(STR_REG_SCHEDUL_MINUTE, &wMinute) ||
		! reg.QueryValue(STR_REG_SCHEDUL_HOUR, &wHour) ||
		! reg.QueryValue(STR_REG_SCHEDUL_DAY_OF_MONTH, &wDayOfMonth) ||
		! reg.QueryValue(STR_REG_SCHEDUL_MONTH, &wMonth) ||
		! reg.QueryValue(STR_REG_SCHEDUL_YEAR, &wYear) ||
		! reg.QueryValue(STR_REG_SCHEDUL_DAY_OF_WEEK_MASK, &wDaysOfWeekMask) ||
		! reg.QueryValue(STR_REG_SCHEDUL_DAY_OF_MONTH_MASK, &dwDaysOfMonthMask) ||
		! reg.QueryValue(STR_REG_SCHEDUL_MONTHS_MASK, &wMonthsMask)) 
	{
		return FALSE;
	}
	if (! reg.QueryValue(STR_REG_SCHEDUL_TASK_IDLE, &fIdle) ||
		! reg.QueryValue(STR_REG_SCHEDUL_SECOND_IDLE, &wSecondIdle) ||
		! reg.QueryValue(STR_REG_SCHEDUL_MINUTE_IDLE, &wMinuteIdle) ||
		! reg.QueryValue(STR_REG_SCHEDUL_HOUR_IDLE, &wHourIdle))
	{}
	if (! reg.QueryValue(STR_REG_SCHEDUL_REPETITION, &wRepetition))
	{}

	scheduled_task.SetTaskType(nTaskType);
	scheduled_task.InitTimeAndDate(wYear, wMonth, wDayOfMonth, wHour, wMinute, wSecond);
	scheduled_task.InitIdle(fIdle, wHourIdle, wMinuteIdle, wSecondIdle);
	scheduled_task.InitRepetition(wRepetition);
	scheduled_task.SetDaysOfWeekMask(wDaysOfWeekMask);
	scheduled_task.SetDaysOfMonthMask(dwDaysOfMonthMask);
	scheduled_task.SetMonthsMask(wMonthsMask);
	return true;
}
BOOL CRegistryConfig::RestoreHotkey(CRegistry& reg, CHotkey& hotkey)
{
	if (! reg.QueryValue(STR_REG_HK_VIRTUALKEY, (DWORD *)&hotkey.m_uVirtualKey) || 
		! reg.QueryValue(STR_REG_HK_MODIFIER, (DWORD *)&hotkey.m_uVkmodifier)) return FALSE;
	return true;
}

BOOL CRegistryConfig::RestoreAction(CRegistry& reg, CAction& action)
{
	reg.QueryValue(STR_REG_HK_DESACTIVATED, &action.m_fDesactivated);
	reg.QueryValue(STR_REG_HK_PROGRAM, action.m_sProgram.GetBuffer(300), 300);
	reg.QueryValue(STR_REG_HK_URL, action.m_sURL.GetBuffer(1000), 1000);
	reg.QueryValue(STR_REG_HK_URL_FAVORITE, action.m_sURLFavorite.GetBuffer(1000), 1000);
	reg.QueryValue(STR_REG_HK_RAS_ENTRY, action.m_sRASEntry.GetBuffer(300), 300);
	reg.QueryValue(STR_REG_HK_ICA_CONNECTION, action.m_sICAConnection.GetBuffer(300), 300);
	reg.QueryValue(STR_REG_HK_SCREEN_SAVER, action.m_sScreenSaver.GetBuffer(300), 300);
	reg.QueryValue(STR_REG_HK_ACTION_ID, (DWORD *)&action.m_uActionId);
	reg.QueryValue(STR_REG_HK_SUB_ACTION_ID, (DWORD* )&action.m_uSubActionId);
	reg.QueryValue(STR_REG_HK_TIMER_VALUE, &action.m_dwTimerValue);
	reg.QueryValue(STR_REG_HK_PASTE_TEXT, action.m_sPasteText.GetBuffer(500), 500);
	action.m_sPasteText.ReleaseBuffer();
	reg.QueryValue(STR_REG_HK_SEARCH_ENGINE, action.m_sSearchEngine.GetBuffer(500), 500);
	action.m_sSearchEngine.ReleaseBuffer();
	reg.QueryValue(STR_REG_HK_DISPLAY_MESSAGE, action.m_sDisplayMessage.GetBuffer(5000), 5000);
	action.m_sDisplayMessage.ReleaseBuffer();
	return true;
}

DWORD CRegistryConfig::SearchMaxID(CTreeCtrlEx& m_tvCommands, HTREEITEM hItem)
{
	DWORD dwMaxId = 0;

	hItem = m_tvCommands.GetChildItem(hItem);
	for(;;)
	{
		if (! hItem) break;
		INIT_ITEM_DATA(hItem);
		if (pItemData->m_nType == CItemData::Command)
		{
			if (pItemData->m_hkcommand.m_dwId > dwMaxId) dwMaxId = pItemData->m_hkcommand.m_dwId;
		}
		DWORD dwNewMaxId = SearchMaxID(m_tvCommands, hItem);
		if (dwNewMaxId > dwMaxId) dwMaxId = dwNewMaxId;
		hItem = m_tvCommands.GetNextSiblingItem(hItem);
	}
	return dwMaxId;
}

DWORD CRegistryConfig::GetID(CTreeCtrlEx& m_tvCommands)
{
	return SearchMaxID(m_tvCommands, TVI_ROOT)+1;
}

void CRegistryConfig::RestoreCommand(CRegistry& reg, HTREEITEM hItemParent, CHotkeyDlg& dlg)
{
	CHotkey hotkey;
	CHKCommand hkcommand;
	CScheduledTask scheduled_task;

	if (!reg.QueryValue(STR_REG_HK_DESCRIPTION, hkcommand.m_sDescription.GetBuffer(300), 300)) return;
	LOGInfof("    Restoring command %s...", hkcommand.m_sDescription);

	reg.QueryValue(STR_REG_HK_ACTIVATE, &hkcommand.m_fActive);
	reg.QueryValue(STR_REG_HK_EXECUTION_MINIMIZED, &hkcommand.m_fExecutionMinimized);
	if (! reg.QueryValue(STR_REG_HK_ID, &hkcommand.m_dwId)) 
	{
		hkcommand.m_dwId = GetID(dlg.m_tvCommands);
		LOG3F("Id = %d\n", hkcommand.m_dwId);
	}

	int nCountKeys = reg.CountKeys();
	for(int nIndex = 0; nIndex < nCountKeys; nIndex++)
	{	
		char szKeyName[MAX_PATH+1];
		if (reg.EnumKey(nIndex, szKeyName, sizeof(szKeyName)))
		{
			CRegistry regSub(reg, szKeyName);
			if (regSub.Open(FALSE))
			{
				if (! _tcsnicmp(szKeyName, STR_ACTION, strlen(STR_ACTION)))
				{
					CAction action;
					if (RestoreAction(regSub, action))
					{
						hkcommand.AddAction(action);			
					}
				}
				else if (! _tcsnicmp(szKeyName, STR_HOTKEY, strlen(STR_HOTKEY)))
				{
					if (RestoreHotkey(regSub, hotkey))
					{
						if (hotkey.IsValid())
							hkcommand.AddHotkey(hotkey);
					}
				}
				else if (! _tcsnicmp(szKeyName, STR_SCHEDUL, strlen(STR_SCHEDUL)))
				{
					if (RestoreScheduledTask(regSub, scheduled_task))
					{
						hkcommand.AddScheduledTask(scheduled_task);
					}
				}
			}
		}
	}
	hkcommand.RegisterHotkeysAndScheduledTasks(dlg, dlg.m_scheduler);

	dlg.AddCommand(hItemParent, hkcommand);
}

void CRegistryConfig::RestoreGroup(CRegistry& reg, HTREEITEM hItemParent, CHotkeyDlg& dlg)
{
	CString sGrpName;
	reg.QueryValue(STR_REG_HK_GROUP_NAME, sGrpName.GetBuffer(300), 300);
	LOGInfof("  Restoring group %s...", sGrpName);
	HTREEITEM hItem = dlg.AddGroup(hItemParent, sGrpName);

	int nCountKeys = reg.CountKeys();
	if (nCountKeys <= 0) return;
	DWORD dwType;
	for(int nIndex = 0; nIndex < nCountKeys; nIndex++)
	{	
		char szKeyName[MAX_PATH+1];
		if (reg.EnumKey(nIndex, szKeyName, sizeof(szKeyName)))
		{
			CRegistry regSub(reg, szKeyName);
			if (regSub.Open(FALSE))
			{
				if (regSub.QueryValue(STR_REG_HK_TYPE, &dwType))
				{
					switch(dwType)
					{
					case CItemData::Group:
						RestoreGroup(regSub, hItem, dlg);
						break;
					case CItemData::Command:
						RestoreCommand(regSub, hItem, dlg);
						break;
					default:
						ASSERT(FALSE);
					}
				}
			}
		}
	}
}

void CRegistryConfig::RestoreHotkeysConfig(CHotkeyDlg& dlg)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_ROOT "\\Hotkeys";
	LOGInfof("Restoring hotkey config '%s'...", sRegkey);
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);

	if (! reg.Open(FALSE)) 
	{
		LOGWarnf("Unable to open hotkey configuration registry entry");
		return;
	}
	int nCountKeys = reg.CountKeys();
	if (nCountKeys <= 0)
	{
		LOGInfof("No hotkey registered.");
		return;
	}
	DWORD dwType;
	for(int nIndex = 0; nIndex < nCountKeys; nIndex++)
	{
		char szKeyName[MAX_PATH+1];
		if (reg.EnumKey(nIndex, szKeyName, sizeof(szKeyName)))
		{
			LOGDebugf("Restoring key %s...", szKeyName);
			CRegistry regGrp(reg, szKeyName);
			if (regGrp.Open(FALSE))
			{
				if (regGrp.QueryValue(STR_REG_HK_TYPE, &dwType))
				{
					if (dwType == CItemData::Group)
					{
						RestoreGroup(regGrp, TVI_ROOT, dlg);
					}
				}
			}
		}
	}
	LOGInfof("Hotkey config restored.");
}

#define STR_REG_PREF_PATHPNICA			"PathPNICA"

#define STR_REG_PREF_TIP_BKTEXT			"TipBkColor"
#define STR_REG_PREF_TIP_TEXT			"TipColor"
#define STR_REG_PREF_TIP_POS			"TipPos"
//	Font
#define STR_REG_PREF_TIP_FONT_NAME		"TipFontName"
#define STR_REG_PREF_TIP_FONT_SIZE		"TipFontSize"
#define STR_REG_PREF_TIP_FONT_BOLD		"TipFontBold"
#define STR_REG_PREF_TIP_FONT_ITALIC	"TipFontItalic"
#define STR_REG_PREF_TIP_FONT_UNDERLINE	"TipFontUnderline"
#define STR_REG_PREF_TIP_FONT_STRIKE_OUT	"TipFontStrikeOut"
#define STR_REG_PREF_TIP_FONT_CHAR_SET	"TipFontCharSet"

#define STR_REG_PREF_TIP_TIMER			"TipTimer"
#define STR_REG_PREF_TIP_DISP_DETAILS	"TipDetails"
#define STR_REG_PREF_TIP_NOTIFIER		"TipNotifier"

void CRegistryConfig::RestorePreferences(CHotkeyDlg& dlg)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_ROOT;
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);

	if (! reg.Open(FALSE)) return;
	
	reg.QueryValue(STR_REG_PREF_PATHPNICA, dlg.m_sPathPNICA.GetBuffer(MAX_PATH+1), MAX_PATH);

	COLORREF cr;
	if (reg.QueryValue(STR_REG_PREF_TIP_BKTEXT, &cr))
	{
		dlg.m_wndtip.m_wndtipcolor.SetBkTextColor(cr);
	}
	if (reg.QueryValue(STR_REG_PREF_TIP_TEXT, &cr))
	{
		dlg.m_wndtip.m_wndtipcolor.SetTextColor(cr);
	}
	int nPos;
	if (reg.QueryValue(STR_REG_PREF_TIP_POS, &nPos))
	{
		dlg.m_wndtip.SetAutomaticPos(nPos);
	}
	DWORD dwShowTipTimer;
	if (reg.QueryValue(STR_REG_PREF_TIP_TIMER, &dwShowTipTimer))
	{
		dlg.m_dwShowTipTime = dwShowTipTimer;
	}
	reg.QueryValue(STR_REG_PREF_TIP_DISP_DETAILS, &dlg.m_fTipDisplayDetails);
	reg.QueryValue(STR_REG_PREF_TIP_NOTIFIER, &dlg.m_fTipNotifier);

	LOGFONT logfont;
	int nFontSize;
	if (reg.QueryValue(STR_REG_PREF_TIP_FONT_NAME, logfont.lfFaceName, sizeof(logfont.lfFaceName))
	&& reg.QueryValue(STR_REG_PREF_TIP_FONT_SIZE, &nFontSize)
	&& reg.QueryValue(STR_REG_PREF_TIP_FONT_BOLD, &logfont.lfWeight)
	&& reg.QueryValue(STR_REG_PREF_TIP_FONT_ITALIC, &logfont.lfItalic)
	&& reg.QueryValue(STR_REG_PREF_TIP_FONT_UNDERLINE, &logfont.lfUnderline)
	&& reg.QueryValue(STR_REG_PREF_TIP_FONT_STRIKE_OUT, &logfont.lfStrikeOut)
	&& reg.QueryValue(STR_REG_PREF_TIP_FONT_CHAR_SET, &logfont.lfCharSet))
	{
		dlg.m_wndtip.m_wndtipfont.m_font.CreateFont(CMyFont::ConvertFontSizeToFontHeight(nFontSize), 0, 0, 0, logfont.lfWeight, logfont.lfItalic, logfont.lfUnderline, logfont.lfStrikeOut, logfont.lfCharSet, 0, 0, 0, FIXED_PITCH | FF_DONTCARE, logfont.lfFaceName);
	}
}

void CRegistryConfig::SavePreferences(CHotkeyDlg& dlg)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_ROOT;
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);

	if (! reg.Open(true)) return;
	
	reg.SetValue(STR_REG_PREF_PATHPNICA, dlg.m_sPathPNICA);
	reg.SetValue(STR_REG_PREF_TIP_BKTEXT, dlg.m_wndtip.m_wndtipcolor.GetBkTextColor());
	reg.SetValue(STR_REG_PREF_TIP_TEXT, dlg.m_wndtip.m_wndtipcolor.GetTextColor());
	reg.SetValue(STR_REG_PREF_TIP_POS, dlg.m_wndtip.GetAutomaticPos());

	LOGFONT logfont;
	dlg.m_wndtip.m_wndtipfont.m_font.GetLogFont(&logfont);
	reg.SetValue(STR_REG_PREF_TIP_FONT_NAME, logfont.lfFaceName);
	reg.SetValue(STR_REG_PREF_TIP_FONT_SIZE, CMyFont::ConvertFontHeightToFontSize(logfont.lfHeight));
	reg.SetValue(STR_REG_PREF_TIP_FONT_BOLD, logfont.lfWeight);
	reg.SetValue(STR_REG_PREF_TIP_FONT_ITALIC, logfont.lfItalic);
	reg.SetValue(STR_REG_PREF_TIP_FONT_UNDERLINE, logfont.lfUnderline);
	reg.SetValue(STR_REG_PREF_TIP_FONT_STRIKE_OUT, logfont.lfStrikeOut);
	reg.SetValue(STR_REG_PREF_TIP_FONT_CHAR_SET, logfont.lfCharSet);

	reg.SetValue(STR_REG_PREF_TIP_TIMER, dlg.m_dwShowTipTime);
	reg.SetValue(STR_REG_PREF_TIP_DISP_DETAILS, dlg.m_fTipDisplayDetails);
	reg.SetValue(STR_REG_PREF_TIP_NOTIFIER, dlg.m_fTipNotifier);
}

void CRegistryConfig::SaveURLS(const CStringArray& saQueryURLs, LPCTSTR last)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_GOTOURLLIST;
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);
	if (! reg.Open(true)) return;
	reg.DeleteAllValues();
	
	//	Last
	reg.SetValue("URL.last", last);

	CString sKey;
	for(int i=0; i<saQueryURLs.GetSize(); i++)
	{
		sKey.Format("URL.%04d", i);
		reg.SetValue(sKey, saQueryURLs[i]);
	}
}

void CRegistryConfig::RestoreURLS(CStringArray& saQueryURLs, CString& last)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_GOTOURLLIST;
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);
	if (! reg.Open(false)) return;
	
	//	Last
	reg.QueryValue("URL.last", last.GetBuffer(1000), 1000);

	CString sURL, sKey;
	for(int i=0; i<1000; i++)
	{
		sKey.Format("URL.%04d", i);
		if (! reg.QueryValue(sKey, sURL.GetBuffer(1000), 1000)) break;
		saQueryURLs.Add(sURL);
	}
}

void CRegistryConfig::SaveQueryStrings(const CStringArray& saQueryStrings, LPCTSTR last)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_QUERYSTRING;
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);
	if (! reg.Open(true)) return;
	reg.DeleteAllValues();

	//	Last
	reg.SetValue("QueryString.last", last);

	CString sKey;
	for(int i=0; i<saQueryStrings.GetSize(); i++)
	{
		sKey.Format("QueryString.%04d", i);
		reg.SetValue(sKey, saQueryStrings[i]);
	}
}

void CRegistryConfig::RestoreQueryStrings(CStringArray& saQueryStrings, CString& last)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_QUERYSTRING;
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);
	if (! reg.Open(false)) return;
	
	//	Last
	reg.QueryValue("QueryString.last", last.GetBuffer(1000), 1000);
	
	CString sQueryString, sKey;
	for(int i=0; i<1000; i++)
	{
		sKey.Format("QueryString.%04d", i);
		if (! reg.QueryValue(sKey, sQueryString.GetBuffer(1000), 1000)) break;
		saQueryStrings.Add(sQueryString);
	}
}

void CRegistryConfig::ConvertCommandConfigV2(CRegistry& reg)
{
	CHotkey hotkey;

	if (!reg.QueryValue(STR_REG_HK_VIRTUALKEY, (DWORD *)&hotkey.m_uVirtualKey) ||
		!reg.QueryValue(STR_REG_HK_MODIFIER, (DWORD *)&hotkey.m_uVkmodifier)
		)
	{
		return;
	}

	reg.DeleteValue(STR_REG_HK_VIRTUALKEY);
	reg.DeleteValue(STR_REG_HK_MODIFIER);

	if (! hotkey.IsValid()) return;

	CString sIndex;
	sIndex.Format(STR_HOTKEY_FORMAT, 0);
	CRegistry regHotkey(reg, sIndex);
	regHotkey.Open(TRUE);
	SaveHotkey(regHotkey, hotkey);
}

void CRegistryConfig::ConvertGroupConfigV2(CRegistry& reg)
{
	CString sGrpName;
	reg.QueryValue(STR_REG_HK_GROUP_NAME, sGrpName.GetBuffer(300), 300);

	int nCountKeys = reg.CountKeys();
	if (nCountKeys <= 0) return;
	DWORD dwType;
	for(int nIndex = 0; nIndex < nCountKeys; nIndex++)
	{	
		char szKeyName[MAX_PATH+1];
		if (reg.EnumKey(nIndex, szKeyName, sizeof(szKeyName)))
		{
			CRegistry regSub(reg, szKeyName);
			if (regSub.Open(TRUE))
			{
				if (regSub.QueryValue(STR_REG_HK_TYPE, &dwType))
				{
					switch(dwType)
					{
					case CItemData::Group:
						ConvertGroupConfigV2(regSub);
						break;
					case CItemData::Command:
						ConvertCommandConfigV2(regSub);
						break;
					default:
						ASSERT(FALSE);
					}
				}
			}
		}
	}
}

void CRegistryConfig::ConvertHotkeysConfigV2(CHotkeyDlg& dlg)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_ROOT "\\Hotkeys";
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);

	if (! reg.Open(FALSE)) return;

	int nCountKeys = reg.CountKeys();
	if (nCountKeys <= 0) return;
	DWORD dwType;
	for(int nIndex = 0; nIndex < nCountKeys; nIndex++)
	{
		char szKeyName[MAX_PATH+1];
		if (reg.EnumKey(nIndex, szKeyName, sizeof(szKeyName)))
		{
			CRegistry regGrp(reg, szKeyName);
			if (regGrp.Open(FALSE))
			{
				if (regGrp.QueryValue(STR_REG_HK_TYPE, &dwType))
				{
					if (dwType == CItemData::Group)
					{
						ConvertGroupConfigV2(regGrp);
					}
				}
			}
		}
	}
}

void CRegistryConfig::ConvertHotkeysConfigV1(CHotkeyDlg& dlg)
{
	CString sRegkey = STR_REGISTRY_BASE_ENTRY STR_REG_ROOT "\\List";
	CRegistry reg(HKEY_CURRENT_USER, sRegkey);

	if (! reg.Open(FALSE)) return;

	int nCountKeys = reg.CountKeys();
	if (nCountKeys > 0)
	{
		HTREEITEM hItemGen = dlg.AddGroup(TVI_ROOT, "G�n�ral");
		for(int nIndex = 0; nIndex < nCountKeys; nIndex++)
		{
			char szKeyName[MAX_PATH+1];
			if (reg.EnumKey(nIndex, szKeyName, sizeof(szKeyName)))
			{
				CHotkey hotkey;
				CString sRegSubKey;
				sRegSubKey.Format("%s\\%d", sRegkey, nIndex);
				CRegistry reghotkey(HKEY_CURRENT_USER, sRegSubKey);
				if (reghotkey.Open(FALSE))
				{
					CString sDesc, sProgram, sURL, sRASEntry;
					UINT uModifier, uVirtualKey, uActionType, uOtherActionId;
					BOOL fActive = TRUE;
					reghotkey.QueryValue(STR_REG_HK_DESCRIPTION, sDesc.GetBuffer(250), 250);
					reghotkey.QueryValue(STR_REG_HK_PROGRAM, sProgram.GetBuffer(MAX_PATH+1), MAX_PATH+1);
					reghotkey.QueryValue(STR_REG_HK_URL, sURL.GetBuffer(MAX_PATH+1), MAX_PATH+1);
					reghotkey.QueryValue(STR_REG_HK_RAS_ENTRY, sRASEntry.GetBuffer(200), 200);
					reghotkey.QueryValue(STR_REG_HK_ACTION_ID, (unsigned long *)&uActionType);
					reghotkey.QueryValue(STR_REG_HK_SUB_ACTION_ID, (unsigned long *)&uOtherActionId);
					reghotkey.QueryValue(STR_REG_HK_VIRTUALKEY, (unsigned long *)&uVirtualKey);
					reghotkey.QueryValue(STR_REG_HK_MODIFIER, (unsigned long *)&uModifier);
					reghotkey.QueryValue(STR_REG_HK_ACTIVATE, &fActive);
					CHKCommand hkcommand;
					hkcommand.m_fActive = fActive;
					hkcommand.m_sDescription = sDesc;

					hotkey.m_uVirtualKey = uVirtualKey;
					hotkey.m_uVkmodifier = uModifier;
					hkcommand.AddHotkey(hotkey);
					
					CAction action;
					action.m_sProgram = sProgram;
					action.m_sURL = sURL;
					action.m_sRASEntry = sRASEntry;
					action.m_uActionId = uActionType;
					action.m_uSubActionId = uOtherActionId;
					hkcommand.AddAction(action);

					//hkcommand.RegisterHotkeysAndScheduledTasks(*this, m_scheduler);
					dlg.AddCommand(hItemGen, hkcommand);
				}
			}
		}
		SaveHotkeysConfig(dlg);
		reg.DeleteSubKeys();
		dlg.CleanMemory(TVI_ROOT);
		dlg.m_tvCommands.DeleteItem(TVI_ROOT);
	}
}