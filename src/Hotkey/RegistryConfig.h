#pragma once

#include <registry.h>


#include <TreeCtrlEx.h>

#include "ItemData.h"
#include "action.h"
#include "Hotkeyc.h"
#include "ScheduledTask.h"


class CHotkeyDlg;

class CRegistryConfig
{
	//BOOL m_fContinue;
public:	
	CRegistryConfig();

	void Init();

	void ConvertHotkeysConfigV1(CHotkeyDlg& dlg);
	void ConvertHotkeysConfigV2(CHotkeyDlg& dlg);

	DWORD SearchMaxID(CTreeCtrlEx& m_tvCommands, HTREEITEM hItem);
	DWORD GetID(CTreeCtrlEx& m_tvCommands);

	void SaveHotkeysConfig(CHotkeyDlg& dlg);
	void RestoreHotkeysConfig(CHotkeyDlg& dlg);

	void SavePreferences(CHotkeyDlg& dlg);
	void RestorePreferences(CHotkeyDlg& dlg);

	void SaveURLS(const CStringArray& saQueryURLs, LPCTSTR last);
	void RestoreURLS(CStringArray& saQueryURLs, CString& last);
	void SaveQueryStrings(const CStringArray& saQueryStrings, LPCTSTR last);
	void RestoreQueryStrings(CStringArray& saQueryStrings, CString& last);

	void SaveHotkey(CRegistry& reg, const CHotkey& hotkey);
private:
	void ConvertGroupConfigV2(CRegistry& reg);
	void ConvertCommandConfigV2(CRegistry& reg);

	int SaveGroup(CRegistry& regParent, HTREEITEM hItem, CHotkeyDlg& dlg);
	void RestoreGroup(CRegistry& reg, HTREEITEM hItem, CHotkeyDlg& dlg);

	void SaveCommand(CRegistry& regParent, HTREEITEM hItem, CHotkeyDlg& dlg);
	void SaveAction(CRegistry& reg, const CAction& action);
	void SaveScheduledTask(CRegistry& reg, const CScheduledTask& scheduled_task);
	
	void RestoreCommand(CRegistry& reg, HTREEITEM hItemParent, CHotkeyDlg& dlg);
	BOOL RestoreAction(CRegistry& reg, CAction& action);
	BOOL RestoreHotkey(CRegistry& reg, CHotkey& hotkey);
	BOOL RestoreScheduledTask(CRegistry& reg, CScheduledTask& scheduled_task);

private:
	void PurgeRegEntry(CRegistry& reg);

	static CLogger * m_logger;
};