// SampleTip.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "SampleTip.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSampleTip

CSampleTip::CSampleTip()
{
}

CSampleTip::~CSampleTip()
{
}


BEGIN_MESSAGE_MAP(CSampleTip, CStatic)
	//{{AFX_MSG_MAP(CSampleTip)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSampleTip message handlers

void CSampleTip::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	Paint(dc);
	// Do not call CStatic::OnPaint() for painting messages
}

#define LEFT_TEXT	2
#define TOP_TEXT	0

void CSampleTip::Paint(CDC &dc)
{
	CRect rc;
	GetClientRect(&rc);

	COLORREF crBkWindow  = m_wndtipcolor.GetBkTextColor();
	COLORREF crFgText    = m_wndtipcolor.GetTextColor();
	COLORREF crBkText    = m_wndtipcolor.GetBkTextColor();

	if (! IsWindowEnabled())
	{
		crBkWindow = crBkText = GetSysColor(COLOR_BTNFACE);
		crFgText   = GetSysColor(COLOR_GRAYTEXT);
	}
	
	CDC memdc;
	CBitmap bm;
	memdc.CreateCompatibleDC(&dc);
	bm.CreateCompatibleBitmap(&dc, rc.Width(), rc.Height());
	memdc.SelectObject(bm);

	CBrush brush_bg;
	brush_bg.CreateSolidBrush(crBkWindow);
	memdc.FillRect(rc, &brush_bg);

	memdc.SelectObject(m_wndtipfont.m_font);

	CSize sizetext = memdc.GetTextExtent("0", 1);

	//	Normal text
	memdc.SetTextColor(crFgText);
	memdc.SetBkColor(crBkText);
	memdc.TextOut(LEFT_TEXT, TOP_TEXT, "Exemple d'affichage");
	
	dc.BitBlt(0, 0, rc.Width(), rc.Height(), &memdc, 0, 0, SRCCOPY);
}
