#if !defined(AFX_SAMPLETIP_H__FC5272F7_FC5E_4ABE_BA02_2605F2F65584__INCLUDED_)
#define AFX_SAMPLETIP_H__FC5272F7_FC5E_4ABE_BA02_2605F2F65584__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SampleTip.h : header file
//

#include <Wndtip.h>

/////////////////////////////////////////////////////////////////////////////
// CSampleTip window

class CSampleTip : public CStatic
{
// Construction
public:
	CSampleTip();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSampleTip)
	//}}AFX_VIRTUAL

// Implementation
public:
	void Paint(CDC& dc);
	virtual ~CSampleTip();
	CWndTip::CWndTipColor m_wndtipcolor;
	CWndTip::CWndTipFont m_wndtipfont;

	// Generated message map functions
protected:
	//{{AFX_MSG(CSampleTip)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAMPLETIP_H__FC5272F7_FC5E_4ABE_BA02_2605F2F65584__INCLUDED_)
