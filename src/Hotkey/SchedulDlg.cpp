// SchedulDlg.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "SchedulDlg.h"

#include <log.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSchedulDlg dialog


CSchedulDlg::CSchedulDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSchedulDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSchedulDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSchedulDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSchedulDlg)
	DDX_Control(pDX, IDC_LIST_SCHEDULS, m_lvSchedulers);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSchedulDlg, CDialog)
	//{{AFX_MSG_MAP(CSchedulDlg)
	ON_NOTIFY(HDN_ITEMCLICK, IDC_LIST_SCHEDULS, OnItemclickListScheduls)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSchedulDlg message handlers

void CSchedulDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	m_lvSchedulers.RemoveAllSchedul();
	CDialog::OnCancel();
}

void CSchedulDlg::OnOK() 
{
	// TODO: Add extra validation here
	m_lvSchedulers.RemoveAllSchedul();
	CDialog::OnOK();
}

BOOL CSchedulDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_lvSchedulers.Init();

	//CScheduledTasks tasks;
	//m_lvSchedulers.FillScheduledTasks(tasks, *m_ptvCommands, TVI_ROOT, "");
	m_lvSchedulers.FillScheduledTasks(*m_pscheduler, *m_ptvCommands, TVI_ROOT, "");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSchedulDlg::OnItemclickListScheduls(NMHDR* pNMHDR, LRESULT* pResult) 
{
	HD_NOTIFY *phdn = (HD_NOTIFY *) pNMHDR;
	// TODO: Add your control notification handler code here
	LOG1F("OnItemclickListScheduls\n");
	*pResult = 0;
}
