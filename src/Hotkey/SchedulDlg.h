#if !defined(AFX_SCHEDULDLG_H__7CACC260_4048_416D_AF70_8287AED11D37__INCLUDED_)
#define AFX_SCHEDULDLG_H__7CACC260_4048_416D_AF70_8287AED11D37__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SchedulDlg.h : header file
//

#include "ScheduledTask.h"
#include <TreeCtrlEx.h>
#include "ListSchedul.h"

/////////////////////////////////////////////////////////////////////////////
// CSchedulDlg dialog

class CSchedulDlg : public CDialog
{
// Construction
public:
	CScheduler * m_pscheduler;
	CTreeCtrlEx * m_ptvCommands;
	
	CSchedulDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSchedulDlg)
	enum { IDD = IDD_DIALOG_SCHEDUL };
	CListSchedul	m_lvSchedulers;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSchedulDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSchedulDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnItemclickListScheduls(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCHEDULDLG_H__7CACC260_4048_416D_AF70_8287AED11D37__INCLUDED_)
