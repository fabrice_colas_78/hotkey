// ScheduledTask.cpp: implementation of the CScheduledTask class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "hotkey.h"
#include "ScheduledTask.h"

#include "Global.h"

#include "..\HotkeyLib\HotkeyLib.h"

#include <CommonLib.h>
#include <log.h>
#include <macrosdef.h>
#include <winerr.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define LOGF	LOG2F

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DWORD CScheduler::ms_dwTaskId      = 153;

#define MY_LOGGER_NAME	LOGGER_HOTKEY_SCHEDULER_CLASS

#include <LoggerMacros.h>
INITLogger(CScheduler::m_logger);

CScheduler::CScheduler()
{
	if (m_logger == NULL)
	{
		//	Get logger only the first time
		m_logger = g_logFactory.GetLoggerPtr(MY_LOGGER_NAME);
	}
	
	m_pCallbackFunc     = NULL;
	m_fSchedulerStarted = FALSE;
	m_fStopScheduler    = FALSE;
	m_fBusy             = FALSE;
	m_dwIdleTime        = 0;
}

CScheduler::~CScheduler()
{
	StopScheduler();
}

void CScheduler::WaitForNotBusy(const char * szFunction)
{
	while(m_fBusy)
	{
		LOGTracef("Busy for %s...", szFunction);
		Sleep(1);
	}
}

void CScheduler::SetCallback(PF_SCHEDULER_CALLBACK pCallbackFunc, void * param)
{
	m_pCallbackFunc      = pCallbackFunc;
	m_pParamCallbackFunc = param;
}

BOOL CScheduler::StartScheduler()
{
	if (m_fSchedulerStarted) return FALSE;

	if (! HotkeyLibInstallHooks())
	{
		LOGErrorf("Hooks not installed %s", GetLastErrorString());
	}
	else
	{
		LOGInfof("Hooks installed");
	}

	m_fBusy             = FALSE;
	m_fStopScheduler    = FALSE;
	
	CloseHandle(NewThread(SchedulerThread, (void *)this));
	return TRUE;
}

BOOL CScheduler::StopScheduler()
{
	HotkeyLibUninstallHooks();
	
	if (! m_fSchedulerStarted) return FALSE;
	m_fStopScheduler = TRUE;
	Sleep(1200);
	if (m_fSchedulerStarted) return FALSE;
	return TRUE;
}

#ifdef _USE_GETIDLESECONDS
DWORD CScheduler::GetIdleSeconds()
{
#ifdef _DEBUG
	DWORD dwKeyboard, dwMouse;
	BOOL  f;
	HotkeyLibGetValues(dwKeyboard, dwMouse, f);
	//LOGF("IdleSeconds: %d  (%d,%d,%d)\n", HotkeyLibGetIdleSeconds(), dwKeyboard, dwMouse, f);
#endif
	return HotkeyLibGetIdleSeconds();
}
#endif

void CScheduler::Check()
{
	static BOOL s_fInIdle = false;

	LOGTracef("Checking...");
	WaitForNotBusy("Check");
	SetBusy(TRUE);
	CDateRef drLocal;
	drLocal.SetCurrentLocalTime();
#ifdef _USE_GETIDLESECONDS
	DWORD dwIdleSeconds = GetIdleSeconds();
#endif
	BOOL fResetIdleTrigger = false;
	if (dwIdleSeconds == 0 && s_fInIdle)
	{
		LOGDebugf("Resetting trigger idle.");
		s_fInIdle = false;
		fResetIdleTrigger = true;
	}
	else if (dwIdleSeconds > 0 && !s_fInIdle)
	{
		LOGDebugf("Entering in idle.");
		s_fInIdle = true;
	}
	//LOGF("Check %s...\n", drLocal.ToString());
	CScheduledTask st;
	for(int nTaskIndex=0; nTaskIndex<m_scheduledTasks.GetSize(); nTaskIndex++)
	{
		st = m_scheduledTasks.GetAt(nTaskIndex);
		//LOGF("TaskId:%d cur:%s date:%s\n", nTaskId, drLocal.ToString(), st.m_drNext.ToString());
		BOOL fCall = FALSE;
#ifdef _USE_GETIDLESECONDS
		if ((st.GetTaskType() == TASKIDLE) && !st.IsIdleNotTriggered() && fResetIdleTrigger)
		{
			LOGDebugf("Reset triggered for id:%u, trig:%d", st.GetTaskId(), st.IsIdleNotTriggered());
			st.ResetTriggered();
			m_scheduledTasks.SetAt(nTaskIndex, st);
		}
		if ((st.GetTaskType() == TASKIDLE) && st.IsIdleNotTriggered())
		{
			LDA("Task idle %d IdleTime:%d IdleValue:%d IsIdleTrigered:%d", st.GetTaskId(), dwIdleSeconds, st.GetIdleValue(), st.IsIdleNotTriggered());
			fCall = dwIdleSeconds >= st.GetIdleValue();
			if (fCall)
			{
				LOGDebugf("Idle triggered for id:%u, trig:%d", st.GetTaskId(), st.IsIdleNotTriggered());
				st.SetTriggered();
				m_scheduledTasks.SetAt(nTaskIndex, st);
			}
		}
		else 
#endif
		if (drLocal >= st.m_drNext)
		{
			LOGInfof("Check: task %s released (id:%d).", st.GetScheduledTaskString(), st.GetTaskId());
			st.m_drPrevious = drLocal;
			st.ComputeNext();
			m_scheduledTasks.SetAt(nTaskIndex, st);
#ifdef _USE_GETIDLESECONDS
			if (st.IsIdle())
			{
				fCall = dwIdleSeconds >= st.GetIdleValue();
				//LOGF("Idle task %d call=%d (%d < %d)\n", st.GetTaskId(), fCall, st.GetIdleValue(), dwIdleSeconds);
			}
			else
#endif
			{
				fCall = TRUE;
			}
		}
		if (m_pCallbackFunc && fCall)
		{
			LOGInfof("Executing task %s (id:%d)...", st.GetScheduledTaskString(), st.GetTaskId());
#ifdef _DEBUG0
			LOGWarnf("task id:%d not executed due to debug mode.", st.GetTaskId());
#else
#ifdef _WTS243
			LOGWarnf("task id:%d not executed due to WTS243 mode.", st.GetTaskId());
#else
			m_pCallbackFunc(st.GetTaskId(), m_pParamCallbackFunc);
#endif
#endif
		}
#ifdef _DEBUG0
		LOG3F("--------------------------------------\n");
		for(int ii=0; ii<m_scheduledTasks.GetSize(); ii++)
		{
			CScheduledTask s = m_scheduledTasks.GetAt(ii);
			LOG3F("Id:%03d Next:%s\n", s.GetTaskId(), s.m_drNext.ToString(CDateRef::WD_DAY_MONTH_YEAR_HH_MI_SS));
		}
#endif
	}
	SetBusy(FALSE);
}

DWORD WINAPI CScheduler::SchedulerThread(void * param)
{
	CScheduler& scheduler = *((CScheduler *)param);
	LOGInfof("D�marrage du scheduler...");
	scheduler.m_fSchedulerStarted = TRUE;
	for(;;)
	{
		if (scheduler.m_fStopScheduler) break;
		scheduler.Check();
		Sleep(1000);
	}
	scheduler.m_fSchedulerStarted = FALSE;
	LOGInfof("Arr�t du scheduler.");
	return 0;
}

DWORD CScheduler::AddTask(int nTaskType, WORD wSecond, WORD wMinute, WORD wHour, WORD wDay, WORD wMonth, WORD wYear, WORD maskDaysOfWeek, DWORD maskDaysOfMonth, WORD maskMonths, BOOL fOnIdle, WORD wSecondIdle, WORD wMinuteIdle, WORD wHourIdle, WORD wRepetition)
{
	CScheduledTask scheduled_task(nTaskType, wSecond, wMinute, wHour, wDay, wMonth, wYear, maskDaysOfWeek, maskDaysOfMonth, maskMonths, fOnIdle, wSecondIdle, wMinuteIdle, wHourIdle, wRepetition);
	return AddTask(scheduled_task);
}

DWORD CScheduler::AddTask(CScheduledTask& scheduled_task)
{
	int nTaskType = scheduled_task.GetTaskType();
	LOGInfof("Adding task: type:%d, %s...", nTaskType, scheduled_task.GetScheduledTaskString());
	if (nTaskType == TASKWEEKLY)
	{
		if ((scheduled_task.GetDaysOfWeekMask() & 0x7F) == 0)
		{
			LOGErrorf("!Error: AddTask: bad maskDayOfWeek %02X", scheduled_task.GetDaysOfWeekMask());
			return NO_TASK_ID;
		}
	}
	else if (nTaskType == TASKMONTHLY)
	{
		if ((scheduled_task.GetDaysOfMonthMask() & 0x7FFFFFFF) == 0)
		{
			LOGErrorf("!Error: AddTask: bad maskDayOfMonth %08X", scheduled_task.GetDaysOfMonthMask());
			return NO_TASK_ID;
		}
	}
	else if (nTaskType == TASKYEARLY)
	{
		if ((scheduled_task.GetMonthsMask() & 0xFFF) == 0)
		{
			LOGErrorf("!Error: AddTask: bad maskMonths %04X", scheduled_task.GetMonthsMask());
			return NO_TASK_ID;
		}
	}
	else if (nTaskType == TASKMONTHLYLASTDAY)
	{
		if ((scheduled_task.GetMonthsMask() & 0xFFF) == 0)
		{
			LOGErrorf("!Error: AddTask: bad maskMonths %04X", scheduled_task.GetMonthsMask());
			return NO_TASK_ID;
		}
	}
	else if (nTaskType == TASKMONTHLYFIRSTDAY)
	{
		if ((scheduled_task.GetMonthsMask() & 0xFFF) == 0)
		{
			LOGErrorf("!Error: AddTask: bad maskMonths %04X", scheduled_task.GetMonthsMask());
			return NO_TASK_ID;
		}
	}
	else if (nTaskType == TASKIDLE)
	{
		
	}
	WaitForNotBusy("AddTask");
	SetBusy(TRUE);
	scheduled_task.SetTaskId(GetNextTaskId());
	BOOL fCanComputeNext = true;
	if (m_fSchedulerStarted && scheduled_task.GetTaskType()==TASKATSTARTUP)
	{
		LOGInfof("Don't compute next for '%s' (%s)", scheduled_task.GetScheduledTaskString(), "At startup");
		fCanComputeNext = false;
	}
	if (fCanComputeNext)
	{
		scheduled_task.ComputeNext();
	}
	m_scheduledTasks.Add(scheduled_task);
	LOGInfof("Task added(id:%d); type:%d, %s.", scheduled_task.GetTaskId(), nTaskType, scheduled_task.GetScheduledTaskString());
	SetBusy(FALSE);
	return scheduled_task.GetTaskId();
}

BOOL CScheduler::DeleteTask(DWORD dwTaskId)
{
	LOGInfof("DeleteTask TaskId:%d", dwTaskId);
	WaitForNotBusy("DeleteTask");
	SetBusy(TRUE);
	BOOL fDeleted = FALSE;
	for(int i=0; i<m_scheduledTasks.GetSize(); i++)
	{
		CScheduledTask st = m_scheduledTasks.GetAt(i);
		if (st.GetTaskId() == dwTaskId)
		{
			m_scheduledTasks.RemoveAt(i);
			fDeleted = TRUE;
			break;
		}
	}
	if (! fDeleted) LOGInfof("DeleteTask: TaskId %d not valid", dwTaskId);
	SetBusy(FALSE);
	return fDeleted;
}

DWORD CScheduler::GetNextTaskId()
{
	return ++ms_dwTaskId;
}

CScheduledTask CScheduler::GetTaskFromId(DWORD dwTaskId)
{	
	WaitForNotBusy("GetTaskFromId");
	SetBusy(TRUE);
	CScheduledTask st_ret;
	for(int i=0; i<m_scheduledTasks.GetSize(); i++)
	{
		CScheduledTask st = m_scheduledTasks.GetAt(i);
		if (st.GetTaskId() == dwTaskId)
		{
			st_ret = st;
			break;
		}
	}
	SetBusy(FALSE);
	return st_ret;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#undef MY_LOGGER_NAME
#define MY_LOGGER_NAME	LOGGER_HOTKEY_SCHEDULED_TASK_CLASS

INITLogger(CScheduledTask::m_logger);


CScheduledTask::CScheduledTask()
{
	Init(-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, FALSE, FALSE, 0, 30, 0, 10);
}

CScheduledTask::CScheduledTask(int nTaskType, WORD wSecond, WORD wMinute, WORD wHour, WORD wDay, WORD wMonth, WORD wYear, WORD maskDaysOfWeek, DWORD maskDaysOfMonth, WORD maskMonths, BOOL fOnIdle, WORD wSecondIdle, WORD wMinuteIdle, WORD wHourIdle, WORD wRepetition)
{
	Init(nTaskType, wSecond, wMinute, wHour, wDay, wMonth, wYear, maskDaysOfWeek, maskDaysOfMonth, maskMonths, fOnIdle, false, wSecondIdle, wMinuteIdle, wHourIdle, wRepetition);
}

CScheduledTask::~CScheduledTask()
{
}

void CScheduledTask::Init(int nTaskType, WORD wSecond, WORD wMinute, WORD wHour, WORD wDayOfMonth, WORD wMonth, WORD wYear, WORD maskDaysOfWeek, DWORD maskDaysOfMonth, WORD maskMonths, BOOL fOnIdle, BOOL fIdleTriggered, WORD wSecondIdle, WORD wMinuteIdle, WORD wHourIdle, WORD wRepetition)
{
	if (m_logger == NULL)
	{
		//	Get logger only the first time
		m_logger = g_logFactory.GetLoggerPtr(MY_LOGGER_NAME);
	}

	m_dwTaskId        = NO_TASK_ID;

	m_drNext.SetUndefined();
	m_drNext.SetLang(DR_LANG_FRENCH);
	m_drPrevious.SetUndefined();
	m_drPrevious.SetLang(DR_LANG_FRENCH);

	m_nTaskType       = nTaskType;
	m_wSecond         = wSecond;
	m_wMinute         = wMinute;
	m_wHour           = wHour;
	m_wDayOfMonth     = wDayOfMonth;
	m_wMonth          = wMonth;
	m_wYear           = wYear;
	m_maskDaysOfWeek  = maskDaysOfWeek;
	m_maskDaysOfMonth = maskDaysOfMonth;
	m_maskMonths      = maskMonths;

	m_fOnIdle         = fOnIdle;
	m_fIdleTriggered  = fIdleTriggered;
	m_wSecondIdle     = wSecondIdle;
	m_wMinuteIdle     = wMinuteIdle;
	m_wHourIdle       = wHourIdle;

	m_wRepetition     = wRepetition;
}

void CScheduledTask::InitDefaultTime()
{
	SYSTEMTIME lt;
	GetLocalTime(&lt);
	CDateRef dr(lt), drCur;
	drCur.SetCurrentLocalTime();
	dr += 60;
	dr.SetSeconds(0);
	if ((dr - drCur) < 30)
		dr += 60;
	m_wSecond         = dr.GetSeconds();
	m_wMinute         = dr.GetMinutes();
	m_wHour           = dr.GetHours();
	m_wDayOfMonth     = dr.GetDayOfMonth();
	m_wMonth          = dr.GetMonth();
	m_wYear           = dr.GetYear();
	//SetDaysOfWeekMask(1<<lt.wDayOfWeek);
	//SetMonthsMask(1<<(lt.wMonth-1));
	//SetDaysOfMonthMask(1<<(lt.wDay-1));
}

void CScheduledTask::InitTimeAndDate(WORD wYear, WORD wMonth, WORD wDayOfMonth, WORD wHour, WORD wMinute, WORD wSecond)
{
	m_wSecond         = wSecond;
	m_wMinute         = wMinute;
	m_wHour           = wHour;
	m_wDayOfMonth     = wDayOfMonth;
	m_wMonth          = wMonth;
	m_wYear           = wYear;
}

void CScheduledTask::InitIdle(BOOL fOnIdle, WORD wInHour, WORD wInMinute, WORD wInSecond)
{
	m_fOnIdle     = fOnIdle;
	m_wSecondIdle = wInSecond;
	m_wMinuteIdle = wInMinute;
	m_wHourIdle   = wInHour;
}

void CScheduledTask::InitRepetition(WORD wRepetition)
{
	m_wRepetition = wRepetition;
}

void CScheduledTask::SetTaskType(int nTaskType)
{
	m_nTaskType = nTaskType;
}

void CScheduledTask::operator=(const CScheduledTask& st)
{
	Init(st.m_nTaskType, st.m_wSecond, st.m_wMinute, st.m_wHour, st.m_wDayOfMonth, st.m_wMonth, st.m_wYear, st.m_maskDaysOfWeek, st.m_maskDaysOfMonth, st.m_maskMonths, st.m_fOnIdle, st.m_fIdleTriggered, st.m_wSecondIdle, st.m_wMinuteIdle, st.m_wHourIdle, st.m_wRepetition);
	m_dwTaskId   = st.m_dwTaskId;
	m_drNext     = st.m_drNext;
	m_drPrevious = st.m_drPrevious;
}

void CScheduledTask::ComputeNext()
{
	CDateRef drLocal;
	drLocal.SetCurrentLocalTime();
	LOGDebugf("------------- %s (Previous:%s) ----------id: %d------", drLocal.ToString(), m_drPrevious.ToString(), m_dwTaskId);
	switch(m_nTaskType)
	{
	case TASKPUNCTUAL:
		{
			m_drNext.SetSeconds(m_wSecond);
			m_drNext.SetMinutes(m_wMinute);
			m_drNext.SetHours(m_wHour);
			m_drNext.SetDayOfMonth(m_wDayOfMonth);
			m_drNext.SetMonth(m_wMonth);
			m_drNext.SetYear(m_wYear);
			if (m_drNext <= drLocal) m_drNext.SetUndefined();
			LOGDebugf("Punctualy: ComputeNext: %s", m_drNext.ToString());
		}
		break;
	case TASKMINUTELY:
		{
			m_drNext = drLocal;
			m_drNext.SetSeconds(m_wSecond);
			if (m_drNext <= drLocal) m_drNext += ONE_MINUTE;
			LOGDebugf("Minutely: ComputeNext: %s", m_drNext.ToString());
		}
		break;
	case TASKHOURLY:
		{
			m_drNext = drLocal;
			m_drNext.SetSeconds(m_wSecond);
			m_drNext.SetMinutes(m_wMinute);
			if (m_drNext <= drLocal) m_drNext += ONE_HOUR;
			LOGDebugf("Hourly: ComputeNext: %s", m_drNext.ToString());
		}
		break;
	case TASKDAILY:
		{
			m_drNext = drLocal;
			m_drNext.SetSeconds(m_wSecond);
			m_drNext.SetMinutes(m_wMinute);
			m_drNext.SetHours(m_wHour);
			if (m_drNext <= drLocal) m_drNext += ONE_DAY;
			LOGDebugf("Daily: ComputeNext: %s", m_drNext.ToString());
		}
		break;
	case TASKWEEKLY:
	case TASKDAILYOPENDAY:
		{
			m_drNext = drLocal;
			m_drNext.SetSeconds(m_wSecond);
			m_drNext.SetMinutes(m_wMinute);
			m_drNext.SetHours(m_wHour);

			WORD wIndexDayOfWeek = drLocal.GetDayOfWeek();
			//LOGF("Today: %s DayOfWeek=%d mask=%02X\n", CDateRef::GetWeekDayString(wIndexDayOfWeek, TRUE), wIndexDayOfWeek, m_maskDaysOfWeek);
			WORD wMaskDayOfWeek = INDEX2BIT(wIndexDayOfWeek);
			for(int i=0; i<7; i++)
			{
				if (m_drNext > drLocal && BitTest(m_maskDaysOfWeek, wMaskDayOfWeek))
				{
					//LOGF("drNext=%s drLocal=%s\n", m_drNext.ToString(), drLocal.ToString());
					//LOGF("maskDaysOfWeek ok %d: %s %02X\n", wIndexDayOfWeek, CDateRef::GetWeekDayString(wIndexDayOfWeek, FALSE),  wMaskDayOfWeek);
					break;
				}
				if (++wIndexDayOfWeek > 6) wIndexDayOfWeek = 0;
				wMaskDayOfWeek = INDEX2BIT(wIndexDayOfWeek);
				m_drNext += ONE_DAY;
				//LOGF("ComputeNext next day of week %d...\n", wIndexDayOfWeek);
			}
			LOGDebugf("Weekly: ComputeNext: %s %s", m_drNext.GetWeekDayString(FALSE), m_drNext.ToString());
		}
		break;
	case TASKMONTHLY:
		{
			LOGDebugf("ComputeNext: TASKMONTHLY for '%s'", GetScheduledTaskString());
			m_drNext = drLocal;
			m_drNext.SetSeconds(m_wSecond);
			m_drNext.SetMinutes(m_wMinute);
			m_drNext.SetHours(m_wHour);
			
			DWORD maskDaysOfMonth = m_maskDaysOfMonth;
			if (BitTest(m_maskDaysOfMonth, MT_LASTDAYOFMONTH))
			{
				//	Dernier jour du mois (DJ)
				WORD _wLastDayInMonth = m_drNext.GetDaysInMonth(m_drNext.GetMonth(), m_drNext.GetYear());
				DWORD _wMaskLastDayOfMonth = INDEX2BIT(_wLastDayInMonth-1);
				LOGDebugf("Dernier jour du mois coch�, wLastDayInMonth:%d, index:%08lX, m_maskDaysOfMonth=%08lXh", _wLastDayInMonth, _wMaskLastDayOfMonth, m_maskDaysOfMonth);
				maskDaysOfMonth |= _wMaskLastDayOfMonth;
			}

			WORD wIndexDayOfMonth = drLocal.GetDayOfMonth()-1;
			//LOGF("DayOfMonth=%d mask=%08X \n", wIndexDayOfMonth, maskDaysOfMonth);
			DWORD dwMaskDayOfMonth = INDEX2BIT(wIndexDayOfMonth);
			LOGTracef("maskDaysOfMonth=%08lX - dwMaskDayOfMonth:%08lXh", maskDaysOfMonth, dwMaskDayOfMonth);
			for(int i=0;; i++)
			{
				if (i > 35)
				{
					LOGWarnf("Undefined monthly task '%s', setting to undefined.", GetScheduledTaskString());
					m_drNext.SetUndefined();
					break;
				}
				LOGTracef("BitTest:%d (%08lX, index:%08lX)", BitTest(maskDaysOfMonth, dwMaskDayOfMonth), maskDaysOfMonth, dwMaskDayOfMonth);
				if (m_drNext > drLocal && BitTest(maskDaysOfMonth, dwMaskDayOfMonth))
				{
					LOGDebugf("drNext=%s drLocal=%s", m_drNext.ToString(), drLocal.ToString());
					LOGDebugf("maskDaysOfMonth %d ok - MaskDayOfMonth:%08X", wIndexDayOfMonth, dwMaskDayOfMonth);
					break;
				}
				WORD wDaysInMonth = m_drNext.GetDaysInMonth(m_drNext.GetMonth(), m_drNext.GetYear());
				if (++wIndexDayOfMonth >= wDaysInMonth)
				{
					LOGDebugf("DaysInMonth %d reached, resetting to first day of month...", wDaysInMonth);
					wIndexDayOfMonth = 0;
				}
				dwMaskDayOfMonth = MT_DAYOFMONTH(wIndexDayOfMonth);
				LOGTracef("wIndexDayOfMonth=%d, dwMaskDayOfMonth=%08lX, wDaysInMonth=%d", wIndexDayOfMonth, dwMaskDayOfMonth, wDaysInMonth);
				m_drNext += ONE_DAY;
				LOGTracef("ComputeNext next day of month %d...", wIndexDayOfMonth);
			}
			LOGDebugf("Monthly: ComputeNext: %d %s", m_drNext.GetDayOfMonth(), m_drNext.ToString());
		}
		LOGDebugf("End of ComputeNext: TASKMONTHLY for '%s'", GetScheduledTaskString());
		break;
	case TASKYEARLY:
		{
			m_drNext = drLocal;
			//m_drNext.SetUndefined();
			m_drNext.SetSeconds(m_wSecond);
			m_drNext.SetMinutes(m_wMinute);
			m_drNext.SetHours(m_wHour);
			m_drNext.SetDayOfMonth(m_wDayOfMonth);

			ASSERT(drLocal.GetMonth()>0 && drLocal.GetMonth()<13);
			WORD wIndexMonth = drLocal.GetMonth()-1;
			LOGDebugf("Month=%s mask=%08X", CDateRef::GetMonthString(wIndexMonth, FALSE, DR_LANG_FRENCH), m_maskMonths);
			WORD wMaskMonth = INDEX2BIT(wIndexMonth);
			LOGDebugf("maskMonths=%08lX wMaskMonth=%08lX", m_maskMonths, wMaskMonth);
			WORD wOffsetYear = 0;
			for(int i=0; i<50; i++)
			{
				LOGDebugf("drNext=%s", m_drNext.ToString());
				if (m_drNext > drLocal && BitTest(m_maskMonths, wMaskMonth))
				{
					LOGDebugf("drNext=%s drLocal=%s", m_drNext.ToString(), drLocal.ToString());
					LOGDebugf("maskMonth ok %d: %08X", wIndexMonth, wMaskMonth);
					break;
				}
				LOGDebugf("Avant drNext=%s indexmonth=%d wOffsetYear=%d", m_drNext.ToString(), wIndexMonth, wOffsetYear);
				m_drNext += ONE_DAY * CDateRef::GetNumberDaysInMonth(drLocal.GetYear()+wOffsetYear, wIndexMonth+1);
				LOGDebugf("Apres drNext=%s numberofdays=%d bisextil:%d", m_drNext.ToString(), CDateRef::GetNumberDaysInMonth(drLocal.GetYear()+wOffsetYear, wIndexMonth+1), CDateRef::IsBisextilYear(drLocal.GetYear()+wOffsetYear));
				if (++wIndexMonth >= 12)
				{
					wIndexMonth = 0;
					wOffsetYear++;
				}
				wMaskMonth = INDEX2BIT(wIndexMonth);
				LOGDebugf("ComputeNext next month %d...", wIndexMonth);
			}
			LOGDebugf("Yearly: ComputeNext: %s %s", m_drNext.GetWeekDayString(FALSE), m_drNext.ToString());
		}
		break;
	case TASKMONTHLYFIRSTDAY:
		{
			m_drNext = drLocal;
			//m_drNext.SetUndefined();
			m_drNext.SetSeconds(m_wSecond);
			m_drNext.SetMinutes(m_wMinute);
			m_drNext.SetHours(m_wHour);
			
			ASSERT(drLocal.GetMonth()>0 && drLocal.GetMonth()<13);
			WORD wIndexMonth = drLocal.GetMonth()-1;
			LOGDebugf("Month=%s mask=%08X", CDateRef::GetMonthString(wIndexMonth, FALSE, DR_LANG_FRENCH), m_maskMonths);
			WORD wMaskMonth = INDEX2BIT(wIndexMonth);
			LOGDebugf("maskMonths=%08lX wMaskMonth=%08lX", m_maskMonths, wMaskMonth);
			WORD wOffsetYear = 0;
			for(int i=0; i<50; i++)
			{
				m_drNext.SetDayOfMonth(1);
				LOGDebugf("Check drNext=%s", m_drNext.ToString());
				if (m_drNext > drLocal && BitTest(m_maskMonths, wMaskMonth))
				{
					LOGDebugf("OK: drNext=%s drLocal=%s", m_drNext.ToString(), drLocal.ToString());
					LOGDebugf("maskMonth ok %d: %08X", wIndexMonth, wMaskMonth);
					break;
				}
				LOGDebugf("Avant drNext=%s indexmonth=%d wOffsetYear=%d", m_drNext.ToString(), wIndexMonth, wOffsetYear);
				if (++wIndexMonth >= 12)
				{
					wIndexMonth = 0;
					wOffsetYear++;
				}
				wMaskMonth = INDEX2BIT(wIndexMonth);
				
				m_drNext.SetYear(drLocal.GetYear()+wOffsetYear);
				m_drNext.SetMonth(wIndexMonth+1);
				//LOGDebugf("Apres drNext=%s numberofdays=%d bisextil:%d", m_drNext.ToString(), CDateRef::GetNumberDaysInMonth(drLocal.GetYear()+wOffsetYear, wIndexMonth+1), CDateRef::IsBisextilYear(drLocal.GetYear()+wOffsetYear));
				LOGDebugf("ComputeNext next month %d...", wIndexMonth);
			}
			LOGDebugf("MonthlyLastDay: ComputeNext: %s %s", m_drNext.GetWeekDayString(FALSE), m_drNext.ToString());
		}
		break;
	case TASKMONTHLYLASTDAY:
		{
			m_drNext = drLocal;
			//m_drNext.SetUndefined();
			m_drNext.SetSeconds(m_wSecond);
			m_drNext.SetMinutes(m_wMinute);
			m_drNext.SetHours(m_wHour);
			
			ASSERT(drLocal.GetMonth()>0 && drLocal.GetMonth()<13);
			WORD wIndexMonth = drLocal.GetMonth()-1;
			LOGDebugf("Month=%s mask=%08X", CDateRef::GetMonthString(wIndexMonth, FALSE, DR_LANG_FRENCH), m_maskMonths);
			WORD wMaskMonth = INDEX2BIT(wIndexMonth);
			LOGDebugf("maskMonths=%08lX wMaskMonth=%08lX", m_maskMonths, wMaskMonth);
			WORD wOffsetYear = 0;
			for(int i=0; i<50; i++)
			{
				m_drNext.SetDayOfMonth(m_drNext.GetNumberDaysInMonth(m_drNext.GetYear(), m_drNext.GetMonth()));
				LOGDebugf("Check drNext=%s", m_drNext.ToString());
				if (m_drNext > drLocal && BitTest(m_maskMonths, wMaskMonth))
				{
					LOGDebugf("OK: drNext=%s drLocal=%s", m_drNext.ToString(), drLocal.ToString());
					LOGDebugf("maskMonth ok %d: %08X", wIndexMonth, wMaskMonth);
					break;
				}
				LOGDebugf("Avant drNext=%s indexmonth=%d wOffsetYear=%d", m_drNext.ToString(), wIndexMonth, wOffsetYear);
				if (++wIndexMonth >= 12)
				{
					wIndexMonth = 0;
					wOffsetYear++;
				}
				wMaskMonth = INDEX2BIT(wIndexMonth);
				
				m_drNext.SetYear(drLocal.GetYear()+wOffsetYear);
				m_drNext.SetMonth(wIndexMonth+1);
				//LOGDebugf("Apres drNext=%s numberofdays=%d bisextil:%d", m_drNext.ToString(), CDateRef::GetNumberDaysInMonth(drLocal.GetYear()+wOffsetYear, wIndexMonth+1), CDateRef::IsBisextilYear(drLocal.GetYear()+wOffsetYear));
				LOGDebugf("ComputeNext next month %d...", wIndexMonth);
			}
			LOGDebugf("MonthlyLastDay: ComputeNext: %s %s", m_drNext.GetWeekDayString(FALSE), m_drNext.ToString());
		}
		break;
	case TASKIDLE:
		{
			//if
		}
		break;
	case TASKREPMINUTE:
		{
			m_drNext = drLocal;
			m_drNext += 60 * m_wRepetition;
			LOGDebugf("RepMinute: ComputeNext: %s", m_drNext.ToString());
		}
		break;
	case TASKREPHOUR:
		{
			m_drNext = drLocal;
			m_drNext += 3600 * m_wRepetition;
			LOGDebugf("RepHour: ComputeNext: %s", m_drNext.ToString());
		}
		break;
	case TASKREPDAY:
		{
			m_drNext = drLocal;
			m_drNext += 3600 * 24 * m_wRepetition;
			LOGDebugf("RepDay: ComputeNext: %s", m_drNext.ToString());
		}
		break;
	case TASKATSTARTUP:
		{
			if (m_drPrevious.IsUndefined())
			{
				//	First execution
				m_drNext = drLocal;
			}
			else
			{
				//	Second or more
				m_drNext.SetUndefined();
			}
			LOGDebugf("AtStartup: ComputeNext: %s", m_drNext.ToString());
		}
		break;
	default:
		LOGErrorf("CScheduledTask::ComputeNext : !Error: unknown task type %d\n", m_nTaskType);
	}
	LOGTracef("----------------------------------");
}

CString CScheduledTask::GetScheduledTaskString()
{
	CString s, s2;
	CDateRef dr(m_wYear, m_wMonth, m_wDayOfMonth, m_wHour, m_wMinute, m_wSecond);
	dr.SetLang(DR_LANG_FRENCH);

	switch(m_nTaskType)
	{
	case TASKPUNCTUAL:
		s.Format("Ponctuelle le %s %d %s %04d � %02d:%02d:%02d", dr.GetWeekDayString(TRUE), m_wDayOfMonth, dr.GetMonthString(TRUE), m_wYear, m_wHour, m_wMinute, m_wSecond);
		if (IsIdle())
		{
			s2.Format(" [si inactivit� > � %02d:%02d:%02d]", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
			s += (const char *)s2;
		}
		return s;
	case TASKMINUTELY:
		s.Format("Toutes les minutes � la seconde %02d", m_wSecond);
		return s;
	case TASKHOURLY:
		s.Format("Toutes les heures � %02d:%02d", m_wMinute, m_wSecond);
		return s;
	case TASKDAILY:
		s.Format("Tous les jours � %02d:%02d:%02d", m_wHour, m_wMinute, m_wSecond);
		if (IsIdle())
		{
			s2.Format(" [si inactivit� > � %02d:%02d:%02d]", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
			s += (const char *)s2;
		}
		return s;
	case TASKDAILYOPENDAY:
		s.Format("Tous les jours ouvr�s � %02d:%02d:%02d", m_wHour, m_wMinute, m_wSecond);
		if (IsIdle())
		{
			s2.Format(" [si inactivit� > � %02d:%02d:%02d]", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
			s += (const char *)s2;
		}
		return s;
	case TASKWEEKLY:
		{
			CString sWeekDays;
			for(int i=0; i<7; i++)
			{
				if ((1<<i) & m_maskDaysOfWeek)
				{
					sWeekDays += CDateRef::GetWeekDayString(i, FALSE, DR_LANG_FRENCH);
					sWeekDays += ",";
				}
			}
			s.Format("Toutes les semaines le %s � %02d:%02d:%02d", sWeekDays, m_wHour, m_wMinute, m_wSecond);
			if (IsIdle())
			{
				s2.Format(" [si inactivit� > � %02d:%02d:%02d]", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
				s += (const char *)s2;
			}
			return s;
		}
	case TASKMONTHLY:
		{
			CString sMonthDays;
			for(int i=0; i<31; i++)
			{
				if ((1<<i) & m_maskDaysOfMonth)
				{
					char sz[5];
					StringCchPrintf(sz, sizeof(sz), "%d", i+1);
					sMonthDays += sz;
					sMonthDays += ",";
				}
			}
			if (IsIdle())
			{
				s2.Format(" [si inactivit� > � %02d:%02d:%02d]", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
				s += (const char *)s2;
			}
			s.Format("Tous les mois le %s � %02d:%02d:%02d", sMonthDays, m_wHour, m_wMinute, m_wSecond);
			return s;
		}
	case TASKYEARLY:
		{
			CString sMonths;
			for(int i=0; i<12; i++)
			{
				if ((1<<i) & m_maskMonths)
				{
					sMonths += CDateRef::GetMonthString(i+1, FALSE, DR_LANG_FRENCH);
					sMonths += ",";
				}
			}
			if (IsIdle())
			{
				s2.Format(" [si inactivit� > � %02d:%02d:%02d]", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
				s += (const char *)s2;
			}
			s.Format("Tous les ans le %d %s � %02d:%02d:%02d", m_wDayOfMonth, sMonths, m_wHour, m_wMinute, m_wSecond);
			return s;
		}
	case TASKMONTHLYFIRSTDAY:
		{
			CString sMonths;
			for(int i=0; i<12; i++)
			{
				if ((1<<i) & m_maskMonths)
				{
					sMonths += CDateRef::GetMonthString(i+1, FALSE, DR_LANG_FRENCH);
					sMonths += ",";
				}
			}
			if (IsIdle())
			{
				s2.Format(" [si inactivit� > � %02d:%02d:%02d]", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
				s += (const char *)s2;
			}
			s.Format("Le premier jour du(es) mois %s � %02d:%02d:%02d", sMonths, m_wHour, m_wMinute, m_wSecond);
			return s;
		}
	case TASKMONTHLYLASTDAY:
		{
			CString sMonths;
			for(int i=0; i<12; i++)
			{
				if ((1<<i) & m_maskMonths)
				{
					sMonths += CDateRef::GetMonthString(i+1, FALSE, DR_LANG_FRENCH);
					sMonths += ",";
				}
			}
			if (IsIdle())
			{
				s2.Format(" [si inactivit� > � %02d:%02d:%02d]", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
				s += (const char *)s2;
			}
			s.Format("Le dernier jour du(es) mois %s � %02d:%02d:%02d", sMonths, m_wHour, m_wMinute, m_wSecond);
			return s;
		}
	case TASKIDLE:
		{
			s.Format("Si inactivit� > � %02d:%02d:%02d", m_wHourIdle, m_wMinuteIdle, m_wSecondIdle);
			return s;
		}
	case TASKREPMINUTE:
		{
			s.Format("Toutes les %d minutes", m_wRepetition);
			return s;
		}
	case TASKREPHOUR:
		{
			s.Format("Toutes les %d heures", m_wRepetition);
			return s;
		}
	case TASKREPDAY:
		{
			s.Format("Tous les %d jours", m_wRepetition);
			return s;
		}
	case TASKATSTARTUP:
		{
			s.Format("Au d�marrage");
			return s;
		}
	default:
		return "[Unknown task type]";
	}
}
