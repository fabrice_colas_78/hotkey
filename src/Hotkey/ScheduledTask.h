// ScheduledTask.h: interface for the CScheduledTask class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCHEDULEDTASK_H__491493A3_74AB_4FDD_8AF6_17285C50EB75__INCLUDED_)
#define AFX_SCHEDULEDTASK_H__491493A3_74AB_4FDD_8AF6_17285C50EB75__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
#include <dateref.h>
#include <macrosdef.h>

#include <Log4CppLib.h>

#define TASKPUNCTUAL		0
#define TASKMINUTELY		1
#define TASKHOURLY			2
#define TASKDAILY			3
#define TASKWEEKLY			4
#define TASKMONTHLY			5
#define TASKYEARLY			6
#define TASKDAILYOPENDAY	7	//	Uniquement du lundi au vendredi
#define TASKMONTHLYFIRSTDAY	8
#define TASKMONTHLYLASTDAY	9
#define TASKIDLE			10

#define TASKATSTARTUP		15

#define TASKREPMINUTE	20
#define TASKREPHOUR		21	
#define TASKREPDAY		22


#define WT_SUNDAY		INDEX2BIT(0)
#define WT_MONDAY		INDEX2BIT(1)
#define WT_THURSDAY		INDEX2BIT(2)
#define WT_WEDNESDAY	INDEX2BIT(3)
#define WT_TUESDAY		INDEX2BIT(4)
#define WT_FRIDAY		INDEX2BIT(5)
#define WT_SATURDAY		INDEX2BIT(6)

#define MT_DAYOFMONTH(day)	INDEX2BIT(day)
#define MT_LASTDAYOFMONTH	MT_DAYOFMONTH(31)

#define YT_JANUARY		INDEX2BIT(0)
#define YT_FEBRUARY		INDEX2BIT(1)
#define YT_MARCH		INDEX2BIT(2)
#define YT_APRIL		INDEX2BIT(3)
#define YT_MAY			INDEX2BIT(4)
#define YT_JUNE			INDEX2BIT(5)
#define YT_JULY			INDEX2BIT(6)
#define YT_AUGHUST		INDEX2BIT(7)
#define YT_SEPTEMBER	INDEX2BIT(8)
#define YT_OCTOBER		INDEX2BIT(9)
#define YT_NOVEMBER		INDEX2BIT(10)
#define YT_DECEMBER		INDEX2BIT(11)


#define NO_TASK_ID		0

class CScheduledTask
{
public:
	CScheduledTask();
	CScheduledTask(int nTaskType, WORD wSecond, WORD wMinute, WORD wHour, WORD wDay, WORD wMonth, WORD wYear, WORD maskDaysOfWeek, DWORD maskDaysOfMonth, WORD maskMonths, BOOL fOnIdle, WORD wSecondIdle, WORD wMinuteIdle, WORD wHourIdle, WORD wRepetition);
	virtual ~CScheduledTask();
	void operator=(const CScheduledTask& st);
	void ComputeNext();
	CString GetScheduledTaskString();
	//void SetLocalTime();
	void InitDefaultTime();
	void InitTimeAndDate(WORD wYear, WORD wMonth, WORD wDay, WORD wHour, WORD wMinute, WORD wSecond);
	void InitIdle(BOOL fOnIdle, WORD wInHour, WORD wInMinute, WORD wInSecond);
	void InitRepetition(WORD wRepetition);
	DWORD GetIdleValue() { return m_wHourIdle*3600 + m_wMinuteIdle*60 + m_wSecondIdle; }
	BOOL IsIdle() const { return m_fOnIdle; }
	WORD GetSecondIdle() const { return m_wSecondIdle; }
	WORD GetMinuteIdle() const { return m_wMinuteIdle; }
	WORD GetHourIdle() const { return m_wHourIdle; }
	WORD GetSecond() const { return m_wSecond; }
	WORD GetMinute() const { return m_wMinute; }
	WORD GetHour() const { return m_wHour; }
	WORD GetDayOfMonth() const { return m_wDayOfMonth; }
	WORD GetMonth() const { return m_wMonth; }
	WORD GetYear() const { return m_wYear; }
	WORD GetRepetition() const { return m_wRepetition; }
	void SetTaskType(int nTaskType);
	int GetTaskType() const { return m_nTaskType; }
	void SetDaysOfWeekMask(WORD maskDaysOfWeek) { m_maskDaysOfWeek = maskDaysOfWeek; }
	WORD GetDaysOfWeekMask() const { return m_maskDaysOfWeek; }
	void SetDaysOfMonthMask(DWORD maskDaysOfMonth) { m_maskDaysOfMonth = maskDaysOfMonth; }
	DWORD GetDaysOfMonthMask() const { return m_maskDaysOfMonth; }
	void SetMonthsMask(WORD maskMonths) { m_maskMonths = maskMonths; }
	WORD GetMonthsMask() const { return m_maskMonths; }
	void SetTaskId(DWORD dwTaskId) { m_dwTaskId = dwTaskId; }
	DWORD GetTaskId() const { return m_dwTaskId; }
	BOOL IsExpired() { return m_drNext.IsUndefined(); }
	BOOL IsIdleNotTriggered() { return !m_fIdleTriggered; }
	void SetTriggered() { m_fIdleTriggered = true; }
	void ResetTriggered() { m_fIdleTriggered = false; }

	CDateRef m_drNext;
	CDateRef m_drPrevious;
private:
	void Init(int nTaskType, WORD wSecond, WORD wMinute, WORD wHour, WORD wDay, WORD wMonth, WORD wYear, WORD maskDaysOfWeek, DWORD maskDaysOfMonth, WORD maskMonths, BOOL fOnIdle, BOOL fIdleTriggered, WORD wSecondIdle, WORD wMinuteIdle, WORD wHourIdle, WORD wRepetition);
	int      m_nTaskType;
	BOOL     m_fOnIdle;
	BOOL     m_fIdleTriggered;
	WORD     m_wSecond, m_wMinute, m_wHour, m_wDayOfMonth, m_wMonth, m_wYear;
	WORD     m_wSecondIdle, m_wMinuteIdle, m_wHourIdle;
	WORD     m_maskDaysOfWeek;
	DWORD    m_maskDaysOfMonth;
	WORD     m_maskMonths;
	DWORD    m_dwTaskId;
	WORD     m_wRepetition;

	static CLogger * m_logger;
};

typedef CArray <CScheduledTask, const CScheduledTask&> _CScheduledTasks;

class CScheduledTasks : public _CScheduledTasks
{
public:
	void operator =(const CScheduledTasks& tasks)
	{
		RemoveAll();
		for(int i=0; i<tasks.GetSize(); i++)
		{
			Add(tasks[i]);
		}
	}
};

typedef void (WINAPI * PF_SCHEDULER_CALLBACK)(DWORD dwTaskId, void * param);

class CScheduler
{
public:
	CScheduler();
	virtual ~CScheduler();
	DWORD GetIdleSeconds();
	void SetCallback(PF_SCHEDULER_CALLBACK pCallbackFunc, void * param);
	CScheduledTask GetTaskFromId(DWORD dwTaskId);
	DWORD AddTask(int nTaskType, WORD wSecond, WORD wMinute, WORD wHour, WORD wDay, WORD wMonth, WORD wYear, WORD maskDaysOfWeek = 0, DWORD maskDaysOfMonth = 0, WORD maskMonths = 0, BOOL fOnIdle = FALSE, WORD wSecondIdle = 0, WORD wMinuteIdle = 30, WORD wHourIdle = 0, WORD wRepetition = 10);
	DWORD AddTask(CScheduledTask& scheduled_task);
	BOOL DeleteTask(DWORD dwTaskId);
	BOOL StartScheduler();
	BOOL StopScheduler();
	void Check();
	void WaitForNotBusy(const char * szFunction);
	void SetBusy(BOOL fSet) { m_fBusy = fSet; }
		 
	//	Static variables and functions
public:
	BOOL UninstallHooks();
	static DWORD WINAPI SchedulerThread(void * param);
	static DWORD GetNextTaskId();
	CScheduledTasks& GetScheduledTasks() { return m_scheduledTasks; }

	//	Private variables and functions
private:
	static DWORD ms_dwTaskId;
	CScheduledTasks m_scheduledTasks;
	BOOL m_fSchedulerStarted;
	BOOL m_fStopScheduler;
	BOOL m_fBusy;
	DWORD m_dwIdleTime;
	//	Callback
	PF_SCHEDULER_CALLBACK m_pCallbackFunc;
	void * m_pParamCallbackFunc;

	static CLogger * m_logger;
};

#endif // !defined(AFX_SCHEDULEDTASK_H__491493A3_74AB_4FDD_8AF6_17285C50EB75__INCLUDED_)
