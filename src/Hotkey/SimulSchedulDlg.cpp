// SimulSchedulDlg.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"
#include "SimulSchedulDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSimulSchedulDlg dialog


CSimulSchedulDlg::CSimulSchedulDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSimulSchedulDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSimulSchedulDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSimulSchedulDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSimulSchedulDlg)
	DDX_Control(pDX, IDC_LIST_SCHEDULS, m_lvTasks);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSimulSchedulDlg, CDialog)
	//{{AFX_MSG_MAP(CSimulSchedulDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSimulSchedulDlg message handlers
