#if !defined(AFX_SIMULSCHEDULDLG_H__1155B441_0D11_4EA7_8065_2E84E21A650D__INCLUDED_)
#define AFX_SIMULSCHEDULDLG_H__1155B441_0D11_4EA7_8065_2E84E21A650D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SimulSchedulDlg.h : header file
//

#include "ListSchedul.h"

/////////////////////////////////////////////////////////////////////////////
// CSimulSchedulDlg dialog

class CSimulSchedulDlg : public CDialog
{
// Construction
public:
	CSimulSchedulDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSimulSchedulDlg)
	enum { IDD = IDD_DIALOG_SIMUL_SCHEDUL };
	CListSchedul	m_lvTasks;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSimulSchedulDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSimulSchedulDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SIMULSCHEDULDLG_H__1155B441_0D11_4EA7_8065_2E84E21A650D__INCLUDED_)
