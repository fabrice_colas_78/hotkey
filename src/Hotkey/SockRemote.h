// SockRemote.h: interface for the CSockRemote class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOCKREMOTE_H__27C05214_89E6_4A26_8F3E_7C84BA44888D__INCLUDED_)
#define AFX_SOCKREMOTE_H__27C05214_89E6_4A26_8F3E_7C84BA44888D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <log.h>
#include <sock.h>

class CSockRemote : public CSockAsync  
{
public:
	CSockRemote();
	virtual ~CSockRemote();
	void OnAccept(LPARAM lParam, CSockAsync * pSockAsync)
	{
		LOG1F("OnAccept\n");
	}
	void OnClose(int nLastError)
	{
		LOG1F("OnClose\n");
	}
};

#endif // !defined(AFX_SOCKREMOTE_H__27C05214_89E6_4A26_8F3E_7C84BA44888D__INCLUDED_)
