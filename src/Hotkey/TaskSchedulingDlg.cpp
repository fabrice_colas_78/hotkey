// TaskSchedulingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "hotkey.h"

#include <log.h>

#include "ScheduledTask.h"

#include "TaskSchedulingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTaskSchedulingDlg dialog

#define YEAR_REF	2003

CTaskSchedulingDlg::CTaskSchedulingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTaskSchedulingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTaskSchedulingDlg)
	m_uRepetition = 0;
	//}}AFX_DATA_INIT
}


void CTaskSchedulingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTaskSchedulingDlg)
	DDX_Control(pDX, IDC_COMBO_IN_SECOND, m_cbInSecond);
	DDX_Control(pDX, IDC_COMBO_IN_MINUTE, m_cbInMinute);
	DDX_Control(pDX, IDC_COMBO_IN_HOUR, m_cbInHour);
	DDX_Control(pDX, IDC_COMBO_SECOND, m_cbSecond);
	DDX_Control(pDX, IDC_COMBO_MINUTE, m_cbMinute);
	DDX_Control(pDX, IDC_COMBO_HOUR, m_cbHour);
	DDX_Control(pDX, IDC_COMBO_MONTH, m_cbMonth);
	DDX_Control(pDX, IDC_COMBO_DAY, m_cbDayOfMonth);
	DDX_Control(pDX, IDC_COMBO_YEAR, m_cbYear);
	DDX_Text(pDX, IDC_EDIT_REPETITION, m_uRepetition);
	DDV_MinMaxInt(pDX, m_uRepetition, 1, 10000);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTaskSchedulingDlg, CDialog)
	//{{AFX_MSG_MAP(CTaskSchedulingDlg)
	ON_BN_CLICKED(IDC_RADIO_DAILY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_CHECK_JANUARY, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_MONDAY, OnCheckDayOfWeek)
	ON_BN_CLICKED(IDC_CHECK_1, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_INACTIVITY, OnCheckInactivity)
	ON_BN_CLICKED(IDC_RADIO_HOURLY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_MINUTELY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_MONTHLY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_PONCTUAL, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_WEEKLY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_YEARLY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_MONTH_LASTDAY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_IDLE, OnRadioTaskType)
	ON_BN_CLICKED(IDC_CHECK_FEBRUARY, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_MARCH, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_APRIL, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_MAY, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_JUNE, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_JULY, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_AUGHUST, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_SEPTEMBER, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_OCTOBER, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_NOVEMBER, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_DECEMBER, OnCheckMonth)
	ON_BN_CLICKED(IDC_CHECK_THURSDAY, OnCheckDayOfWeek)
	ON_BN_CLICKED(IDC_CHECK_WEDNESDAY, OnCheckDayOfWeek)
	ON_BN_CLICKED(IDC_CHECK_TUESDAY, OnCheckDayOfWeek)
	ON_BN_CLICKED(IDC_CHECK_FRIDAY, OnCheckDayOfWeek)
	ON_BN_CLICKED(IDC_CHECK_SATURDAY, OnCheckDayOfWeek)
	ON_BN_CLICKED(IDC_CHECK_SUNDAY, OnCheckDayOfWeek)
	ON_BN_CLICKED(IDC_CHECK_2, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_3, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_4, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_5, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_6, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_7, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_8, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_9, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_10, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_11, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_12, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_13, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_14, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_15, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_16, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_17, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_18, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_19, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_20, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_21, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_22, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_23, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_24, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_25, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_26, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_27, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_28, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_29, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_30, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_31, OnCheckDay)
	ON_BN_CLICKED(IDC_CHECK_LASTDAY, OnCheckDay)
	ON_BN_CLICKED(IDC_RADIO_REP_DAY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_REP_HOUR, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_REP_MINUTE, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_DAILY_OPENDAY, OnRadioTaskType)
	ON_BN_CLICKED(IDC_RADIO_ATSTARTUP, OnRadioTaskType)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTaskSchedulingDlg message handlers

BOOL CTaskSchedulingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	FillControls();
	SetTaskType();
	SetDaysOfWeekMask();
	SetDaysOfMonthMask();
	SetMonthsMask();
	
	UpdateData(FALSE);
	EnableControls();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTaskSchedulingDlg::FillControls()
{
	CheckDlgButton(IDC_CHECK_INACTIVITY, m_scheduled_task.IsIdle());
	CString s;
	int nIndex;
	//	Annee
	for(int y=YEAR_REF; y<=YEAR_REF+50; y++)
	{
		s.Format("%04u", y);
		nIndex = m_cbYear.AddString(s);
		if (m_scheduled_task.GetYear() == y)
		{
			m_cbYear.SetCurSel(nIndex);
		}
	}
	//	Mois
	char * szMonth[] = {"", "Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet",
		"Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre"};
	for(int m=1; m<sizeof(szMonth)/sizeof(szMonth[0]); m++)
	{
		nIndex = m_cbMonth.AddString(szMonth[m]);
		if (m_scheduled_task.GetMonth() == m)
		{
			m_cbMonth.SetCurSel(nIndex);
		}
	}
	//	Jour
	for(int d=1; d<=31; d++)
	{
		s.Format("%02u", d);
		nIndex = m_cbDayOfMonth.AddString(s);
		if (m_scheduled_task.GetDayOfMonth() == d)
		{
			m_cbDayOfMonth.SetCurSel(nIndex);
		}
	}
	
	//	Heure
	for(int hh=0; hh<=23; hh++)
	{
		s.Format("%02u", hh);
		nIndex = m_cbHour.AddString(s);
		m_cbInHour.AddString(s);
		if (m_scheduled_task.GetHour() == hh)
		{
			m_cbHour.SetCurSel(nIndex);
		}
		if (m_scheduled_task.GetHourIdle() == hh)
		{
			m_cbInHour.SetCurSel(nIndex);
		}
	}
	//	Minute
	for(int mm=0; mm<=59; mm++)
	{
		s.Format("%02u", mm);
		nIndex = m_cbMinute.AddString(s);
		m_cbInMinute.AddString(s);
		if (m_scheduled_task.GetMinute() == mm)
		{
			m_cbMinute.SetCurSel(nIndex);
		}
		if (m_scheduled_task.GetMinuteIdle() == mm)
		{
			m_cbInMinute.SetCurSel(nIndex);
		}
	}
	//	Seconde
	for(int ss=0; ss<=59; ss++)
	{
		s.Format("%02u", ss);
		nIndex = m_cbSecond.AddString(s);
		m_cbInSecond.AddString(s);
		if (m_scheduled_task.GetSecond() == ss)
		{
			m_cbSecond.SetCurSel(nIndex);
		}
		if (m_scheduled_task.GetSecondIdle() == ss)
		{
			m_cbInSecond.SetCurSel(nIndex);
		}
	}
	//	Repetition
	m_uRepetition = m_scheduled_task.GetRepetition();
}

void CTaskSchedulingDlg::SetTaskType()
{
	switch(m_scheduled_task.GetTaskType())
	{
	case TASKPUNCTUAL:
		CheckDlgButton(IDC_RADIO_PONCTUAL, TRUE);
		break;
	case TASKMINUTELY:
		CheckDlgButton(IDC_RADIO_MINUTELY, TRUE);
		break;	
	case TASKHOURLY:
		CheckDlgButton(IDC_RADIO_HOURLY, TRUE);
		break;	
	case TASKDAILY:
		CheckDlgButton(IDC_RADIO_DAILY, TRUE);
		break;	
	case TASKDAILYOPENDAY:
		CheckDlgButton(IDC_RADIO_DAILY_OPENDAY, TRUE);
		break;
	case TASKWEEKLY:
		CheckDlgButton(IDC_RADIO_WEEKLY, TRUE);
		break;	
	case TASKMONTHLY:
		CheckDlgButton(IDC_RADIO_MONTHLY, TRUE);
		break;	
	case TASKYEARLY:
		CheckDlgButton(IDC_RADIO_YEARLY, TRUE);
		break;	
	case TASKMONTHLYFIRSTDAY:
		CheckDlgButton(IDC_RADIO_MONTH_FIRSTDAY, TRUE);
		break;
	case TASKMONTHLYLASTDAY:
		CheckDlgButton(IDC_RADIO_MONTH_LASTDAY, TRUE);
		break;
	case TASKIDLE:
		CheckDlgButton(IDC_RADIO_IDLE, TRUE);
		break;
	case TASKREPMINUTE:
		CheckDlgButton(IDC_RADIO_REP_MINUTE, TRUE);
		break;	
	case TASKREPHOUR:
		CheckDlgButton(IDC_RADIO_REP_HOUR, TRUE);
		break;	
	case TASKREPDAY:
		CheckDlgButton(IDC_RADIO_REP_DAY, TRUE);
		break;	
	case TASKATSTARTUP:
		CheckDlgButton(IDC_RADIO_ATSTARTUP, TRUE);
		break;	
	default:
		ASSERT(FALSE);
		break;
	}
}

int CTaskSchedulingDlg::GetTaskType()
{
	if (IsDlgButtonChecked(IDC_RADIO_PONCTUAL)) return TASKPUNCTUAL;
	if (IsDlgButtonChecked(IDC_RADIO_MINUTELY)) return TASKMINUTELY;
	if (IsDlgButtonChecked(IDC_RADIO_HOURLY)) return TASKHOURLY;
	if (IsDlgButtonChecked(IDC_RADIO_DAILY)) return TASKDAILY;
	if (IsDlgButtonChecked(IDC_RADIO_DAILY_OPENDAY)) return TASKDAILYOPENDAY;
	if (IsDlgButtonChecked(IDC_RADIO_WEEKLY)) return TASKWEEKLY;
	if (IsDlgButtonChecked(IDC_RADIO_MONTHLY)) return TASKMONTHLY;
	if (IsDlgButtonChecked(IDC_RADIO_YEARLY)) return TASKYEARLY;
	if (IsDlgButtonChecked(IDC_RADIO_MONTH_FIRSTDAY)) return TASKMONTHLYFIRSTDAY;
	if (IsDlgButtonChecked(IDC_RADIO_MONTH_LASTDAY)) return TASKMONTHLYLASTDAY;
	if (IsDlgButtonChecked(IDC_RADIO_IDLE)) return TASKIDLE;
	if (IsDlgButtonChecked(IDC_RADIO_REP_MINUTE)) return TASKREPMINUTE;
	if (IsDlgButtonChecked(IDC_RADIO_REP_HOUR)) return TASKREPHOUR;
	if (IsDlgButtonChecked(IDC_RADIO_REP_DAY)) return TASKREPDAY;
	if (IsDlgButtonChecked(IDC_RADIO_ATSTARTUP)) return TASKATSTARTUP;
	return -1;
}

void CTaskSchedulingDlg::OnRadioTaskType() 
{
	// TODO: Add your control notification handler code here
	EnableControls();
}

void CTaskSchedulingDlg::OnCheckMonth() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CTaskSchedulingDlg::OnCheckDayOfWeek() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

void CTaskSchedulingDlg::OnCheckDay() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}

#define ENABLE_CONTROLS(tab)	for(int i=0; i<sizeof(tab)/sizeof(tab[0]); i++) \
									GetDlgItem(tab[i])->EnableWindow(TRUE);
#define DISABLE_CONTROLS(tab)	for(int i=0; i<sizeof(tab)/sizeof(tab[0]); i++) \
									GetDlgItem(tab[i])->EnableWindow(FALSE);
#define IDC_DAYS_OF_WEEK		IDC_CHECK_SUNDAY,IDC_CHECK_MONDAY,IDC_CHECK_TUESDAY,IDC_CHECK_WEDNESDAY,IDC_CHECK_THURSDAY,IDC_CHECK_FRIDAY,IDC_CHECK_SATURDAY
#define IDC_DAYS_OF_MONTH		IDC_CHECK_1,IDC_CHECK_2,IDC_CHECK_3,IDC_CHECK_4,IDC_CHECK_5,IDC_CHECK_6,IDC_CHECK_7,IDC_CHECK_8,IDC_CHECK_9,IDC_CHECK_10,IDC_CHECK_11,IDC_CHECK_12,IDC_CHECK_13,IDC_CHECK_14,IDC_CHECK_15,IDC_CHECK_16,IDC_CHECK_17,IDC_CHECK_18,IDC_CHECK_19,IDC_CHECK_20,IDC_CHECK_21,IDC_CHECK_22,IDC_CHECK_23,IDC_CHECK_24,IDC_CHECK_25,IDC_CHECK_26,IDC_CHECK_27,IDC_CHECK_28,IDC_CHECK_29,IDC_CHECK_30,IDC_CHECK_31,IDC_CHECK_LASTDAY
#define IDC_MONTHS				IDC_CHECK_JANUARY,IDC_CHECK_FEBRUARY,IDC_CHECK_MARCH,IDC_CHECK_APRIL,IDC_CHECK_MAY,IDC_CHECK_JUNE,IDC_CHECK_JULY,IDC_CHECK_AUGHUST,IDC_CHECK_SEPTEMBER,IDC_CHECK_OCTOBER,IDC_CHECK_NOVEMBER,IDC_CHECK_DECEMBER
#define IDC_REPETITION			IDC_EDIT_REPETITION

void CTaskSchedulingDlg::EnableControls()
{
	int nTabCtrl[] = {
		IDC_COMBO_DAY, IDC_COMBO_MONTH, IDC_COMBO_YEAR,
		IDC_COMBO_HOUR, IDC_COMBO_MINUTE, IDC_COMBO_SECOND,
		IDC_DAYS_OF_WEEK,
		IDC_DAYS_OF_MONTH,
		IDC_MONTHS,
		IDC_CHECK_INACTIVITY,
		IDC_COMBO_IN_HOUR, IDC_COMBO_IN_MINUTE, IDC_COMBO_IN_SECOND,
		IDC_REPETITION,
		};

	DISABLE_CONTROLS(nTabCtrl);
	GetDlgItem(IDOK)->EnableWindow(TRUE);
	LOG3F("EnableControls: TaskType: %d\n", GetTaskType());
	switch(GetTaskType())
	{
	case TASKPUNCTUAL:
		{
			int en_tab[] = {		
				IDC_COMBO_DAY, IDC_COMBO_MONTH, IDC_COMBO_YEAR,
				IDC_COMBO_HOUR, IDC_COMBO_MINUTE, IDC_COMBO_SECOND,
				IDC_CHECK_INACTIVITY,
			};
			ENABLE_CONTROLS(en_tab);
		}
		break;
	case TASKMINUTELY:
		{
			int en_tab[] = {		
				IDC_COMBO_SECOND,
			};
			ENABLE_CONTROLS(en_tab);
		}
		break;
	case TASKHOURLY:
		{
			int en_tab[] = {		
				IDC_COMBO_MINUTE, IDC_COMBO_SECOND,
			};
			ENABLE_CONTROLS(en_tab);
		}
		break;
	case TASKDAILY:
	case TASKDAILYOPENDAY:
		{
			int en_tab[] = {		
				IDC_COMBO_HOUR, IDC_COMBO_MINUTE, IDC_COMBO_SECOND,
				IDC_CHECK_INACTIVITY,			
			};
			if (GetTaskType() == TASKDAILYOPENDAY)
			{
				CheckDlgButton(IDC_CHECK_SUNDAY,false);
				CheckDlgButton(IDC_CHECK_MONDAY,true);
				CheckDlgButton(IDC_CHECK_TUESDAY,true);
				CheckDlgButton(IDC_CHECK_WEDNESDAY,true);
				CheckDlgButton(IDC_CHECK_THURSDAY,true);
				CheckDlgButton(IDC_CHECK_FRIDAY,true);
				CheckDlgButton(IDC_CHECK_SATURDAY,false);
			}
			ENABLE_CONTROLS(en_tab);
		}
		break;
	case TASKWEEKLY:
		{
			int en_tab[] = {		
				IDC_COMBO_HOUR, IDC_COMBO_MINUTE, IDC_COMBO_SECOND,
				IDC_DAYS_OF_WEEK,
				IDC_CHECK_INACTIVITY,
			};
			ENABLE_CONTROLS(en_tab);
			GetDlgItem(IDOK)->EnableWindow(GetDaysOfWeekMask());
		}
		break;
	case TASKMONTHLY:
		{
			int en_tab[] = {		
				IDC_COMBO_HOUR, IDC_COMBO_MINUTE, IDC_COMBO_SECOND,
				IDC_DAYS_OF_MONTH,
				IDC_CHECK_INACTIVITY,
			};
			ENABLE_CONTROLS(en_tab);
			GetDlgItem(IDOK)->EnableWindow(GetDaysOfMonthMask());
		}
		break;
	case TASKYEARLY:
		{
			int en_tab[] = {		
				IDC_COMBO_DAY, 
				IDC_COMBO_HOUR, IDC_COMBO_MINUTE, IDC_COMBO_SECOND,
				IDC_MONTHS,
				IDC_CHECK_INACTIVITY,			
			};
			ENABLE_CONTROLS(en_tab);
			GetDlgItem(IDOK)->EnableWindow(GetMonthsMask());
		}
		break;
	case TASKMONTHLYFIRSTDAY:
	case TASKMONTHLYLASTDAY:
		{
			int en_tab[] = {		
				IDC_COMBO_HOUR, IDC_COMBO_MINUTE, IDC_COMBO_SECOND,
				IDC_MONTHS,
				IDC_CHECK_INACTIVITY,			
			};
			ENABLE_CONTROLS(en_tab);
			GetDlgItem(IDOK)->EnableWindow(GetMonthsMask());
		}
		break;
	case TASKIDLE:
		break;
	case TASKREPMINUTE:
	case TASKREPHOUR:
	case TASKREPDAY:
		{
			int en_tab[] = {		
				IDC_REPETITION
			};
			ENABLE_CONTROLS(en_tab);
		}
		break;
	case TASKATSTARTUP:
		break;
	default:
		ASSERT(FALSE);
	}
	if ((IsDlgButtonChecked(IDC_CHECK_INACTIVITY) && GetDlgItem(IDC_CHECK_INACTIVITY)->IsWindowEnabled()) ||
		(GetTaskType() == TASKIDLE))
	{
		int nTab[] = {IDC_COMBO_IN_HOUR, IDC_COMBO_IN_MINUTE, IDC_COMBO_IN_SECOND,};
		for(int i=0; i<sizeof(nTab)/sizeof(nTab[0]); i++)
			GetDlgItem(nTab[i])->EnableWindow(TRUE);
	
	}
}

void CTaskSchedulingDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(true);
	m_scheduled_task.SetTaskType(GetTaskType());
	m_scheduled_task.InitTimeAndDate(m_cbYear.GetCurSel()+YEAR_REF, m_cbMonth.GetCurSel() + 1, m_cbDayOfMonth.GetCurSel() + 1,
			m_cbHour.GetCurSel(), m_cbMinute.GetCurSel(), m_cbSecond.GetCurSel());
	m_scheduled_task.InitIdle(IsDlgButtonChecked(IDC_CHECK_INACTIVITY), m_cbInHour.GetCurSel(), m_cbInMinute.GetCurSel(), m_cbInSecond.GetCurSel());
	m_scheduled_task.InitRepetition(m_uRepetition);
	m_scheduled_task.SetDaysOfWeekMask(GetDaysOfWeekMask());
	m_scheduled_task.SetDaysOfMonthMask(GetDaysOfMonthMask());
	m_scheduled_task.SetMonthsMask(GetMonthsMask());
	CDialog::OnOK();
}

WORD CTaskSchedulingDlg::GetDaysOfWeekMask()
{
	int nTabId[] = {IDC_DAYS_OF_WEEK};
	WORD maskDaysOfWeek = 0;

	for(int i=0; i<sizeof(nTabId)/sizeof(nTabId[0]); i++)
	{
		if (IsDlgButtonChecked(nTabId[i]))
		{
			maskDaysOfWeek |= (1<<i);
		}
	}
	return maskDaysOfWeek;
}

void CTaskSchedulingDlg::SetDaysOfWeekMask()
{
	int nTabId[] = {IDC_DAYS_OF_WEEK};
	WORD maskDaysOfWeek = m_scheduled_task.GetDaysOfWeekMask();

	for(int i=0; i<sizeof(nTabId)/sizeof(nTabId[0]); i++)
	{
		if (maskDaysOfWeek & (1<<i))
		{
			CheckDlgButton(nTabId[i], TRUE);
		}
	}
}

DWORD CTaskSchedulingDlg::GetDaysOfMonthMask()
{
	int nTabId[] = {IDC_DAYS_OF_MONTH};
	DWORD maskDaysOfMonth = 0;

	for(int i=0; i<sizeof(nTabId)/sizeof(nTabId[0]); i++)
	{
		if (IsDlgButtonChecked(nTabId[i]))
		{
			maskDaysOfMonth |= (1<<i);
		}
	}
	return maskDaysOfMonth;
}

void CTaskSchedulingDlg::SetDaysOfMonthMask()
{
	int nTabId[] = {IDC_DAYS_OF_MONTH};
	DWORD maskDaysOfMonth = m_scheduled_task.GetDaysOfMonthMask();

	for(int i=0; i<sizeof(nTabId)/sizeof(nTabId[0]); i++)
	{
		if (maskDaysOfMonth & (1<<i))
		{
			CheckDlgButton(nTabId[i], TRUE);
		}
	}
}

WORD CTaskSchedulingDlg::GetMonthsMask()
{
	int nTabId[] = {IDC_MONTHS};
	WORD maskMonths = 0;

	for(int i=0; i<sizeof(nTabId)/sizeof(nTabId[0]); i++)
	{
		if (IsDlgButtonChecked(nTabId[i]))
		{
			maskMonths |= (1<<i);
		}
	}
	return maskMonths;
}

void CTaskSchedulingDlg::SetMonthsMask()
{
	int nTabId[] = {IDC_MONTHS};
	WORD maskMonths = m_scheduled_task.GetMonthsMask();

	for(int i=0; i<sizeof(nTabId)/sizeof(nTabId[0]); i++)
	{
		if (maskMonths & (1<<i))
		{
			CheckDlgButton(nTabId[i], TRUE);
		}
	}
}

void CTaskSchedulingDlg::OnCheckInactivity() 
{
	// TODO: Add your control notification handler code here
	EnableControls();	
}
