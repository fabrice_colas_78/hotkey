#if !defined(AFX_TASKSCHEDULINGDLG_H__5098ADD7_F243_48D5_A947_CC79858F1D8D__INCLUDED_)
#define AFX_TASKSCHEDULINGDLG_H__5098ADD7_F243_48D5_A947_CC79858F1D8D__INCLUDED_

#include "ScheduledTask.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TaskSchedulingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTaskSchedulingDlg dialog

class CTaskSchedulingDlg : public CDialog
{
// Construction
public:
	CScheduledTask m_scheduled_task;
	void FillControls();
	CTaskSchedulingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTaskSchedulingDlg)
	enum { IDD = IDD_DIALOG_TASK_SCHEDULING };
	CComboBox	m_cbInSecond;
	CComboBox	m_cbInMinute;
	CComboBox	m_cbInHour;
	CComboBox	m_cbSecond;
	CComboBox	m_cbMinute;
	CComboBox	m_cbHour;
	CComboBox	m_cbMonth;
	CComboBox	m_cbDayOfMonth;
	CComboBox	m_cbYear;
	int		m_uRepetition;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTaskSchedulingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTaskSchedulingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioTaskType();
	afx_msg void OnCheckMonth();
	afx_msg void OnCheckDayOfWeek();
	afx_msg void OnCheckDay();
	virtual void OnOK();
	afx_msg void OnCheckInactivity();
	afx_msg void OnCheckLastday();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	WORD GetDaysOfWeekMask();
	void SetDaysOfWeekMask();
	DWORD GetDaysOfMonthMask();
	void SetDaysOfMonthMask();
	WORD GetMonthsMask();
	void SetMonthsMask();
	int GetTaskType();
	void SetTaskType();
	void EnableControls();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TASKSCHEDULINGDLG_H__5098ADD7_F243_48D5_A947_CC79858F1D8D__INCLUDED_)
