// TaskWindowList.cpp : implementation file
//

#include "stdafx.h"
#include "TaskWindowList.h"

#include "resource.h"

#include <mmsystem.h>

#include <log.h>
#include <res.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TASKWINDOWLIST_CLASSNAME	"FCSTaskWindowsListClass"
#define TASKWINDOWLIST_WINDOWNAME	"Liste des fen�tres disponibles"

#define TASKWINDOWLIST_STYLE		(WS_CAPTION | WS_POPUP | WS_BORDER | WS_THICKFRAME)
#define TASKWINDOWLIST_EX_STYLE		(WS_EX_TOOLWINDOW | WS_EX_TOPMOST)

#define LISTBOX_STYLE				(WS_CHILD | LBS_OWNERDRAWVARIABLE | LBS_SORT | \
										LBS_EXTENDEDSEL | LBS_NOINTEGRALHEIGHT | LBS_NOTIFY | \
										LBS_DISABLENOSCROLL | WS_VSCROLL | WS_GROUP)

/////////////////////////////////////////////////////////////////////////////
// CTaskWindowList

IMPLEMENT_DYNCREATE(CTaskWindowList, CWnd)

CTaskWindowList::CTaskWindowList()
{
}

CTaskWindowList::~CTaskWindowList()
{
}

BEGIN_MESSAGE_MAP(CTaskWindowList, CWnd)
	//{{AFX_MSG_MAP(CTaskWindowList)
	ON_WM_PAINT()
	ON_WM_KILLFOCUS()
	ON_WM_SETFOCUS()
	ON_WM_TIMER()
	ON_LBN_DBLCLK(101, OnDblclkListWindows)
	ON_WM_NCDESTROY()
	ON_WM_SIZE()
	ON_WM_SIZING()
	ON_WM_NCHITTEST()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTaskWindowList diagnostics

#ifdef _DEBUG
void CTaskWindowList::AssertValid() const
{
	CWnd::AssertValid();
}

void CTaskWindowList::Dump(CDumpContext& dc) const
{
	CWnd::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTaskWindowList message handlers

BOOL CTaskWindowList::Create()
{
	RegisterWindowClass();
	BOOL fCreat = CreateEx(TASKWINDOWLIST_EX_STYLE, TASKWINDOWLIST_CLASSNAME, TASKWINDOWLIST_WINDOWNAME, TASKWINDOWLIST_STYLE, 0, 0, 200, 200, NULL, NULL);
	if (fCreat)
	{
		CRect rc(0, 0, 100, 100);
		m_lbWindows.Create(LISTBOX_STYLE, rc, this, 101);
		m_lbWindows.ShowWindow(SW_SHOWNORMAL);
		m_lbWindows.SetFocus();
		//m_lbWindows.SetFont((CFont *)&m_lbfont, TRUE);
		FillWindowsList();
		RecalcSizeAndPos();
		GetWindowRect(rc);
		SetCursorPos(rc.left + rc.Width()/2, rc.top + m_lbWindows.GetItemSize().cy / 2);
		SetTimer(1234, 100, NULL);
		
		//	Chargement du son
		m_ressound.Load(IDR_WAV_TASK_WINDOW_LIST2, "WAV");
		m_ressound.Lock();
		PlaySound((LPCSTR)m_ressound.GetPtr(), AfxGetApp()->m_hInstance, SND_MEMORY | SND_ASYNC | SND_NODEFAULT | SND_NOSTOP);
	}
	return fCreat;
}

BOOL CTaskWindowList::RegisterWindowClass()
{
	WNDCLASS WndClass;
	HINSTANCE hInst = AfxGetInstanceHandle();
	
	if( ! (::GetClassInfo(hInst, TASKWINDOWLIST_CLASSNAME, &WndClass)))
	{
		// otherwise we need to register a new class
		WndClass.style         = CS_SAVEBITS | CS_DBLCLKS;
		WndClass.lpfnWndProc   = ::DefWindowProc;
		WndClass.cbClsExtra    = WndClass.cbWndExtra = 0;
		WndClass.hInstance     = hInst;
		WndClass.hIcon         = NULL;
		WndClass.hCursor       = LoadCursor( hInst, IDC_ARROW );
		WndClass.hbrBackground = (HBRUSH)(COLOR_INFOBK + 1); 
		WndClass.lpszMenuName  = NULL;
		WndClass.lpszClassName = TASKWINDOWLIST_CLASSNAME;
		return AfxRegisterClass(&WndClass);
			
	}
	return TRUE;
}

BOOL CALLBACK CTaskWindowList::EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
	CListBoxWindows& lb = *((CListBoxWindows *)lParam);
	char szWindowText[300];
	char szClassName[200];
	if (! ::IsWindowVisible(hwnd)) return TRUE;
	if ((GetWindowLong(hwnd, GWL_EXSTYLE) & WS_EX_TOOLWINDOW) == WS_EX_TOOLWINDOW) return TRUE;
	
	::GetWindowText(hwnd, szWindowText, sizeof(szWindowText));
	::GetClassName(hwnd, szClassName, sizeof(szClassName));
	//HICON hIcon = (HICON)::SendMessage(hwnd, WM_QUERYDRAGICON, TRUE, 0);
	HICON hIcon = (HICON)::SendMessage(hwnd, WM_GETICON, TRUE, 0);
	if (hIcon == NULL)
	{
#ifndef GCL_HICON
#define GCL_HICON           (-14)
#endif
		hIcon = (HICON)GetClassLong(hwnd, (int)GCL_HICON);
		ASSERT(hIcon);
	}
	lb.AddItem(hwnd, szWindowText, szClassName, hIcon);
	return TRUE;
}

void CTaskWindowList::FillWindowsList()
{
	EnumWindows(EnumWindowsProc, (LPARAM)&m_lbWindows);
	m_lbWindows.SetCurSel(0);
}

#define OFFY	70

void CTaskWindowList::RecalcSizeAndPos()
{
	CSize sizeItem = m_lbWindows.GetItemSize();
	int nItemCount = m_lbWindows.GetCount();
	int nItemsSize = sizeItem.cy * nItemCount;
	if (nItemsSize > GetSystemMetrics(SM_CYSCREEN)-OFFY)
	{
		while(nItemsSize > GetSystemMetrics(SM_CYSCREEN)-OFFY)
			nItemsSize -= sizeItem.cy;
	}

	CRect rcLB;
	rcLB.left   = 0;
#ifdef _DEBUG
	rcLB.left   += 10;
#endif
	rcLB.right  = rcLB.left + sizeItem.cx + GetSystemMetrics(SM_CXVSCROLL);
	rcLB.top    = 0;
	rcLB.bottom = rcLB.top + nItemsSize;// + GetSystemMetrics(SM_CYHSCROLL);
	m_lbWindows.MoveWindow(rcLB, TRUE);

	CRect rcWnd;
	rcWnd.left   = GetSystemMetrics(SM_CXSCREEN)/2 - sizeItem.cx/2;
	rcWnd.right  = rcWnd.left + rcLB.Width() + GetSystemMetrics(SM_CXFRAME)*2;
	rcWnd.top    = GetSystemMetrics(SM_CYSCREEN)/2 - (rcLB.Height()/2 + GetSystemMetrics(SM_CYBORDER)*2);
	rcWnd.bottom = rcWnd.top + rcLB.Height() + GetSystemMetrics(SM_CYSMCAPTION) + GetSystemMetrics(SM_CYFRAME)*2;
		
	MoveWindow(rcWnd, TRUE);
}

void CTaskWindowList::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CWnd::OnPaint() for painting messages
}

void CTaskWindowList::Show(BOOL fShow, BOOL fActivate)
{
	if (!fShow)
	{
		SetWindowPos(NULL, 0,  0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_HIDEWINDOW | SWP_NOZORDER);
	}
	else
	{
		SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_SHOWWINDOW | SWP_NOZORDER);
		if (fActivate)
		{
			SetForegroundWindow();
		}
		RedrawWindow();
	}
}

void CTaskWindowList::OnKillFocus(CWnd* pNewWnd) 
{
	CWnd::OnKillFocus(pNewWnd);
	
	// TODO: Add your message handler code here
	LOG1F("OnKillFocus\n");
}

void CTaskWindowList::OnSetFocus(CWnd* pOldWnd) 
{
	CWnd::OnSetFocus(pOldWnd);
	
	// TODO: Add your message handler code here
	LOG1F("OnSetFocus\n");
}

void CTaskWindowList::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if ((HWND)(*GetActiveWindow()) != (HWND)(*this)) 
	{
#ifndef _DEBUG
		PostMessage(WM_CLOSE, 0, 0);
#endif
	}
	CWnd::OnTimer(nIDEvent);
}

void CTaskWindowList::OnDblclkListWindows() 
{
	// TODO: Add your control notification handler code here
}

void CTaskWindowList::OnNcDestroy() 
{
	CWnd::OnNcDestroy();
	
	// TODO: Add your message handler code here
	LOG1F("delete *CTaskWindowList\n");
	delete this;	
}

void CTaskWindowList::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	//LOG1F("CTaskWindowList::OnSize\n");
}

void CTaskWindowList::OnSizing(UINT fwSide, LPRECT pRect) 
{
	CWnd::OnSizing(fwSide, pRect);
	
	// TODO: Add your message handler code here
	//LOG1F("CTaskWindowList::OnSizing\n");
}

LRESULT CTaskWindowList::OnNcHitTest(CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	UINT uVal = (UINT)CWnd::OnNcHitTest(point);
	LOG1F("Val=%d %d\n", CWnd::OnNcHitTest(point), HTBOTTOM);
	switch(uVal)
	{
	case HTLEFT:
	case HTTOP:
	case HTRIGHT:
	case HTBOTTOM:
	case HTTOPLEFT:
	case HTTOPRIGHT:
	case HTBOTTOMRIGHT:
	case HTBOTTOMLEFT:
		return HTNOWHERE;
	default:
		return uVal;
	}
}

BOOL CTaskWindowList::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	LOG1F("OnCommand %d,%d %d,%d %d\n", LOWORD(wParam), HIWORD(wParam), LOWORD(lParam), HIWORD(lParam), LBN_DBLCLK);
	switch(LOWORD(wParam))
	{
	case 101:
		if (HIWORD(wParam) == LBN_DBLCLK)
		{
			HWND hWndToActivate = m_lbWindows.GetWindowHandleCurrent();
			WINDOWPLACEMENT wp;
			wp.length = sizeof(wp);
			::GetWindowPlacement(hWndToActivate, &wp);
			if (::IsIconic(hWndToActivate))
			{
				if ((wp.flags | WPF_RESTORETOMAXIMIZED) == WPF_RESTORETOMAXIMIZED)
					::ShowWindow(hWndToActivate, SW_RESTORE);
				else
					::ShowWindow(hWndToActivate, SW_SHOWMAXIMIZED);
			}
			::SetForegroundWindow(hWndToActivate);
			PostMessage(WM_CLOSE, 0, 0);
		}
		break;
	case 102:
		{
			for(int i=0; i<m_lbWindows.GetCount(); i++)
			{
				if (m_lbWindows.GetSel(i) > 0)
				{
					LOG1F("SelIndex=%d\n", i); 
					::ShowWindow(m_lbWindows.GetWindowHandle(i), SW_SHOWMINIMIZED);
				}
			}
			PostMessage(WM_CLOSE, 0, 0);
		}
		break;
	case 103:
		{
			for(int i=0; i<m_lbWindows.GetCount(); i++)
			{
				if (m_lbWindows.GetSel(i) > 0)
				{
					::PostMessage(m_lbWindows.GetWindowHandle(i), WM_CLOSE, 0, 0);
				}
			}
			PostMessage(WM_CLOSE, 0, 0);
		}
		break;
	}
	return TRUE;
}
