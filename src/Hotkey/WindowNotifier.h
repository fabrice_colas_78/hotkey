#if !defined(AFX_WINDOWNOTIFIER_H__2DAA5476_6C26_46FF_94F2_F892D0415A61__INCLUDED_)
#define AFX_WINDOWNOTIFIER_H__2DAA5476_6C26_46FF_94F2_F892D0415A61__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WindowNotifier.h : header file
//

#define WM_WINDOWNOTIFIERCLICKED	WM_USER+123
#define WN_TEXT_NORMAL				0x0000
#define WN_TEXT_BOLD				0x0001
#define WN_TEXT_ITALIC				0x0002
#define WN_TEXT_UNDERLINE			0x0004


/////////////////////////////////////////////////////////////////////////////
// CWindowNotifier window

class CWindowNotifier : public CWnd
{
	DECLARE_DYNAMIC(CWindowNotifier)

// Construction
public:
	CWindowNotifier();
	int Create(CWnd *pWndParent);
	void Show(LPCTSTR szCaption, DWORD dwTimeToShow=500, DWORD dwTimeToStay=3000, DWORD dwTimeToHide=500, int nIncrement=1);
	void Hide();
	BOOL SetSkin(UINT nBitmapID, short red=-1, short green=-1, short blue=-1);
	BOOL SetSkin(LPCTSTR szFileName, short red=-1, short green=-1, short blue=-1);
	void SetTextFont(LPCTSTR szFont, int nSize, int nNormalStyle, int nSelectedStyle);
	void SetTextColor(COLORREF crNormalTextColor, COLORREF crSelectedTextColor);
	void SetTextRect(RECT rcText);
	int GetSkinWidth() { return m_nSkinWidth; }
	int GetSkinHeight() { return m_nSkinHeight; }


// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWindowNotifier)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CWindowNotifier();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseHover(WPARAM w, LPARAM l);
	afx_msg LRESULT OnMouseLeave(WPARAM w, LPARAM l);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	
	// Generated message map functions
protected:
	//{{AFX_MSG(CWindowNotifier)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	BYTE* Get24BitPixels(HBITMAP pBitmap, WORD *pwWidth, WORD *pwHeight);
	HRGN GenerateRegion(HBITMAP hBitmap, BYTE red, BYTE green, BYTE blue);

private:
	CWnd *m_pWndParent;
	
	CFont m_myNormalFont;
	CFont m_mySelectedFont;
	COLORREF m_crNormalTextColor;
	COLORREF m_crSelectedTextColor;
	HCURSOR m_hCursor;
	
	CBitmap m_biSkinBackground;
	HRGN m_hSkinRegion;
	CRect m_rcText;
	int m_nSkinWidth;
	int m_nSkinHeight;

	CString m_strCaption;
	BOOL m_bMouseIsOver;

 	DWORD m_dwTimeToShow;
	DWORD m_dwTimeToLive;
	DWORD m_dwTimeToHide;
	DWORD m_dwDelayBetweenShowEvents;
	DWORD m_dwDelayBetweenHideEvents;
	int m_nAnimStatus;
	int m_nStartPosX;
	int m_nStartPosY;
	int m_nCurrentPosX;
	int m_nCurrentPosY;
	int m_nTaskbarPlacement;
	int m_nIncrement;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WINDOWNOTIFIER_H__2DAA5476_6C26_46FF_94F2_F892D0415A61__INCLUDED_)
