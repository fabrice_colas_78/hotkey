#pragma once

#include <log.h>

#include <winerr.h>

#include "global.h"

#define NO_VIRTUAL_KEY	0xFFFFFFFF
#define NO_MODIFIER		0xFFFFFFFF

class CHotkey
{
public:
	static UINT GetFirstFreeHotkeyId()
	{
		static int s_nHotkeyId = 10;
		return ++s_nHotkeyId;
	}
	static BOOL IsRegisterableHotkey(HWND hWnd, UINT uVirtualKey, UINT ukModifier)
	{
		UINT uHotkeyId;
		if (RegisterHotkey(hWnd, uVirtualKey, ukModifier, uHotkeyId))
		{
			UnRegisterHotkey(hWnd, uHotkeyId);
			return TRUE;
		}
		return FALSE;
	}
	static BOOL RegisterHotkey(HWND hWnd, UINT uVirtualKey, UINT ukModifier, UINT& uIdHotkey)
	{
		uIdHotkey = GetFirstFreeHotkeyId();
		return RegisterHotKey(hWnd, uIdHotkey, ukModifier, uVirtualKey);
	}
	static BOOL UnRegisterHotkey(HWND hWnd, UINT uIdHotkey)
	{
		return UnregisterHotKey(hWnd, uIdHotkey);
	}
	CHotkey::CHotkey()
	{
		m_uVirtualKey = NO_VIRTUAL_KEY;	// 'A';
		m_uVkmodifier = NO_MODIFIER;	// MOD_ALT|MOD_CONTROL;
		m_hwndOwner   = NULL;
		m_uHotkeyID   = 0;
	}
	void operator=(const CHotkey& hotkey)
	{
		m_hwndOwner    = hotkey.m_hwndOwner;
		m_uVirtualKey  = hotkey.m_uVirtualKey;
		m_uVkmodifier  = hotkey.m_uVkmodifier;
		m_uHotkeyID    = hotkey.m_uHotkeyID;
	}
	BOOL IsValid()
	{
		return m_uVirtualKey != NO_VIRTUAL_KEY && m_uVkmodifier != NO_MODIFIER;
	}
	BOOL RegisterHotkey(HWND hwndOwner)
	{
		ASSERT(!IsRegistered());
		m_hwndOwner = hwndOwner;
		BOOL fRet = RegisterHotkey(m_hwndOwner, m_uVirtualKey, m_uVkmodifier, m_uHotkeyID);
		if (! fRet) m_uHotkeyID = 0;
		LOG3F(_T("RegisterHotkey() %d %08lX: %s\n"), m_uHotkeyID, m_hwndOwner, fRet? _T("TRUE"): _T("FALSE"));
		return fRet;
	}
	BOOL UnRegisterHotkey()
	{
		if (m_uHotkeyID)
		{
			BOOL fRet = UnRegisterHotkey(m_hwndOwner, m_uHotkeyID);
			m_uHotkeyID = 0;
			LOG3F("UnRegisterHotkey() %d %08lX : %s (%s)\n", m_uHotkeyID, m_hwndOwner, fRet?"TRUE":"FALSE", GetLastErrorString());
			return fRet;
		}
		return FALSE;
	}
	BOOL IsRegistered()
	{
		return m_uHotkeyID != 0;
	}
	//static CString CHotkeyDlg::GetHotkeyString(UINT ukModifier, UINT uVirtualKey);
	CString GetHotkeyString()
	{
		if (! IsValid()) return "[Invalide]";
		CString sHotkey;

		for(int i=0;; i++)
		{
			if (s_modifiers[i].szLabel == NULL)
			{
				ASSERT(FALSE);
				break;
			}
			if (s_modifiers[i].uVk == m_uVkmodifier)
			{
				sHotkey += s_modifiers[i].szLabel;
				break;
			}
		}
		
		for(int i=0;; i++)
		{
			if (s_hotkeys[i].szLabel == NULL)
			{
				ASSERT(FALSE);
				break;
			}
			if (s_hotkeys[i].uVk == m_uVirtualKey)
			{
				if (sHotkey != "") sHotkey += " + ";
				sHotkey += s_hotkeys[i].szLabel;
				break;
			}
		}
		return sHotkey;
	}
	HWND m_hwndOwner;
	UINT m_uVirtualKey;
	UINT m_uVkmodifier;
	UINT m_uHotkeyID;
};
