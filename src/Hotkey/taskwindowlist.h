#if !defined(AFX_TASKWINDOWLIST_H__FC93E65B_BB34_4606_BD59_ED83B107B1A6__INCLUDED_)
#define AFX_TASKWINDOWLIST_H__FC93E65B_BB34_4606_BD59_ED83B107B1A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TaskWindowList.h : header file
//

#include <afxtempl.h>

#include <res.h>
#include "ListBoxWindows.h"

/////////////////////////////////////////////////////////////////////////////
// CTaskWindowList window

class CTaskWindowList : public CWnd
{
protected:
	DECLARE_DYNCREATE(CTaskWindowList)

// Attributes
public:
	CTaskWindowList();           // protected constructor used by dynamic creation
	~CTaskWindowList();
	static BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);

// Operations
public:
	CRes m_ressound;
	//CMyFont m_lbfont;
	CListBoxWindows m_lbWindows;
	void Show(BOOL fShow, BOOL fActivate);
	BOOL RegisterWindowClass();
	BOOL Create();
	void FillWindowsList();
	void RecalcSizeAndPos();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTaskWindowsList)
	protected:
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CTaskWindowList)
	afx_msg void OnPaint();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDblclkListWindows();
	afx_msg void OnNcDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	//}}AFX_MSG
	afx_msg BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TASKWINDOWLIST_H__FC93E65B_BB34_4606_BD59_ED83B107B1A6__INCLUDED_)
