// HotkeyLib.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "HotkeyLib.h"

#pragma comment(lib, "User32")

HINSTANCE g_hInstance = NULL;
HHOOK g_hHookKeyboard = NULL;
HHOOK g_hHookMouse = NULL;
DWORD g_dwLastTick = 0;
#ifdef _DEBUG
DWORD g_dwCountKeyboard = 0;
DWORD g_dwCountMouse = 0;
BOOL  g_fHookStarted = FALSE;
#endif

#ifndef WH_KEYBOARD_LL
#define WH_KEYBOARD_LL	13
#endif
#ifndef WH_MOUSE_LL
#define WH_MOUSE_LL	14
#endif

LRESULT CALLBACK HookKeyboard(int code, WPARAM wParam, LPARAM lParam)
{
#ifdef _DEBUG
	g_dwCountKeyboard++;
#endif
	if (code == HC_ACTION)
	{
		//	Reset last tick count
		g_dwLastTick = GetTickCount();
	}
	return CallNextHookEx(g_hHookKeyboard, code, wParam, lParam);
}

LRESULT CALLBACK HookMouse(int code, WPARAM wParam, LPARAM lParam)
{
#ifdef _DEBUG
	g_dwCountMouse++;
#endif
	if (code == HC_ACTION)
	{
		//	Reset last tick count
		g_dwLastTick = GetTickCount();
	}
	return ::CallNextHookEx(g_hHookMouse, code, wParam, lParam);
}

HOTKEYLIB_API BOOL HotkeyLibInstallHooks()
{
	g_hHookKeyboard = ::SetWindowsHookEx(WH_KEYBOARD_LL, HookKeyboard, g_hInstance, 0);
	g_hHookMouse = ::SetWindowsHookEx(WH_MOUSE_LL, HookMouse, g_hInstance, 0);
	g_dwLastTick = GetTickCount();
#ifdef _DEBUG
	g_fHookStarted = TRUE;
#endif
	return g_hHookKeyboard && g_hHookMouse;
}

HOTKEYLIB_API BOOL HotkeyLibUninstallHooks()
{
	if (!g_hHookKeyboard || !g_hHookKeyboard) return TRUE;
	BOOL fRet = UnhookWindowsHookEx(g_hHookKeyboard) && UnhookWindowsHookEx(g_hHookMouse);
#ifdef _DEBUG
	g_fHookStarted = FALSE;
#endif
	g_hHookKeyboard = NULL;
	g_hHookMouse = NULL;
	return TRUE;
}

HOTKEYLIB_API DWORD HotkeyLibGetIdleSeconds(BOOL * pfReset)
{
	if (pfReset)
	{

	}
	return (GetTickCount() - g_dwLastTick) / 1000;
}

#ifdef _DEBUG
HOTKEYLIB_API void HotkeyLibGetValues(DWORD& dwKeyboard, DWORD& dwMouse, BOOL& fHookStarted)
{
	dwKeyboard = g_dwCountKeyboard;
	dwMouse = g_dwCountMouse;
	fHookStarted = g_fHookStarted;
}
#endif
