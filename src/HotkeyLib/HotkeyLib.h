
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the HOTKEYLIB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// HOTKEYLIB_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef HOTKEYLIB_EXPORTS
#define HOTKEYLIB_API __declspec(dllexport)
#else
#define HOTKEYLIB_API __declspec(dllimport)
#endif

extern HINSTANCE g_hInstance;

HOTKEYLIB_API BOOL HotkeyLibInstallHooks();
HOTKEYLIB_API BOOL HotkeyLibUninstallHooks();
HOTKEYLIB_API DWORD HotkeyLibGetIdleSeconds(BOOL * pfReset = NULL);
HOTKEYLIB_API void HotkeyLibGetValues(DWORD& dwKeyboard, DWORD& dwMouse, BOOL& fHookStarted);